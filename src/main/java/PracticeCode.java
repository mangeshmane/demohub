import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Iterator;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class PracticeCode {

	public static void main(String[] args) throws ClassNotFoundException {

		    String jdbcURL = "jdbc:mysql://localhost/sys";
	        String username = "root";
	        String password = "root";
	        String myDriver = "com.mysql.cj.jdbc.Driver";
	        Class.forName(myDriver);
	        String excelFilePath = "C:\\Users\\mange\\Downloads\\4_Products.xlsx";
	 
	        int batchSize = 20;
	 
	        Connection connection = null;
	 
	        try {
	            long start = System.currentTimeMillis();
	             
	            FileInputStream inputStream = new FileInputStream(excelFilePath);
	 
	           Workbook workbook = new XSSFWorkbook(inputStream);
	 
	            Sheet firstSheet = workbook.getSheetAt(0);
	            Iterator<Row> rowIterator = firstSheet.iterator();
	 
	            connection = DriverManager.getConnection(jdbcURL, username, password);
	            connection.setAutoCommit(false);
	  
	            String sql = "INSERT INTO products (Identifier, Level, Type, Name, Revision, DesignState, Weight, Material, Process, Procurement Intent, EndItem) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?)";
	            PreparedStatement statement = connection.prepareStatement(sql);    
	             
	            int count = 0;
	             
	            rowIterator.next(); // skip the header row
	             
	            while (rowIterator.hasNext()) {
	                Row nextRow = rowIterator.next();
	                Iterator<Cell> cellIterator = nextRow.cellIterator();
	 
	                while (cellIterator.hasNext()) {
	                    Cell nextCell = cellIterator.next();
	                    
	                    int columnIndex = nextCell.getColumnIndex();
	                    CellType type = nextCell.getCellType();
	                    switch (columnIndex) {
	                    case 0:
	                        String Identifier = nextCell.getStringCellValue();
	                        statement.setString(1, Identifier);
	                        break;
	                    case 1:
	                        double Level =  nextCell.getNumericCellValue();
	                        statement.setDouble(2, Level);
	                        break;
	                    case 2:
	                        String Type = nextCell.getStringCellValue();
	                        statement.setString(3, Type);
	                        break;
	                    case 3:
	                        String Name = nextCell.getStringCellValue();
	                        statement.setString(4, Name);
	                        break;
	                    case 4:
	                    	double Revision = nextCell.getNumericCellValue();
	                        statement.setDouble(5, Revision);
	                        break;
	                    case 5:
	                        String DesignState = nextCell.getStringCellValue();
	                        statement.setString(6, DesignState);
	                        break;
	                    case 6:
	                        Double Weight = nextCell.getNumericCellValue();
	                        statement.setDouble(7, Weight);
	                        break;
	                    case 7:
	                        String Material = nextCell.getStringCellValue();
	                        statement.setString(8, Material);
	                        break;
	                    case 8:
	                        String Process = nextCell.getStringCellValue();
	                        statement.setString(9, Process);
	                        break;
	                    case 9:
	                        String ProcurementIntent = nextCell.getStringCellValue();
	                        statement.setString(10, ProcurementIntent);
	                        break;
	                    case 10:
	                        String EndItem = nextCell.getStringCellValue();
	                        statement.setString(11, EndItem);
	                        break;
	                     default:
	                    	 System.out.println("nothing to print");
	                    }
	 
	                }
	               
//	                statement.addBatch();
//	                 
//	                if (count % batchSize == 0) {
//	                    statement.executeBatch();
//	                }              
//	 
	            }
	 
	            int ex = statement.executeUpdate();
	             
	            // execute the remaining queries
//	            statement.executeBatch();
	  
	            connection.commit();
	            connection.close();
	             
	            long end = System.currentTimeMillis();
	            System.out.printf("Import done in %d ms\n", (end - start));
	             
	        } catch (IOException ex1) {
	            System.out.println("Error reading file");
	            ex1.printStackTrace();
	        } catch (SQLException ex2) {
	            System.out.println("Database error");
	            ex2.printStackTrace();
	        }
	}
	
}

