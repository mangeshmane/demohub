package com.dem.demoHub.pdfPractice;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import javax.swing.text.StyleConstants.FontConstants;

import java.util.AbstractMap.SimpleEntry;

import com.demo.demoHub.Utility.StringConstant;
import com.itextpdf.io.font.FontProgram;
import com.itextpdf.io.font.FontProgramFactory;
import com.itextpdf.io.font.PdfEncodings;
import com.itextpdf.io.font.constants.StandardFonts;
import com.itextpdf.io.image.ImageData;
import com.itextpdf.io.image.ImageDataFactory;
import com.itextpdf.kernel.colors.WebColors;
import com.itextpdf.kernel.events.Event;
import com.itextpdf.kernel.events.IEventHandler;
import com.itextpdf.kernel.events.PdfDocumentEvent;
import com.itextpdf.kernel.font.PdfFont;
import com.itextpdf.kernel.font.PdfFontFactory;
import com.itextpdf.kernel.geom.PageSize;
import com.itextpdf.kernel.geom.Rectangle;
import com.itextpdf.kernel.pdf.PageLabelNumberingStyle;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfOutline;
import com.itextpdf.kernel.pdf.PdfPage;
import com.itextpdf.kernel.pdf.PdfString;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.kernel.pdf.action.PdfAction;
import com.itextpdf.kernel.pdf.canvas.PdfCanvas;
import com.itextpdf.kernel.pdf.canvas.draw.DottedLine;
import com.itextpdf.kernel.pdf.navigation.PdfDestination;
import com.itextpdf.layout.Canvas;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.borders.Border;
import com.itextpdf.layout.borders.SolidBorder;
import com.itextpdf.layout.element.AreaBreak;
import com.itextpdf.layout.element.Cell;
import com.itextpdf.layout.element.Div;
import com.itextpdf.layout.element.IBlockElement;
import com.itextpdf.layout.element.Image;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.element.Tab;
import com.itextpdf.layout.element.TabStop;
import com.itextpdf.layout.element.Table;
import com.itextpdf.layout.element.Text;
import com.itextpdf.layout.layout.LayoutArea;
import com.itextpdf.layout.layout.LayoutContext;
import com.itextpdf.layout.layout.LayoutResult;
import com.itextpdf.layout.properties.AreaBreakType;
import com.itextpdf.layout.properties.HorizontalAlignment;
import com.itextpdf.layout.properties.Property;
import com.itextpdf.layout.properties.TabAlignment;
import com.itextpdf.layout.properties.TextAlignment;
import com.itextpdf.layout.properties.UnitValue;
import com.itextpdf.layout.properties.VerticalAlignment;
import com.itextpdf.layout.renderer.DocumentRenderer;
import com.itextpdf.layout.renderer.ParagraphRenderer;
import com.itextpdf.layout.renderer.TableRenderer;

public class HeaderAndFooterWorkingExampleNew {

	public static final String DEST = "C:\\Users\\mange\\OneDrive\\Desktop\\New folder\\pdf\\_sign1.pdf";

	public static void main(String[] args) throws Exception {
		File file = new File(DEST);
		file.getParentFile().mkdirs();

		new HeaderAndFooterWorkingExampleNew().manipulatePdf(DEST);
	}

	protected void manipulatePdf(String dest) throws Exception {
		PdfDocument pdfDoc = new PdfDocument(new PdfWriter(dest));

		// third parameter false is for not flushing pages immediately.
		Document doc = new Document(pdfDoc, PageSize.A4, false);
		pdfDoc.addNewPage();
		
		//these two fonts are from TOC code base.
		PdfFont font = PdfFontFactory.createFont(StandardFonts.TIMES_ROMAN);
	    PdfFont bold = PdfFontFactory.createFont(StandardFonts.HELVETICA_BOLD);
		
	    
		Image leftImage = getImage(StringConstant.HEAD_LEFT);
		Image rightImage = getImage(StringConstant.HEAD_RIGHT);
		Image firstPage = getImage(StringConstant.FIRST_PAGE_IMAGE);
		Image check = getImage("C:\\Users\\mange\\Downloads\\Checkbox_Checked.png");
		Image unCheck = getImage("C:\\Users\\mange\\Downloads\\checkbox_unchecked.png");
		// Header code is here.
		TableHeaderEventHandler handler = new TableHeaderEventHandler(doc, leftImage, rightImage);
		pdfDoc.addEventHandler(PdfDocumentEvent.END_PAGE, handler);

		// footer code is here.
//		pdfDoc.addEventHandler(PdfDocumentEvent.END_PAGE, new TableFooterEventHandler(doc));

		// Calculate top margin to be sure that the table will fit the margin.
		float topMargin = 20 + handler.getTableHeight();
		doc.setMargins(100, 36, 36, 36);
		
		// first page two paragraph or heading we can say.
		doc.add(new Paragraph("Industrielle Sachversicherung")
		   .setTextAlignment(TextAlignment.RIGHT)
		   .setFontSize(20)
		   .setFontColor(WebColors.getRGBColor("#595959"))
		   .setBold().setMarginBottom(20));
		Text text1 = new Text("/ ");
	    text1.setFontSize(32)
			 .setFontColor(WebColors.getRGBColor("#ffc000"));
		
		Text text2 = new Text("BESICHTIGUNGSBERICHT");
	    text2.setFontSize(32)
			 .setFontColor(WebColors.getRGBColor("#595959"));
	    
	    Paragraph fp = new Paragraph();
	    fp.add(text1)
	      .add(text2)
	      .setTextAlignment(TextAlignment.RIGHT)
	      .setMarginBottom(20);
	    doc.add(fp);
	   
	    Table outer = new Table(1);
	    outer.setMarginTop(18);
	    outer.setFixedLayout().useAllAvailableWidth();
	    Table table22 = new Table(new float[] {20,80});
	    table22.setFixedLayout().useAllAvailableWidth();
	    table22.setMargin(5);
	    
	    table22.addCell(new Cell().add(new Paragraph("Name:").setVerticalAlignment(VerticalAlignment.TOP).setTextAlignment(TextAlignment.CENTER)).setBorder(Border.NO_BORDER));
	    table22.addCell(new Cell().add(new Paragraph("adfadfad")).setBorder(Border.NO_BORDER).setBorderBottom(new SolidBorder(1)));
	    table22.addCell(new Cell(0,2).setBorder(Border.NO_BORDER).setHeight(15));
	    
	    table22.addCell(new Cell().add(new Paragraph("Address:").setVerticalAlignment(VerticalAlignment.TOP).setTextAlignment(TextAlignment.CENTER)).setBorder(Border.NO_BORDER));
	    table22.addCell(new Cell().add(new Paragraph("")).setBorder(Border.NO_BORDER).setBorderBottom(new SolidBorder(1)));
	    table22.addCell(new Cell(0,2).setBorder(Border.NO_BORDER).setHeight(15));
	   
	    table22.addCell(new Cell().add(new Paragraph("Zip code, City:").setVerticalAlignment(VerticalAlignment.TOP).setTextAlignment(TextAlignment.CENTER)).setBorder(Border.NO_BORDER));
	    table22.addCell(new Cell().add(new Paragraph("")).setBorder(Border.NO_BORDER).setBorderBottom(new SolidBorder(1)));
	    table22.addCell(new Cell(0,2).setBorder(Border.NO_BORDER).setHeight(15));
	    
	    table22.addCell(new Cell().add(new Paragraph("Date:").setVerticalAlignment(VerticalAlignment.TOP).setTextAlignment(TextAlignment.CENTER)).setBorder(Border.NO_BORDER));
	    table22.addCell(new Cell().add(new Paragraph("")).setBorder(Border.NO_BORDER).setBorderBottom(new SolidBorder(1)));
		doc.add(firstPage);
		
	    outer.addCell(new Cell().add(table22)
	    		.setBorderLeft(Border.NO_BORDER)
	    		.setBorderRight(Border.NO_BORDER)
	    		.setBorderTop(new SolidBorder(WebColors.getRGBColor("#ffc000"), 5))
	    		.setBorderBottom(new SolidBorder(WebColors.getRGBColor("#ffc000"), 5)));
	    doc.add(outer);
	    
		
//		doc.add(new AreaBreak(AreaBreakType.NEXT_PAGE));
		pdfDoc.getNumberOfPages();
//		doc.add(new Paragraph("This is first page."));
		doc.add(new AreaBreak(AreaBreakType.NEXT_PAGE));
		
		int object_type = 6;
		
		//Start Integrate the api here.
		for(int i = 0; i<object_type; i++) {
			
		}
		
		
		doc.add(new AreaBreak(AreaBreakType.NEXT_PAGE));
		for (int i = 0; i < 10; i++) {
			doc.add(new Paragraph("Hello World!"));
		}
		doc.add(new Paragraph("heaay you itext...."));
		SimpleEntry<String, Integer> titlePage = null;
		List<SimpleEntry<String, SimpleEntry<String, Integer>>> toc = new ArrayList<>();
		Paragraph pp = new Paragraph("1.first");
		doc.add(pp);
		
		titlePage = new SimpleEntry("1.first", pdfDoc.getNumberOfPages());
		
		pp
		.setFont(bold)
		.setFontSize(12)
//		.setKeepWithNext(true)
		.setDestination("page1")
		.setNextRenderer(new UpdatePageRenderer(pp, titlePage));
		toc.add(new SimpleEntry("page1", titlePage));
		

		doc.add(new AreaBreak(AreaBreakType.NEXT_PAGE));

		pp = new Paragraph("2.second");
		titlePage = new SimpleEntry("2.second", pdfDoc.getNumberOfPages());
		doc.add(pp);
		pp
		.setFont(bold)
		.setFontSize(12)
//		.setKeepWithNext(false)
		.setDestination("page2")
		.setNextRenderer(new UpdatePageRenderer(pp, titlePage));
		toc.add(new SimpleEntry("page2", titlePage));
		
		
		int contentPageNumber = pdfDoc.getNumberOfPages();
		
		doc.add(new AreaBreak());
		pdfDoc.getNumberOfPages();
		doc.add(new Paragraph("Hello World!"));
		doc.add(new AreaBreak());
		pdfDoc.getNumberOfPages();
		doc.add(new Paragraph("Hello World!"));
		doc.add(new AreaBreak());
		pdfDoc.getNumberOfPages();
		Text texts = new Text("    No");
		Paragraph pr = new Paragraph().add(unCheck.setHeight(15).setWidth(15));
		pr.add(texts).setHorizontalAlignment(HorizontalAlignment.CENTER);
		doc.add(pr);
		Paragraph ps = new Paragraph(texts).setPaddingTop(1).setVerticalAlignment(VerticalAlignment.MIDDLE).add(unCheck).setHorizontalAlignment(HorizontalAlignment.CENTER);
		doc.add(ps);
		Div div = new Div()
			     .setBorderLeft(new SolidBorder(2))
				 .setPaddingLeft(3)
				 .setMarginBottom(10);
		div.add(unCheck.setHeight(15).setWidth(15))
		.add(pr);
		doc.add(div);
		
		
		int beforeIndex = pdfDoc.getNumberOfPages();
		// Create table of contents
		doc.add(new AreaBreak(AreaBreakType.NEXT_PAGE));
        Paragraph p = new Paragraph("Table of Contents")
                .setFont(bold)
                .setDestination("toc");
        doc.add(p);
        List<TabStop> tabStops = new ArrayList<>();
        float[] columnWidtha = {50f,500f,50f};
	    Table table = new Table(UnitValue.createPercentArray(columnWidtha));
		table.useAllAvailableWidth();
		table.setFixedLayout();
		table.setBorder(Border.NO_BORDER);
        tabStops.add(new TabStop(580, TabAlignment.RIGHT, new DottedLine()));
        Integer counter = 1;
        for (SimpleEntry<String, SimpleEntry<String, Integer>> entry : toc) {
            SimpleEntry<String, Integer> text = entry.getValue();
//            p = new Paragraph()
//                    .addTabStops(tabStops)
//                    .add(text.getKey())
//                    .add(new Tab())
//                    .add(String.valueOf(text.getValue()))
//                    .setAction(PdfAction.createGoTo(entry.getKey()));
//            document.add(p);
            table.addCell(new Cell().add(new Paragraph(String.valueOf(counter))));
            table.addCell(new Cell().add(new Paragraph(text.getKey())));
            table.addCell(new Cell().add(new Paragraph(String.valueOf(text.getValue()))
            		.setAction(PdfAction.createGoTo(entry.getKey()))));
            counter++;
        }
        
        doc.add(table);
        doc.flush();
//         Move the table of contents to the first page
        int tocPageNumber = pdfDoc.getNumberOfPages();

        pdfDoc.movePage(tocPageNumber,2);
        pdfDoc.removePage(3);
//        pdfDoc.getPage(1).setPageLabel(PageLabelNumberingStyle.UPPERCASE_LETTERS,

     // footer code is here.
     	pdfDoc.addEventHandler(PdfDocumentEvent.END_PAGE, new TableFooterEventHandler(doc));
//     	PdfFont f1 = PdfFontFactory.createFont("MaterialIcons-Regular.ttf", "Identity-H", null, true);
     	Paragraph pw = new Paragraph("This is a tick box character: ");
     	FontProgram program = FontProgramFactory.createFont("src/main/resources/fonts/Wingdings-Regular-Font.ttf"); 
     	PdfFont fonts = PdfFontFactory.createFont(program,PdfEncodings.IDENTITY_H);
     	Text chunk = new Text("").setFont(fonts).setFontSize(14);
     	doc.add(new Paragraph(chunk));

     	Table boxTable = new Table(new float[] {6,44,6,44}).setPadding(0).setFixedLayout().useAllAvailableWidth()
                .setMargin(0);
     	
     	boxTable.addCell(new Cell().add(new Paragraph(String.valueOf('\u00FE')).setFont(fonts).setFontSize(22)));
     	boxTable.addCell(new Cell().add(new Paragraph("Yes")).setFont(font).setVerticalAlignment(VerticalAlignment.MIDDLE));
     	boxTable.addCell(new Cell().add(new Paragraph(String.valueOf('\u00A8')).setFont(fonts).setFontSize(22)));
     	boxTable.addCell(new Cell().add(new Paragraph("No")).setFont(font).setVerticalAlignment(VerticalAlignment.MIDDLE));
     	doc.add(boxTable);
     	Paragraph ppp = new Paragraph("   ");
     	ppp.add(String.valueOf('\u00A8')).setFont(fonts).setFontSize(25);
     	
     	
//     	doc.add(ppp.add("adf")).setFont(fonts).setFontSize(20).add(new Paragraph("   yes"));
     	Paragraph p1 = new Paragraph("No");
     	doc.add(ppp.add(new Tab()).add(p1.setFontSize(13).setFont(font).setVerticalAlignment(VerticalAlignment.TOP).setPadding(2)));
     	doc.add( new Paragraph("dfdf"));
		doc.close();
	}

	/*
	 * This is the Header of event handler to put Header on every page.
	 */

	private static class TableHeaderEventHandler implements IEventHandler {
		private Table table;
		private float tableHeight;
		private Document doc;

		public TableHeaderEventHandler(Document doc, Image leftImage, Image rightImage) {
			this.doc = doc;
			initTable(leftImage, rightImage);

			TableRenderer renderer = (TableRenderer) table.createRendererSubTree();
			renderer.setParent(new DocumentRenderer(doc));

			// Simulate the positioning of the renderer to find out how much space the
			// header table will occupy.
			LayoutResult result = renderer.layout(new LayoutContext(new LayoutArea(0, PageSize.A4)));
			tableHeight = result.getOccupiedArea().getBBox().getHeight();
		}

		@Override
		public void handleEvent(Event currentEvent) {
			PdfDocumentEvent docEvent = (PdfDocumentEvent) currentEvent;
			PdfDocument pdfDoc = docEvent.getDocument();
			PdfPage page = docEvent.getPage();
			PdfCanvas canvas = new PdfCanvas(page.newContentStreamBefore(), page.getResources(), pdfDoc);
			PageSize pageSize = pdfDoc.getDefaultPageSize();
//          float width = pageSize.getWidth() - doc.getRightMargin() - doc.getLeftMargin() + 20;
			float coordX = page.getPageSize().getX() + doc.getLeftMargin() - 13;// lower left x coordinate of page is
																				// zero always y also zero.
			float coordY = page.getPageSize().getTop() - doc.getTopMargin() - 10;
			float width = page.getPageSize().getWidth() - doc.getRightMargin() - 5;
			float height = getTableHeight();
			Rectangle rect = new Rectangle(coordX, 750, width, 80);
//          Rectangle rect = new Rectangle(22, 760, width, 80);
			new Canvas(canvas, rect).add(table).close();
		}

		public float getTableHeight() {
			return tableHeight;
		}

		private void initTable(Image leftImage, Image rightImage) {
//			table.setWidth(doc.getPdfDocument().getDefaultPageSize().getRight() - doc.getPdfDocument().getDefaultPageSize().getLeft() - doc.getLeftMargin() - doc.getRightMargin());

			float[] columnWidtha = { 500f, 500f };
			table = new Table(UnitValue.createPercentArray(columnWidtha));
			table.useAllAvailableWidth();
			table.setFixedLayout();
			table.addCell(new Cell().add(leftImage.setAutoScale(true)).setBorder(Border.NO_BORDER));
			table.addCell(new Cell().add(rightImage.setAutoScale(true).setHorizontalAlignment(HorizontalAlignment.RIGHT))
									.setBorder(Border.NO_BORDER));
		}
	}

	/*
	 * This is the footer of event handler to put footer on every page.
	 */
	private static class TableFooterEventHandler implements IEventHandler {
		private Table table;
		private Document doc;

		@SuppressWarnings("unused")
		public TableFooterEventHandler(Document doc) {
			this.doc = doc;
		}

		@SuppressWarnings("resource")
		@Override
		public void handleEvent(Event currentEvent) {
			PdfDocumentEvent docEvent = (PdfDocumentEvent) currentEvent;
			PdfDocument pdfDoc = docEvent.getDocument();
			int noOfPages = docEvent.getDocument().getNumberOfPages();
			int pageNo = pdfDoc.getPageNumber(docEvent.getPage());
			PdfPage page = docEvent.getPage();
//			if(pdfDoc.getPageNumber(page) == 2) {
//				table = new Table(1);
//			} else if (pdfDoc.getPageNumber(page) == 1) {
				table = new Table(new float[] { 300f, 300f, 300f }).useAllAvailableWidth();
				table.addCell(new Cell().add(new Paragraph("06.04.2022 v01").setTextAlignment(TextAlignment.LEFT)).setBorder(Border.NO_BORDER));
				table.addCell(new Cell().add(new Paragraph("Besichtigungsbericht").setTextAlignment(TextAlignment.CENTER)).setBorder(Border.NO_BORDER));
				table.addCell(new Cell().add(new Paragraph("Seite " + pdfDoc.getPageNumber(page) + "/" + noOfPages)
						.setTextAlignment(TextAlignment.RIGHT)).setBorder(Border.NO_BORDER));
//			} else {
//				table = new Table(new float[] { 300f, 300f, 300f }).useAllAvailableWidth();
//				table.addCell(new Cell().add(new Paragraph("06.04.2022 v01").setTextAlignment(TextAlignment.LEFT)).setBorder(Border.NO_BORDER));
//				table.addCell(new Cell().add(new Paragraph("Besichtigungsbericht").setTextAlignment(TextAlignment.CENTER)).setBorder(Border.NO_BORDER));
//				table.addCell(new Cell().add(new Paragraph("Seite " + (pdfDoc.getPageNumber(page) - 1) + "/" + (noOfPages-1))
//						.setTextAlignment(TextAlignment.RIGHT)).setBorder(Border.NO_BORDER));
//			}
			

			PdfCanvas canvas = new PdfCanvas(page.newContentStreamBefore(), page.getResources(), pdfDoc);
//          PdfCanvas canvass = new PdfCanvas(page);

			Rectangle rectangle = new Rectangle(35, 10, page.getPageSize().getWidth() - 67, 26);
			Canvas canvasObj = new Canvas(canvas, rectangle);
			canvasObj.add(table);
			canvasObj.close();
		}

	}
	private static PdfOutline createOutline(PdfOutline outline, PdfDocument pdf, String title, String name) {
        if (outline == null) {
            outline = pdf.getOutlines(false);
            outline = outline.addOutline(title);
            outline.addDestination(PdfDestination.makeDestination(new PdfString(name)));
        } else {
            PdfOutline kid = outline.addOutline(title);
            kid.addDestination(PdfDestination.makeDestination(new PdfString(name)));
        }

        return outline;
    }
	 private static class UpdatePageRenderer extends ParagraphRenderer {
	        protected SimpleEntry<String, Integer> entry;

	        public UpdatePageRenderer(Paragraph modelElement, SimpleEntry<String, Integer> entry) {
	            super(modelElement);
	            this.entry = entry;
	        }

	        @Override
	        public LayoutResult layout(LayoutContext layoutContext) {
	            LayoutResult result = super.layout(layoutContext);
	            entry.setValue(layoutContext.getArea().getPageNumber());
	            if (result.getStatus() != LayoutResult.FULL) {
	                if (null != result.getOverflowRenderer()) {
	                    result.getOverflowRenderer().setProperty(
	                            Property.LEADING,
	                            result.getOverflowRenderer().getModelElement().getDefaultProperty(Property.LEADING));
	                } else {
	                    // if overflow renderer is null, that could mean that the whole renderer will overflow
	                    setProperty(
	                            Property.LEADING,
	                            result.getOverflowRenderer().getModelElement().getDefaultProperty(Property.LEADING));
	                }
	            }
	            return result;
	        }
	    }
	 
		public Image getImage(String path) {
//			 URI left = new URI(rightUrl);
//		     URI right = new URI(leftUrl);
			//Image data for create image from path and send it to the Image object.
			ImageData imageFactory = null;
			try {
				imageFactory = ImageDataFactory.create(path);
			} catch (MalformedURLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return new Image(imageFactory);
		}
}
