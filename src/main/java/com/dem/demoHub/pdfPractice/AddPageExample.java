package com.dem.demoHub.pdfPractice;

import java.io.File;

import com.itextpdf.forms.PdfPageFormCopier;
import com.itextpdf.kernel.geom.PageSize;
import com.itextpdf.kernel.geom.Rectangle;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfPage;
import com.itextpdf.kernel.pdf.PdfReader;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.kernel.pdf.canvas.PdfCanvas;
import com.itextpdf.layout.Canvas;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.borders.SolidBorder;
import com.itextpdf.layout.element.AreaBreak;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.properties.AreaBreakType;

public class AddPageExample {
	 public static final String DEST = "C:\\Users\\mange\\Downloads\\1670479935269_survey_report_1670479904315%20(3).pdf";

	    public static final String SRC1 = "C:\\Users\\mange\\Downloads\\sample.pdf";
	    public static final String SRC2 = "C:\\Users\\mange\\Downloads\\text.pdf";
	    public static final String SRC3 = "C:\\Users\\mange\\Downloads\\sample.pdf";
	public static void main(String[] args) throws Exception {
		File tempDirectory = new File(System.getProperty("java.io.tmpdir"));
		File newSrcFile = new File(tempDirectory.getAbsolutePath() + File.separator + "tmpFile" + 78 + ".pdf");
		System.out.println(newSrcFile.getAbsolutePath());
		// create a new PDF document
		PdfDocument pdf = new PdfDocument(new PdfWriter(newSrcFile));

		// create a new document with the PDF document as its target
		Document doc = new Document(pdf);

		// add some content to the document
		doc.add(new Paragraph("Page 1"));

		// create a new page using addNewPage()
		pdf.addNewPage();
		doc.add(new Paragraph("Page 2 (added using addNewPage())"));

		// create a new page using areaBreak().nextPage()
		doc.add(new AreaBreak(AreaBreakType.NEXT_PAGE));
		doc.add(new Paragraph("Page 3 (added using areaBreak().nextPage())"));
		PdfDocument tocDoc = new PdfDocument(new PdfReader(SRC3));
        tocDoc.copyPagesTo(1, tocDoc.getNumberOfPages(), pdf, new PdfPageFormCopier());
        tocDoc.close();
        PdfPage page = pdf.addNewPage(pdf.getNumberOfPages()+1,PageSize.A4);
        PdfCanvas canvas = new PdfCanvas(page.newContentStreamBefore(), page.getResources(), pdf);
		PageSize pageSize = pdf.getDefaultPageSize();
        float coordX = page.getPageSize().getX() + doc.getLeftMargin() ;// lower left x coordinate of page is
		// zero always y also zero.
		float coordY = page.getPageSize().getTop() - doc.getTopMargin() - 10;
		float width = page.getPageSize().getWidth() - doc.getRightMargin() - 5;
//		Rectangle rect = new Rectangle(coordX, 750, width, 80);
		Rectangle rect = new Rectangle(coordX, 40, width, 670);
		canvas.stroke().resetStrokeColorGray();
		String paragraph = "now it will work.";
		for(int i = 0;i<23; i++) {
			paragraph = paragraph.concat(paragraph) + "\n";
		}
		//Rectangle rect = new Rectangle(22, 760, width, 80);
		new Canvas(canvas, rect).add(new Paragraph(paragraph)).setBorder(new SolidBorder(1)).close();
        doc.add(new Paragraph("here you are.................."));
        doc.flush();
        doc.close();
        
		// close the document
		doc.close();
	}
}
