package com.dem.demoHub.pdfPractice;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.AbstractMap.SimpleEntry;
import java.util.ArrayList;
import java.util.List;

import com.itextpdf.io.font.constants.StandardFonts;
import com.itextpdf.kernel.font.PdfFont;
import com.itextpdf.kernel.font.PdfFontFactory;
import com.itextpdf.kernel.geom.PageSize;
import com.itextpdf.kernel.pdf.PageLabelNumberingStyle;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfOutline;
import com.itextpdf.kernel.pdf.PdfString;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.kernel.pdf.action.PdfAction;
import com.itextpdf.kernel.pdf.canvas.draw.DottedLine;
import com.itextpdf.kernel.pdf.navigation.PdfDestination;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.borders.Border;
import com.itextpdf.layout.element.AreaBreak;
import com.itextpdf.layout.element.Cell;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.element.TabStop;
import com.itextpdf.layout.element.Table;
import com.itextpdf.layout.layout.LayoutContext;
import com.itextpdf.layout.layout.LayoutResult;
import com.itextpdf.layout.properties.AreaBreakType;
import com.itextpdf.layout.properties.TabAlignment;
import com.itextpdf.layout.properties.TextAlignment;
import com.itextpdf.layout.properties.UnitValue;
import com.itextpdf.layout.properties.VerticalAlignment;
import com.itextpdf.layout.renderer.ParagraphRenderer;

public class TableOfContent3Example {

	public static final String DEST = "C:\\Users\\mange\\OneDrive\\Desktop\\New folder\\pdf\\_sign1.pdf";

    public static void main(String[] args) throws Exception {
        File file = new File(DEST);
        file.getParentFile().mkdirs();

        new TableOfContent3Example().manipulatePdf(DEST);
    }

    public void manipulatePdf(String dest) throws Exception {
        PdfFont font = PdfFontFactory.createFont(StandardFonts.TIMES_ROMAN);
        PdfFont bold = PdfFontFactory.createFont(StandardFonts.HELVETICA_BOLD);
        PdfDocument pdfDoc = new PdfDocument(new PdfWriter(dest));
        Document document = new Document(pdfDoc,PageSize.A4,false);
        document
                .setTextAlignment(TextAlignment.JUSTIFIED)
                .setFont(font)
                .setFontSize(11);
        pdfDoc.addNewPage();
        SimpleEntry<String, Integer> titlePage = null;
        List<SimpleEntry<String, SimpleEntry<String, Integer>>> toc = new ArrayList<>();
        Paragraph pp = new Paragraph("1.first");
       
        titlePage = new SimpleEntry("1.first", pdfDoc.getNumberOfPages());       
        
        pp.setFont(bold)
          .setFontSize(12)
          .setKeepWithNext(true)
          .setDestination("page1")
          .setNextRenderer(new UpdatePageRenderer(pp, titlePage));
        toc.add(new SimpleEntry("page1",titlePage));
        document.add(pp);
        
        document.add(new AreaBreak(AreaBreakType.NEXT_PAGE));
        
        pp = new Paragraph("2.second");
        titlePage = new SimpleEntry("2.second", pdfDoc.getNumberOfPages());       
       
        pp.setFont(bold)
          .setFontSize(12)
          .setKeepWithNext(true)
          .setDestination("page2")
          .setNextRenderer(new UpdatePageRenderer(pp, titlePage));
        toc.add(new SimpleEntry("page2",titlePage));
        document.add(pp);
        
//        TODO:above two documents are repeating itself so next time try this one solution:
        /*create the list of section and subsection and then send it to createPdfWithOutlines() this method.
         * and also send the current file which is already created and only loop through the file line by line.
         * then search only for the paragraph and which are already inside the list which created above.
         * and other 
        */
        
        // Create table of contents
        document.add(new AreaBreak());
        Paragraph p = new Paragraph("Table of Contents")
                .setFont(bold)
                .setDestination("toc");
        document.add(p);
        List<TabStop> tabStops = new ArrayList<>();
        float[] columnWidtha = {50f,500f,50f};
	    Table table = new Table(UnitValue.createPercentArray(columnWidtha));
		table.useAllAvailableWidth();
		table.setFixedLayout();
		table.setBorder(Border.NO_BORDER);
        tabStops.add(new TabStop(580, TabAlignment.RIGHT, new DottedLine()));
        Integer counter = 1;
        for (SimpleEntry<String, SimpleEntry<String, Integer>> entry : toc) {
            SimpleEntry<String, Integer> text = entry.getValue();
//            p = new Paragraph()
//                    .addTabStops(tabStops)
//                    .add(text.getKey())
//                    .add(new Tab())
//                    .add(String.valueOf(text.getValue()))
//                    .setAction(PdfAction.createGoTo(entry.getKey()));
//            document.add(p);
            table.addCell(new Cell().add(new Paragraph(String.valueOf(counter))));
            table.addCell(new Cell().add(new Paragraph(text.getKey())));
            table.addCell(new Cell().add(new Paragraph(String.valueOf(text.getValue()))
            		.setAction(PdfAction.createGoTo(entry.getKey()))));
            counter++;
        }
        document.add(table);
        // Move the table of contents to the first page
        int tocPageNumber = pdfDoc.getNumberOfPages();
        pdfDoc.movePage(tocPageNumber, 1);

        int n = pdfDoc.getNumberOfPages();
        Paragraph footer;
        for (int page = 1; page <= n; page++) {
            footer = new Paragraph(String.format("Page %s of %s", page, n));
            document.showTextAligned(footer, 297.5f, 20, page, TextAlignment.CENTER, VerticalAlignment.MIDDLE, 0);
        }
        
        document.close();
    }

    private static void createPdfWithOutlines(String path, Document document,
            List<SimpleEntry<String, SimpleEntry<String, Integer>>> toc, PdfFont titleFont) throws Exception {
        PdfDocument pdfDocument = document.getPdfDocument();

        try (BufferedReader br = new BufferedReader(new FileReader(path))) {
            String line;
            boolean title = true;
            int counter = 0;
            PdfOutline outline = null;
            while ((line = br.readLine()) != null) {
                Paragraph p = new Paragraph(line);
                p.setKeepTogether(true);
                if (title) {
                    String name = String.format("title%02d", counter++);
//                    outline = createOutline(outline, pdfDocument, line, name);
                    SimpleEntry<String, Integer> titlePage = new SimpleEntry(line, pdfDocument.getNumberOfPages());
                    p
                            .setFont(titleFont)
                            .setFontSize(12)
                            .setKeepWithNext(true)
                            .setDestination(name)

                            // Add the current page number to the table of contents list
                            .setNextRenderer(new UpdatePageRenderer(p, titlePage));
                    document.add(p);
                    toc.add(new SimpleEntry(name, titlePage));
                    title = false;
                } else {
                    p.setFirstLineIndent(36);
                    if (line.isEmpty()) {
                        p.setMarginBottom(12);
                        title = true;
                    } else {
                        p.setMarginBottom(0);
                    }

                    document.add(p);
                }
            }
        }
    }
    
    private static PdfOutline createOutline(PdfOutline outline, PdfDocument pdf, String title, String name) {
        if (outline == null) {
            outline = pdf.getOutlines(true);
            outline = outline.addOutline(title);
            outline.addDestination(PdfDestination.makeDestination(new PdfString(name)));
        } else {
            PdfOutline kid = outline.addOutline(title);
            kid.addDestination(PdfDestination.makeDestination(new PdfString(name)));
        }

        return outline;
    }

    private static class UpdatePageRenderer extends ParagraphRenderer {
        protected SimpleEntry<String, Integer> entry;

        public UpdatePageRenderer(Paragraph modelElement, SimpleEntry<String, Integer> entry) {
            super(modelElement);
            this.entry = entry;
        }

        @Override
        public LayoutResult layout(LayoutContext layoutContext) {
            LayoutResult result = super.layout(layoutContext);
            entry.setValue(layoutContext.getArea().getPageNumber());
            return result;
        }
    }
}
