package com.dem.demoHub.pdfPractice;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.util.AbstractMap.SimpleEntry;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.swing.text.StyleConstants.FontConstants;

import org.apache.commons.io.FileUtils;
import org.apache.poi.sl.usermodel.TableCell.BorderEdge;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.itextpdf.forms.PdfPageFormCopier;
import com.itextpdf.io.font.FontProgram;
import com.itextpdf.io.font.FontProgramFactory;
import com.itextpdf.io.font.PdfEncodings;
import com.itextpdf.io.font.constants.StandardFonts;
import com.itextpdf.io.image.ImageData;
import com.itextpdf.io.image.ImageDataFactory;
import com.itextpdf.kernel.colors.Color;
import com.itextpdf.kernel.colors.DeviceCmyk;
import com.itextpdf.kernel.colors.WebColors;
import com.itextpdf.kernel.events.Event;
import com.itextpdf.kernel.events.IEventHandler;
import com.itextpdf.kernel.events.PdfDocumentEvent;
import com.itextpdf.kernel.font.PdfFont;
import com.itextpdf.kernel.font.PdfFontFactory;
import com.itextpdf.kernel.geom.PageSize;
import com.itextpdf.kernel.geom.Rectangle;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfOutline;
import com.itextpdf.kernel.pdf.PdfPage;
import com.itextpdf.kernel.pdf.PdfReader;
import com.itextpdf.kernel.pdf.PdfString;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.kernel.pdf.action.PdfAction;
import com.itextpdf.kernel.pdf.canvas.PdfCanvas;
import com.itextpdf.kernel.pdf.canvas.draw.DottedLine;
import com.itextpdf.kernel.pdf.navigation.PdfDestination;
import com.itextpdf.kernel.pdf.navigation.PdfExplicitDestination;
import com.itextpdf.kernel.utils.PdfMerger;
import com.itextpdf.layout.Canvas;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.borders.Border;
import com.itextpdf.layout.borders.SolidBorder;
import com.itextpdf.layout.element.AreaBreak;
import com.itextpdf.layout.element.Cell;
import com.itextpdf.layout.element.Image;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.element.TabStop;
import com.itextpdf.layout.element.Table;
import com.itextpdf.layout.element.Text;
import com.itextpdf.layout.layout.LayoutArea;
import com.itextpdf.layout.layout.LayoutContext;
import com.itextpdf.layout.layout.LayoutResult;
import com.itextpdf.layout.properties.AreaBreakType;
import com.itextpdf.layout.properties.BorderCollapsePropertyValue;
import com.itextpdf.layout.properties.BorderRadius;
import com.itextpdf.layout.properties.HorizontalAlignment;
import com.itextpdf.layout.properties.Property;
import com.itextpdf.layout.properties.TabAlignment;
import com.itextpdf.layout.properties.TextAlignment;
import com.itextpdf.layout.properties.UnitValue;
import com.itextpdf.layout.properties.VerticalAlignment;
import com.itextpdf.layout.renderer.DocumentRenderer;
import com.itextpdf.layout.renderer.ParagraphRenderer;
import com.itextpdf.layout.renderer.TableRenderer;

public class ItextHelper {
	private static final Logger logger = LoggerFactory.getLogger(ItextHelper.class);
	
	public static final String DEST = "C:\\Users\\mange\\OneDrive\\Desktop\\New folder\\pdf\\_sign1.pdf";
	public static final String newFileDest12 = "C:\\Users\\mange\\Downloads\\1679304558471_survey_report_1679304506599.pdf";
	public static final String newFileDest1 = "C:\\Users\\mange\\Downloads\\1674813426338_testpdffile.pdf";
	public static final String newFileDest2 = "C:\\Users\\mange\\Downloads\\1674813426338_testpdffile.pdf";
	public String[] fileArray = {newFileDest12,newFileDest2};
//	public static final String DESTOFMERGE = "https://rcsdevstorage.blob.core.windows.net/rcsdevcontainer1/parent/böser´s%20spargelrestaurant%20inhaber%20wolfgang%20böser/docs/survey/1670479935269_survey_report_1670479904315.pdf";
	private boolean server = true;		
	public static void main(String[] args) {
		new ItextHelper().manipulatePdf(DEST);
		
		String criteriaName = "starting from today itself";
		String[] arr = criteriaName.split(" ");
		StringBuilder builder = new StringBuilder();
		for(int i = 0; i <arr.length; i++) {
			String firstLetter = arr[i].substring(0, 1).toUpperCase();
			if(i == 0) {
				builder.append(firstLetter + arr[i].substring(1));
			} else {
				builder.append(" " + firstLetter + arr[i].substring(1));
			}
			
		}
		System.out.println(builder.toString());
	}
	//this method is for getting paragraph Subsection with it's border
	public static Paragraph getParagraphForSubSections(Integer counter,Integer subCounter,String content,List<String> index) {
		String para = new String(counter+"."+subCounter+" "+content);
		index.add(para);
		return new Paragraph(para).setFontSize(8)
			    .setBorder(new SolidBorder(0.5f))
				.setBackgroundColor(WebColors.getRGBColor("#ffc000"))
				.setPaddingLeft(2)
				.setBorderRight(Border.NO_BORDER);
	}
	
	//this method is for adding TOC(table of contet) for pdf
	
	public static void addIndexing(Document doc, Paragraph para, String dest,
		List<SimpleEntry<String, SimpleEntry<String, Integer>>> toc, PdfFont font, SimpleEntry<String, Integer> titlePage) {

		para.setFont(font)
		    .setFontSize(12)
			.setDestination(dest)
			.setNextRenderer(new UpdatePageRenderer(para, titlePage));
		toc.add(new SimpleEntry(dest, titlePage));
		
	}
	
	public static Paragraph getMainSectionParagraph(Integer counter,String section,List<String> index) {
		String para = new String(counter+". "+section);
		index.add(para);
		return  new Paragraph(para)
				.setTextAlignment(TextAlignment.LEFT).setFontSize(20)
				.setFontColor(WebColors.getRGBColor("#595959"))
				.setBold().setMarginBottom(20).setMarginLeft(15);
	}
	
	public static Paragraph getMainSectionParagraph(Integer sectionCounter,Integer subsectionCounter,String section) {
		return  new Paragraph(sectionCounter+"."+subsectionCounter+" "+section)
				.setTextAlignment(TextAlignment.LEFT).setFontSize(13)
				.setFontColor(WebColors.getRGBColor("#595959"));
	}

	public static Table getTableForShowingRealData() {
		float[] columnWidth = {100f,400f,200f};
		Table table3 = new Table(UnitValue.createPercentArray(columnWidth));
		table3.setWidth(UnitValue.createPercentValue(100));
		table3.setFixedLayout();
		return table3;
		
	}
	
	public static Table getTableForCheckboxValue(PdfFont font, String check) {
		FontProgram program = null;
		try {
			program = FontProgramFactory.createFont("src/main/resources/fonts/Wingdings-Regular-Font.ttf");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
     	PdfFont fonts = PdfFontFactory.createFont(program,PdfEncodings.IDENTITY_H);
     	Table boxTable = new Table(new float[] {25,45,45,45}).setPadding(0).setFixedLayout().useAllAvailableWidth()
                .setMargin(0);
     	//here both checkboxes are empty.
     	if (null == check || check.isEmpty()) {
     		boxTable.addCell(new Cell().add(new Paragraph(String.valueOf('\u00A8')).setFont(fonts).setFontSize(18)).setBorder(Border.NO_BORDER));
         	boxTable.addCell(new Cell().add(new Paragraph("Yes")).setFont(font).setVerticalAlignment(VerticalAlignment.MIDDLE).setBorder(Border.NO_BORDER));
         	boxTable.addCell(new Cell().add(new Paragraph(String.valueOf('\u00A8')).setFont(fonts).setFontSize(18)).setBorder(Border.NO_BORDER));
         	boxTable.addCell(new Cell().add(new Paragraph("No")).setFont(font).setVerticalAlignment(VerticalAlignment.MIDDLE).setBorder(Border.NO_BORDER));
     	} else if (check.contains("true")) {
     		boxTable.addCell(new Cell().add(new Paragraph(String.valueOf('\u00FE')).setFont(fonts).setFontSize(18)).setBorder(Border.NO_BORDER));
         	boxTable.addCell(new Cell().add(new Paragraph("Yes")).setFont(font).setVerticalAlignment(VerticalAlignment.MIDDLE).setBorder(Border.NO_BORDER));
         	boxTable.addCell(new Cell().add(new Paragraph(String.valueOf('\u00A8')).setFont(fonts).setFontSize(18)).setBorder(Border.NO_BORDER));
         	boxTable.addCell(new Cell().add(new Paragraph("No")).setFont(font).setVerticalAlignment(VerticalAlignment.MIDDLE).setBorder(Border.NO_BORDER));
     	} else if(check.contains("false")) {
     		//if value is false
     		boxTable.addCell(new Cell().add(new Paragraph(String.valueOf('\u00A8')).setFont(fonts).setFontSize(18)).setBorder(Border.NO_BORDER));
         	boxTable.addCell(new Cell().add(new Paragraph("Yes")).setFont(font).setVerticalAlignment(VerticalAlignment.MIDDLE).setBorder(Border.NO_BORDER));
         	boxTable.addCell(new Cell().add(new Paragraph(String.valueOf('\u00FE')).setFont(fonts).setFontSize(18)).setBorder(Border.NO_BORDER));
         	boxTable.addCell(new Cell().add(new Paragraph("No")).setFont(font).setVerticalAlignment(VerticalAlignment.MIDDLE).setBorder(Border.NO_BORDER));
     	}
     	
     	return boxTable;
	}
	
	public static Cell getCell() {
		return new Cell();
	}
	protected void manipulatePdf(String dest){
	  try {	
		File file = new File(DEST);
		file.getParentFile().mkdirs();
		PdfWriter pdfWriter = new PdfWriter(dest);
		File newSrcFile = null;
		if(true) {
			File tempDirectory = new File(System.getProperty("java.io.tmpdir"));
			newSrcFile = new File(tempDirectory.getAbsolutePath() + File.separator + "tmpFile" + 78 + ".pdf");
			InputStream stream = new FileInputStream(file);
			FileUtils.copyInputStreamToFile(stream, newSrcFile);
//			fileArray[2] = newSrcFile.getAbsolutePath();
		}
		String counterName = "SurveyName";
		int pageCount = 1;
		//existing doc
		PdfDocument pdfDoc = new PdfDocument(pdfWriter);
		
		// third parameter false is for not flushing pages immediately.
		Document doc = new Document(pdfDoc, PageSize.A4, false);
		pdfDoc.addNewPage();
		
		//these two fonts are from TOC code base.
		PdfFont font = PdfFontFactory.createFont(StandardFonts.TIMES_ROMAN);
	    PdfFont bold = PdfFontFactory.createFont(StandardFonts.HELVETICA_BOLD);
	    
	    String HEAD_RIGHT = "C:\\Users\\mange\\Downloads\\right.jpeg";
		String HEAD_LEFT = "C:\\Users\\mange\\Downloads\\left.jpeg";
		String FIRST_PAGE_IMAGE = "C:\\Users\\mange\\Downloads\\firstPageImage.jpeg";
	    
		Image leftImage = getImage(HEAD_LEFT);
		Image rightImage = getImage(HEAD_RIGHT);
		
		Image firstPage = getImage(FIRST_PAGE_IMAGE);
		doc.add(new Paragraph("First Page:"));
//		//2nd page will be empty
//		doc.add(new AreaBreak(AreaBreakType.NEXT_PAGE));
		
		//3rd page and print third page
		doc.add(new AreaBreak(AreaBreakType.NEXT_PAGE));
		SimpleEntry<String, Integer> titlePage = null;
		List<SimpleEntry<String, SimpleEntry<String, Integer>>> toc = new ArrayList<>();
		Paragraph para = new Paragraph();
		
		String mainSection = counterName+pageCount;
		String sec = new String(pageCount + ". " + mainSection);
		para = new Paragraph(sec).setTextAlignment(TextAlignment.LEFT).setFontSize(20)
				.setFontColor(WebColors.getRGBColor("#595959")).setBold().setMarginBottom(20).setMarginLeft(15);

		titlePage = new SimpleEntry<>(sec, pdfDoc.getNumberOfPages());
		para.setFont(font).setFontSize(12).setDestination(mainSection)
				.setNextRenderer(new UpdatePageRenderer(para, titlePage));

		toc.add(new SimpleEntry(mainSection, titlePage));
		pageCount++;
		mainSection=counterName+pageCount;
		doc.add(para);
		//3rd page.
		doc.add(new AreaBreak(AreaBreakType.NEXT_PAGE));
		sec = new String(pageCount + ". " + mainSection);
		para = new Paragraph(sec).setTextAlignment(TextAlignment.LEFT).setFontSize(20)
				.setFontColor(WebColors.getRGBColor("#595959")).setBold().setMarginBottom(20).setMarginLeft(15);

		titlePage = new SimpleEntry<>(sec, pdfDoc.getNumberOfPages());
		para.setFont(font).setFontSize(12).setDestination(mainSection)
				.setNextRenderer(new UpdatePageRenderer(para, titlePage));

		toc.add(new SimpleEntry(mainSection, titlePage));
		pageCount++;
		mainSection=counterName+pageCount;
		doc.add(para);
		doc.add(new Paragraph("you are testing TOC... 3rd Page:"));
		
//		doc.add(new AreaBreak(AreaBreakType.NEXT_PAGE));
		Table imageTable = new Table(UnitValue.createPercentArray(new float[] {500f,500f}));
		imageTable.setFixedLayout().setWidth(UnitValue.createPercentValue(100));
		imageTable.setBorderCollapse(BorderCollapsePropertyValue.SEPARATE);
		imageTable.setVerticalBorderSpacing(10);
		imageTable.setHorizontalBorderSpacing(10);

		for (int j = 0; j < 2; j++) {
			for (int i = 0; i < 2; i++) {
				imageTable.addCell(new Cell().add(firstPage.setHeight(260f).setWidth(241f))
								.add(new Paragraph("Cell row " + j + "col asdkljflksajdfluiouetillkajfl;kajfslkjfkjdfkfjdk " + i)
								.setTextAlignment(TextAlignment.CENTER).setBackgroundColor(WebColors.getRGBColor("#afcddc")))
								.setBorderRadius(new BorderRadius(5f)));
			}
		}
//		doc.add(imageTable);
		
		// Header code is here.
		TableHeaderEventHandler handler = new TableHeaderEventHandler(doc, leftImage, rightImage);
		pdfDoc.addEventHandler(PdfDocumentEvent.END_PAGE, handler);

		// footer code is here.
//		pdfDoc.addEventHandler(PdfDocumentEvent.END_PAGE, new TableFooterEventHandler(doc));

		// Calculate top margin to be sure that the table will fit the margin.
		float topMargin = 20 + handler.getTableHeight();
		doc.setMargins(100, 36, 36, 36);
		
		sec = new String(pageCount + ". " + mainSection);
		para = new Paragraph(sec).setTextAlignment(TextAlignment.LEFT).setFontSize(20)
				.setFontColor(WebColors.getRGBColor("#595959")).setBold().setMarginBottom(20).setMarginLeft(15);

		titlePage = new SimpleEntry<>(sec, pdfDoc.getNumberOfPages());
		para.setFont(font).setFontSize(12).setDestination(mainSection)
				.setNextRenderer(new UpdatePageRenderer(para, titlePage));

		toc.add(new SimpleEntry(mainSection, titlePage));
		pageCount++;
		mainSection=counterName+pageCount;
		doc.add(para);
		
		// first page two paragraph or heading we can say.
		doc.add(new Paragraph("Industrielle Sachversicherung")
		   .setTextAlignment(TextAlignment.RIGHT)
		   .setFontSize(20)
		   .setFontColor(WebColors.getRGBColor("#595959"))
		   .setBold().setMarginBottom(20));
		Text text1 = new Text("/ ");
	    text1.setFontSize(32)
			 .setFontColor(WebColors.getRGBColor("#ffc000"));
		
		Text text2 = new Text("BESICHTIGUNGSBERICHT");
	    text2.setFontSize(32)
			 .setFontColor(WebColors.getRGBColor("#595959"));
	    
	    Paragraph fp = new Paragraph();
	    fp.add(text1)
	      .add(text2)
	      .setTextAlignment(TextAlignment.RIGHT)
	      .setMarginBottom(20);
	    doc.add(fp);
	    Table table22 = new Table(new float[] {20,80});
	    table22.setFixedLayout().useAllAvailableWidth();
	    table22.setMargins(20, 20, 20, 20);
	    table22.addCell(new Cell().add(new Paragraph("name:")));
	    table22.addCell(new Cell().add(new Paragraph("_______________")));
		doc.add(firstPage);
//		doc.add(table22);
		
		int object_type = 6;

		//add toc for this pdf
		doc.add(new AreaBreak(AreaBreakType.NEXT_PAGE));
		sec = new String(pageCount + ". " + mainSection);
		para = new Paragraph(sec).setTextAlignment(TextAlignment.LEFT).setFontSize(20)
				.setFontColor(WebColors.getRGBColor("#595959")).setBold().setMarginBottom(20).setMarginLeft(15);

		titlePage = new SimpleEntry<>(sec, pdfDoc.getNumberOfPages());
		para.setFont(font).setFontSize(12).setDestination(mainSection)
				.setNextRenderer(new UpdatePageRenderer(para, titlePage));

		toc.add(new SimpleEntry(mainSection, titlePage));
		pageCount++;
		mainSection=counterName+pageCount;
		doc.add(para);
		for (int i = 0; i < 10; i++) {
			doc.add(new Paragraph("Hello World!"));
		}
		doc.add(new Paragraph("heaay you itext...."));
		
		sec = new String(pageCount + ". " + mainSection);
		para = new Paragraph(sec).setTextAlignment(TextAlignment.LEFT).setFontSize(20)
				.setFontColor(WebColors.getRGBColor("#595959")).setBold().setMarginBottom(20).setMarginLeft(15);

		titlePage = new SimpleEntry<>(sec, pdfDoc.getNumberOfPages());
		para.setFont(font).setFontSize(12).setDestination(mainSection)
				.setNextRenderer(new UpdatePageRenderer(para, titlePage));

		toc.add(new SimpleEntry(mainSection, titlePage));
		pageCount++;
		mainSection=counterName+pageCount;
		doc.add(para);
		

		doc.add(new AreaBreak(AreaBreakType.NEXT_PAGE));

		sec = new String(pageCount + ". " + mainSection);
		para = new Paragraph(sec).setTextAlignment(TextAlignment.LEFT).setFontSize(20)
				.setFontColor(WebColors.getRGBColor("#595959")).setBold().setMarginBottom(20).setMarginLeft(15);

		titlePage = new SimpleEntry<>(sec, pdfDoc.getNumberOfPages());
		para.setFont(font).setFontSize(12).setDestination(mainSection)
				.setNextRenderer(new UpdatePageRenderer(para, titlePage));

		toc.add(new SimpleEntry(mainSection, titlePage));
		pageCount++;
		mainSection=counterName+pageCount;
		doc.add(para);
		int in = 2;
		String abc = "adbc";
		for(int i = 0;i<3;i++) {
			sec = new String(pageCount + ". " + mainSection);
			para = new Paragraph(sec).setTextAlignment(TextAlignment.LEFT).setFontSize(20)
					.setFontColor(WebColors.getRGBColor("#595959")).setBold().setMarginBottom(20).setMarginLeft(15);

			titlePage = new SimpleEntry<>(sec, pdfDoc.getNumberOfPages());
			para.setFont(font).setFontSize(12).setDestination(mainSection)
					.setNextRenderer(new UpdatePageRenderer(para, titlePage));

			toc.add(new SimpleEntry(mainSection, titlePage));
			pageCount++;
			mainSection=counterName+pageCount;
			doc.add(para);
		}
		
		int contentPageNumber = pdfDoc.getNumberOfPages();
		
		
		doc.add(new Paragraph("you are a genious."));
		
		int tocPageNumber = pdfDoc.getNumberOfPages();
		
		// here the pages are the first document pages. after these pages new document start.
		
		// new pdf documents
//		PdfDocument newPdfDoc = new PdfDocument(new PdfReader(newFileDest1));
		PdfMerger merger = new PdfMerger(pdfDoc);
		doc.add(new AreaBreak(AreaBreakType.NEXT_PAGE));
		tocPageNumber = pdfDoc.getNumberOfPages();
		Document mergeDoc = null;
//		File fileOuput = getTempFile(23);
		for(int i = 0; i<fileArray.length; i++) {
			File fileOuput = getTempFile(23);
			PdfFont fontMerge = PdfFontFactory.createFont(StandardFonts.TIMES_ROMAN);
			Paragraph mergePara = null;
//			mergePara = new Paragraph(i+".copied new document"+in++);
//			titlePage = new SimpleEntry(i+".copied new document", pdfDoc.getNumberOfPages());
			tocPageNumber = pdfDoc.getNumberOfPages();
			
//			merger.merge(newPdfDoc, 1, newPdfDoc.getNumberOfPages());
			PdfDocument newPdfDoc = new PdfDocument(new PdfReader(fileArray[i]),new PdfWriter(fileOuput));
			
//			PdfExplicitDestination ds = new PdfExplicitDestination().createFit(newPdfDoc.getPage(1));
			newPdfDoc.close();
//			PdfPage page = newPdfDoc.getPage(1);
//			mergeDoc = new Document(newPdfDoc);
//			mergeDoc.add(mergePara);
//			newPdfDoc.close();
			
			PdfDocument newPdfDocMerge = new PdfDocument(new PdfReader(fileOuput));
			for(int i1 = 0; i1<1; i1++) {
				newPdfDocMerge.copyPagesTo(1, newPdfDocMerge.getNumberOfPages(), pdfDoc,new PdfPageFormCopier());	
				
			}
			int pageNo = pdfDoc.getNumberOfPages();
//			PdfPage page2 = pdfDoc.getPage(pageNo);
			PdfPage page2 = pdfDoc.getPage(tocPageNumber + 1);
			PdfDestination destination = PdfExplicitDestination.createFit(page2);
			 
			sec = new String(pageCount + ". " + mainSection);
			mergePara = new Paragraph(sec).setTextAlignment(TextAlignment.LEFT).setFontSize(20)
					.setFontColor(WebColors.getRGBColor("#595959")).setBold().setMarginBottom(20).setMarginLeft(15);
			titlePage = new SimpleEntry<>(sec, pdfDoc.getNumberOfPages());
			// first param is section of the page and 2nd is current pageNumber.
			mergePara.setFont(fontMerge).setFontSize(12).setDestination(mainSection)
					.setNextRenderer(new UpdatePageRenderer(mergePara, titlePage));

			toc.add(new SimpleEntry(mainSection, titlePage));
			pageCount++;
			mainSection = counterName + pageCount;
			
			PdfCanvas canvas = new PdfCanvas(page2);
			
//			PdfCanvas canvas = new PdfCanvas(pdfMergedPage.newContentStreamBefore(), pdfMergedPage.getResources(), surveyPdfDoc);
//        	 PdfCanvas canvass = new PdfCanvas(page);

			Rectangle rectangle = new Rectangle(35, 10, page2.getPageSize().getTop() - 36, 740);
			Canvas canvasObj = new Canvas(canvas, rectangle);
			Paragraph docPara = mergePara;
			docPara.setProperty(Property.DESTINATION, mainSection);
			canvasObj.add(docPara);
			canvas.release();
//			PdfFont createFont = PdfFontFactory.createFont(StandardFonts.TIMES_ROMAN);
//	        canvas.beginText()
//	        	.setFontAndSize(createFont, 20)
//	        	.showText(mainSection)
//	            .moveText(36, page2.getPageSize().getTop() - 36)
//	            .endText();
//	        canvas.release();
			
//	        page2.flush();
	       
//			pageNo = pdfDoc.getNumberOfPages();
//			mergeDoc.close();
			int size = toc.size();
			SimpleEntry<String, SimpleEntry<String, Integer>> tocMerged = toc.get(size-1);
			SimpleEntry<String, Integer> entry = tocMerged.getValue();
			entry.setValue(tocPageNumber + 1);
			
			
			
			newPdfDocMerge.close();
			fileOuput.delete();
//			doc.add(new AreaBreak(AreaBreakType.NEXT_PAGE));
			newPdfDoc.close();
			in++;
		}
		/*
		** TODO:IMP after the documents added inside the existing doc the all the new docs are closed
		** but the existing is still ON for indexing so all the pages were added in above after
		** page tocPageNumber(look variable above) it actually add after index page
		** in that way we can have our index page in first document and other pages in are after index.
		*/
//		int beforeIndex = pdfDoc.getNumberOfPages();
		int beforeIndex1 = pdfDoc.getNumberOfPages();
		
		// Create table of contents
//		doc.add(new AreaBreak(AreaBreakType.NEXT_PAGE));
		int beforeIndex = pdfDoc.getNumberOfPages();
//		doc.add(new AreaBreak(AreaBreakType.NEXT_PAGE));
		PdfPage lastPage = pdfDoc.getLastPage();
		pdfDoc.addNewPage();
		Document newDoc = new Document(pdfDoc);
        Paragraph p = new Paragraph("Table of Contents")
                .setFont(bold)
                .setDestination("toc");
        doc.add(p);
        List<TabStop> tabStops = new ArrayList<>();
        float[] columnWidtha = {50f,500f,50f};
	    Table table = new Table(UnitValue.createPercentArray(columnWidtha));
		table.useAllAvailableWidth();
		table.setFixedLayout();
		table.setBorder(Border.NO_BORDER);
        tabStops.add(new TabStop(580, TabAlignment.RIGHT, new DottedLine()));
        Integer counter = 1;
        for (SimpleEntry<String, SimpleEntry<String, Integer>> entry : toc) {
            SimpleEntry<String, Integer> text = entry.getValue();
//            p = new Paragraph()
//                    .addTabStops(tabStops)
//                    .add(text.getKey())
//                    .add(new Tab())
//                    .add(String.valueOf(text.getValue()))
//                    .setAction(PdfAction.createGoTo(entry.getKey()));
//            document.add(p);
            table.addCell(new Cell().add(new Paragraph(String.valueOf(counter))));
            table.addCell(new Cell().add(new Paragraph(text.getKey())));
            PdfPage page = pdfDoc.getPage(text.getValue());
            table.addCell(new Cell().add(new Paragraph(String.valueOf(text.getValue()))
            		.setAction(PdfAction.createGoTo(PdfExplicitDestination.createFit(page)))));//entry.getKey()
            counter++;
        }
        
//        doc.add(table);
        newDoc.add(table);
        newDoc.flush();
//        doc.add(new AreaBreak(AreaBreakType.NEXT_PAGE));
//        doc.add(new AreaBreak(AreaBreakType.NEXT_PAGE));
//        doc.flush();
//      newPdfDoc.close();
//         Move the table of contents to the first page
        int tocPageNumber1 = pdfDoc.getNumberOfPages();
        
//        int tocCount = tocPageNumber - beforeIndex;
        int tocCount =  beforeIndex - (tocPageNumber+1);
        int actualTocCount = tocCount;
//        int[] middleCount = new int[tocCount];
//        for(int count = 0;count<tocCount;count++) {
//        	doc.add(new AreaBreak(AreaBreakType.NEXT_PAGE));
//        	middleCount[count] = pdfDoc.getNumberOfPages();
//        }
//        tocPageNumber = pdfDoc.getNumberOfPages();
//        tocCount = tocPageNumber - beforeIndex; // tocCount will always be even number so we can delete pages.
//        int moveCount = 2;
//        int remover = tocCount + moveCount - (tocCount/2); // from remover count you can delete the pages.
//        for(int count = beforeIndex+1;count<=tocPageNumber;count++) {
//        	 pdfDoc.movePage(count,moveCount);
//        	 moveCount++;
//        	 
//        	 if(count == tocPageNumber) {
//        		 for(int i = 0; i<=middleCount.length;i++) { //here we are deleting the first empty page also
//        			 pdfDoc.removePage(remover);
////        			 remover++;
//        		 }
//        	 }
//        }
//        pdfDoc.movePage(tocPageNumber,2);
//        pdfDoc.removePage(3);
//        pdfDoc.getPage(1).setPageLabel(PageLabelNumberingStyle.UPPERCASE_LETTERS,

     // footer code is here.
     	pdfDoc.addEventHandler(PdfDocumentEvent.END_PAGE, new TableFooterEventHandler(doc,actualTocCount));
     	pdfDoc.addEventHandler( PdfDocumentEvent.END_PAGE, new TextWatermark(leftImage));

		doc.close();
	  } catch(Exception ex) {
		  ex.printStackTrace();
	  }
	}
	public static File getTempFile(int i) {
		File tempDirectory = new File(System.getProperty("java.io.tmpdir"));
		File file = new File(tempDirectory.getAbsolutePath() + File.separator + "tmpFile" + i + ".pdf");
		return file;
	}
	protected class TextWatermark implements IEventHandler {
		Color lime, blue;
		PdfFont helvetica;
		Image img;

		protected TextWatermark(Image img) throws IOException {
			helvetica = PdfFontFactory.createFont(StandardFonts.HELVETICA);
			lime = new DeviceCmyk(0.208f, 0, 0.584f, 0);
			blue = new DeviceCmyk(0.445f, 0.0546f, 0, 0.0667f);
			this.img = img;
		}

		@Override
		public void handleEvent(Event event) {
			PdfDocumentEvent docEvent = (PdfDocumentEvent) event;
			PdfDocument pdf = docEvent.getDocument();
			PdfPage page = docEvent.getPage();
			int pageNumber = pdf.getPageNumber(page);
			Rectangle pageSize = page.getPageSize();
			PdfCanvas pdfCanvas = new PdfCanvas(page.newContentStreamBefore(), page.getResources(), pdf);
//			pdfCanvas.saveState().setFillColor(pageNumber % 2 == 1 ? lime : blue)
//					.rectangle(pageSize.getLeft(), pageSize.getBottom(), pageSize.getWidth(), pageSize.getHeight())
//					.fill().restoreState();
			
			img.setFixedPosition(pageSize.getWidth()/2 - 100, pageSize.getHeight()/2 - 120);
			if (pageNumber > 1) {
				pdfCanvas.beginText().setFontAndSize(helvetica, 10)
						.moveText(pageSize.getWidth() / 2 - 120, pageSize.getTop() - 20)
						.showText("The Strange Case of Dr. Jekyll and Mr. Hyde").moveText(120, -pageSize.getTop() + 40)
						.showText(String.valueOf(pageNumber)).endText();
			}
			Image image = img;
			new Canvas(pdfCanvas, pageSize).add(img.setOpacity(0.1f)).add(image.setOpacity(0.5f)).close();
			pdfCanvas.release();
		}
	}


	/*
	 * This is the Header of event handler to put Header on every page.
	 */

	public static class TableHeaderEventHandler implements IEventHandler {
		private Table table;
		private float tableHeight;
		private Document doc;

		public TableHeaderEventHandler(Document doc, Image leftImage, Image rightImage) {
			this.doc = doc;
			initTable(leftImage, rightImage);

			TableRenderer renderer = (TableRenderer) table.createRendererSubTree();
			renderer.setParent(new DocumentRenderer(doc));

			// Simulate the positioning of the renderer to find out how much space the
			// header table will occupy.
			LayoutResult result = renderer.layout(new LayoutContext(new LayoutArea(0, PageSize.A4)));
			tableHeight = result.getOccupiedArea().getBBox().getHeight();
		}

		@Override
		public void handleEvent(Event currentEvent) {
			PdfDocumentEvent docEvent = (PdfDocumentEvent) currentEvent;
			PdfDocument pdfDoc = docEvent.getDocument();
			PdfPage page = docEvent.getPage();
			PdfCanvas canvas = new PdfCanvas(page.newContentStreamBefore(), page.getResources(), pdfDoc);
			PageSize pageSize = pdfDoc.getDefaultPageSize();
//          float width = pageSize.getWidth() - doc.getRightMargin() - doc.getLeftMargin() + 20;
			float coordX = page.getPageSize().getX() + doc.getLeftMargin() - 13;// lower left x coordinate of page is
																				// zero always y also zero.
			float coordY = page.getPageSize().getTop() - doc.getTopMargin() - 10;
			float width = page.getPageSize().getWidth() - doc.getRightMargin() - 5;
			float height = getTableHeight();
			Rectangle rect = new Rectangle(coordX, 750, width, 80);
//          Rectangle rect = new Rectangle(22, 760, width, 80);
			new Canvas(canvas, rect).add(table).close();
		}

		public float getTableHeight() {
			return tableHeight;
		}

		private void initTable(Image leftImage, Image rightImage) {
//			table.setWidth(doc.getPdfDocument().getDefaultPageSize().getRight() - doc.getPdfDocument().getDefaultPageSize().getLeft() - doc.getLeftMargin() - doc.getRightMargin());

			float[] columnWidtha = { 500f, 500f };
			table = new Table(UnitValue.createPercentArray(columnWidtha));
			table.useAllAvailableWidth();
			table.setFixedLayout();
			table.addCell(new Cell().setBorder(Border.NO_BORDER));//.add(leftImage.setAutoScale(true)).setBorder(Border.NO_BORDER)
			table.addCell(new Cell().add(rightImage.setAutoScale(true).setHorizontalAlignment(HorizontalAlignment.RIGHT))
									.setBorder(Border.NO_BORDER));
		}
	}

	/*
	 * This is the footer of event handler to put footer on every page.
	 */
	 public static class TableFooterEventHandler implements IEventHandler {
		private Table table;
		private Document doc;
		private int tocCount;
		
		
		@SuppressWarnings("unused")
		public TableFooterEventHandler(Document doc,int tocCount) {
			this.doc = doc;
			this.tocCount = tocCount;
		}

		@SuppressWarnings("resource")
		@Override
		public void handleEvent(Event currentEvent) {
			PdfDocumentEvent docEvent = (PdfDocumentEvent) currentEvent;
			PdfDocument pdfDoc = docEvent.getDocument();
			int noOfPages = docEvent.getDocument().getNumberOfPages();
			int pageNo = pdfDoc.getPageNumber(docEvent.getPage());
			PdfPage page = docEvent.getPage();
			Boolean data = false;
			String version = "";
			String creationDate = "";
//			if(!StringHelper.isObjectNull(master)) {
//				if(master.getVersion()<10) {
//					version = "v0"+String.valueOf(master.getVersion());
//				} else {
//					version = "v"+String.valueOf(master.getVersion());
//				}
//				
//				creationDate = DateUtil.formatDate(new Date(master.getCreatedAt().getTime()), "dd-mm-YYYY");
//				data = true;
//			}
			// this code is for handling indexing if implemented is not working.
//			if(pdfDoc.getPageNumber(page) == 2) {
//				table = new Table(1);
//			} else if (pdfDoc.getPageNumber(page) == 1) {
//				table = new Table(new float[] { 300f, 300f, 300f }).useAllAvailableWidth();
//				if (data) {
//					table.addCell(new Cell().add(new Paragraph(creationDate + " " +version).setTextAlignment(TextAlignment.LEFT)).setBorder(Border.NO_BORDER));
//				} else {
//					table.addCell(new Cell().add(new Paragraph(creationDate + " " +version).setTextAlignment(TextAlignment.LEFT)).setBorder(Border.NO_BORDER));
//				}
//				table.addCell(new Cell().add(new Paragraph("Besichtigungsbericht").setTextAlignment(TextAlignment.CENTER)).setBorder(Border.NO_BORDER));
//				table.addCell(new Cell().add(new Paragraph("Seite " + pdfDoc.getPageNumber(page) + "/" + noOfPages)
//						.setTextAlignment(TextAlignment.RIGHT)).setBorder(Border.NO_BORDER));
//			} else {
				table = new Table(new float[] { 300f, 300f, 300f }).useAllAvailableWidth();
				table.addCell(new Cell().add(new Paragraph("06.04.2022 v01").setTextAlignment(TextAlignment.LEFT)).setBorder(Border.NO_BORDER));
				table.addCell(new Cell().add(new Paragraph("Besichtigungsbericht").setTextAlignment(TextAlignment.CENTER)).setBorder(Border.NO_BORDER));
//				table.addCell(new Cell().add(new Paragraph("Seite " + (pdfDoc.getPageNumber(page) - tocCount) + "/" + (noOfPages-tocCount))
//						.setTextAlignment(TextAlignment.RIGHT)).setBorder(Border.NO_BORDER));pdfDoc.getPageNumber(page) + "/" + noOfPages
				table.addCell(new Cell().add(new Paragraph("Seite " + (pdfDoc.getPageNumber(page) + "/" + noOfPages))
						.setTextAlignment(TextAlignment.RIGHT)).setBorder(Border.NO_BORDER));
//			}
			

			PdfCanvas canvas = new PdfCanvas(page.newContentStreamBefore(), page.getResources(), pdfDoc);
//          PdfCanvas canvass = new PdfCanvas(page);

			Rectangle rectangle = new Rectangle(35, 10, page.getPageSize().getWidth() - 67, 26);
			Canvas canvasObj = new Canvas(canvas, rectangle);
			canvasObj.add(table);
			canvasObj.close();
		}

	}
	 
	 
	static PdfOutline createOutline(PdfOutline outline, PdfDocument pdf, String title, String name) {
        if (outline == null) {
            outline = pdf.getOutlines(false);
            outline = outline.addOutline(title);
            outline.addDestination(PdfDestination.makeDestination(new PdfString(name)));
        } else {
            PdfOutline kid = outline.addOutline(title);
            kid.addDestination(PdfDestination.makeDestination(new PdfString(name)));
        }

        return outline;
    }
	 public static class UpdatePageRenderer extends ParagraphRenderer {
	        protected SimpleEntry<String, Integer> entry;

	        public UpdatePageRenderer(Paragraph modelElement, SimpleEntry<String, Integer> entry) {
	            super(modelElement);
	            this.entry = entry;
	        }

	        @Override
	        public LayoutResult layout(LayoutContext layoutContext) {
	            LayoutResult result = super.layout(layoutContext);
	            entry.setValue(layoutContext.getArea().getPageNumber());
	            if (result.getStatus() != LayoutResult.FULL) {
	                if (null != result.getOverflowRenderer()) {
	                    result.getOverflowRenderer().setProperty(
	                            Property.LEADING,
	                            result.getOverflowRenderer().getModelElement().getDefaultProperty(Property.LEADING));
	                } else {
	                    // if overflow renderer is null, that could mean that the whole renderer will overflow
	                    setProperty(
	                            Property.LEADING,
	                            result.getOverflowRenderer().getModelElement().getDefaultProperty(Property.LEADING));
	                }
	            }
	            return result;
	        }
	    }
	 
		public static Image getImage(String path) {
			//URI uri = null; 
			//Image data for create image from path and send it to the Image object.
			ImageData imageFactory = null;
			try {
			//	uri = new URI(path);
				imageFactory = ImageDataFactory.create(path);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				logger.error("Unable to create image: "+e.getMessage());
				
			}
			return new Image(imageFactory);
		}
}
