package com.dem.demoHub.pdfPractice;


import java.io.IOException;
import java.util.AbstractMap;
import java.util.ArrayList;

import com.itextpdf.io.font.constants.StandardFonts;
import com.itextpdf.kernel.colors.ColorConstants;
import com.itextpdf.kernel.font.PdfFont;
import com.itextpdf.kernel.font.PdfFontFactory;
import com.itextpdf.kernel.geom.PageSize;
import com.itextpdf.kernel.pdf.PageLabelNumberingStyle;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfOutline;
import com.itextpdf.kernel.pdf.PdfString;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.kernel.pdf.action.PdfAction;
import com.itextpdf.kernel.pdf.canvas.draw.DottedLine;
import com.itextpdf.kernel.pdf.navigation.PdfDestination;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.element.AreaBreak;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.element.Tab;
import com.itextpdf.layout.element.TabStop;
import com.itextpdf.layout.layout.LayoutContext;
import com.itextpdf.layout.layout.LayoutResult;
import com.itextpdf.layout.properties.HorizontalAlignment;
import com.itextpdf.layout.properties.Property;
import com.itextpdf.layout.properties.TabAlignment;
import com.itextpdf.layout.properties.TextAlignment;
import com.itextpdf.layout.properties.VerticalAlignment;
import com.itextpdf.layout.renderer.IRenderer;
import com.itextpdf.layout.renderer.ParagraphRenderer;

public class NewIndexCreation {

	public static final String DEST = "C:\\Users\\mange\\OneDrive\\Desktop\\New folder\\pdf\\_sign1.pdf";
	public static void main(String[] args) throws IOException {
	        PdfWriter writer = new PdfWriter(DEST);
	        PdfDocument pdf = new PdfDocument(writer);
	        Document document = new Document(pdf, PageSize.A4,false);
	        

	        // Create a PdfFont
	        PdfFont font = PdfFontFactory.createFont(StandardFonts.TIMES_ROMAN,"Cp1254");

	        document
	                .setTextAlignment(TextAlignment.JUSTIFIED)
	                .setFont(font)
	                .setFontSize(11);
	        PdfOutline outline = null;
	        java.util.List<AbstractMap.SimpleEntry<String, AbstractMap.SimpleEntry<String, Integer>>> toc = new ArrayList<>();
	        for(int i=0;i<100;i++){
	            String line = "This is paragraph " + String.valueOf(i+1)+ " ";
	            line = line.concat(line).concat(line).concat(line).concat(line).concat(line);
	            Paragraph p = new Paragraph(line);
	            p.setKeepTogether(true);
	            document.add(p.setFont(font).setFontSize(10).setHorizontalAlignment(HorizontalAlignment.CENTER).setTextAlignment(TextAlignment.LEFT));

	            //PROCESS FOR TOC
	            String name = "para " + String.valueOf(i+1);
	            outline = createOutline(outline,pdf,line ,name );
	            AbstractMap.SimpleEntry<String, Integer> titlePage = new AbstractMap.SimpleEntry(line, pdf.getNumberOfPages());
	            p
	                    .setFont(font)
	                    .setFontSize(12)
	                    //.setKeepWithNext(true)
	                    .setDestination(name)

	                    // Add the current page number to the table of contents list
	                    .setNextRenderer(new UpdatePageRenderer(p));
	            toc.add(new AbstractMap.SimpleEntry(name, titlePage));

	        }

	        int contentPageNumber = pdf.getNumberOfPages();

	        for (int i = 1; i <= contentPageNumber; i++) {

	            // Write aligned text to the specified by parameters point
	            document.showTextAligned(new Paragraph(String.format("Sayfa %s / %s", i, contentPageNumber)).setFontSize(10),
	                    559, 26, i, TextAlignment.RIGHT, VerticalAlignment.MIDDLE, 0);
	        }

	        //BEGINNING OF TOC
	        document.add(new AreaBreak());
	        Paragraph p = new Paragraph("Table of Contents")
	                .setFont(font)
	                .setDestination("toc");
	        document.add(p);
	        java.util.List<TabStop> tabStops = new ArrayList<>();
	        tabStops.add(new TabStop(580, TabAlignment.RIGHT, new DottedLine()));
	        for (AbstractMap.SimpleEntry<String, AbstractMap.SimpleEntry<String, Integer>> entry : toc) {
	            AbstractMap.SimpleEntry<String, Integer> text = entry.getValue();
	            p = new Paragraph()
	                    .addTabStops(tabStops)
	                    .add(text.getKey())
	                    .add(new Tab())
	                    .add(String.valueOf(text.getValue()))
	                    .setAction(PdfAction.createGoTo(entry.getKey()));
	            document.add(p);
	        }



	        // Move the table of contents to the first page
	        int tocPageNumber = pdf.getNumberOfPages();
	        for (int i = 1; i <= tocPageNumber; i++) {

	            // Write aligned text to the specified by parameters point
	            document.showTextAligned(new Paragraph("\n footer text\n second line\nthird line").setFontColor(ColorConstants.RED).setFontSize(8),
	                    300, 26, i, TextAlignment.CENTER, VerticalAlignment.MIDDLE, 0);
	        }

	        document.flush();
	        for(int z = 0; z< (tocPageNumber - contentPageNumber ); z++){
	            pdf.movePage(tocPageNumber,1);
	            pdf.getPage(1).setPageLabel(PageLabelNumberingStyle.UPPERCASE_LETTERS,
	                    null, 1);
	        }


	        //pdf.movePage(tocPageNumber, 1);

	        // Add page labels
	        /*pdf.getPage(1).setPageLabel(PageLabelNumberingStyle.UPPERCASE_LETTERS,
	                null, 1);*/
	        pdf.getPage(tocPageNumber - contentPageNumber + 1).setPageLabel(PageLabelNumberingStyle.DECIMAL_ARABIC_NUMERALS,
	                null, 1);


	        document.close();

	    }
	    private static PdfOutline createOutline(PdfOutline outline, PdfDocument pdf, String title, String name) {
	        if (outline == null) {
	            outline = pdf.getOutlines(false);
	            outline = outline.addOutline(title);
	            outline.addDestination(PdfDestination.makeDestination(new PdfString(name)));
	        } else {
	            PdfOutline kid = outline.addOutline(title);
	            kid.addDestination(PdfDestination.makeDestination(new PdfString(name)));
	        }

	        return outline;
	    }
	    private static class UpdatePageRenderer extends ParagraphRenderer {
	        protected AbstractMap.SimpleEntry<String, Integer> entry;

	        public UpdatePageRenderer(Paragraph modelElement, AbstractMap.SimpleEntry<String, Integer> entry) {
	            super(modelElement);
	            this.entry = entry;
	        }
	        public UpdatePageRenderer(Paragraph modelElement) {
	            super(modelElement);
	        }

	        @Override
	        public LayoutResult layout(LayoutContext layoutContext) {
	            LayoutResult result = super.layout(layoutContext);
	            //entry.setValue(layoutContext.getArea().getPageNumber());
	            if (result.getStatus() != LayoutResult.FULL) {
	                if (null != result.getOverflowRenderer()) {
	                    result.getOverflowRenderer().setProperty(
	                            Property.LEADING,
	                            result.getOverflowRenderer().getModelElement().getDefaultProperty(Property.LEADING));
	                } else {
	                    // if overflow renderer is null, that could mean that the whole renderer will overflow
	                    setProperty(
	                            Property.LEADING,
	                            result.getOverflowRenderer().getModelElement().getDefaultProperty(Property.LEADING));
	                }
	            }
	            return result;
	        }

	        @Override
	        // If not overriden, the default renderer will be used for the overflown part of the corresponding paragraph
	        public IRenderer getNextRenderer() {
	            return new UpdatePageRenderer((Paragraph) this.getModelElement());
	        }

	      
	    }
	}

