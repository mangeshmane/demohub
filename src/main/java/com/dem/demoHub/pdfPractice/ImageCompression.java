package com.dem.demoHub.pdfPractice;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FilterOutputStream;
import java.net.URI;
import java.net.URL;
import java.time.format.DateTimeFormatter;

import javax.imageio.IIOImage;
import javax.imageio.ImageIO;
import javax.imageio.ImageWriteParam;
import javax.imageio.ImageWriter;
import javax.imageio.stream.ImageOutputStream;

import com.itextpdf.io.image.ImageData;
import com.itextpdf.io.image.ImageDataFactory;
import com.itextpdf.kernel.colors.WebColors;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.element.Cell;
import com.itextpdf.layout.element.Image;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.element.Table;
import com.itextpdf.layout.properties.BorderCollapsePropertyValue;
import com.itextpdf.layout.properties.BorderRadius;
import com.itextpdf.layout.properties.HorizontalAlignment;
import com.itextpdf.layout.properties.TextAlignment;
import com.itextpdf.layout.properties.UnitValue;
import com.itextpdf.layout.properties.VerticalAlignment;

import java.awt.Color;

public class ImageCompression {
	public static void main(String[] args) {
        try {
            // Read input image
            File inputFile = new File("input.jpg");
//          String path = "https://rcsdevstorage.blob.core.windows.net/rcsdevcontainer1/parent/1012968/docs/objmod/1/1680686590180_photo_4.jpg";
//            String path = "https://rcsdevstorage.blob.core.windows.net/rcsdevcontainer1/surve_artus_logo_header_right.png";
            String path1 = "C:\\Users\\mange\\Downloads\\vecteezy_high-quality-3d-scientist-man-with-microscope-in-scientific_22483658_828.png";
            String path = "https://rcsdevstorage.blob.core.windows.net/rcsdevcontainer1/parent/105417/docs/objmod/1/1680878956591_photo_5.png";
//          String path1 = "C:\\Users\\mange\\Downloads\\pngwing.com.png";
           
            URL url = new URL(path); 
            BufferedImage image = ImageIO.read(url);
            // Create output stream for compressed image
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            BufferedImage originalImage = ImageIO.read(new File(path1));
            if(path.endsWith("png")) {
            	BufferedImage newBufferedImage = new BufferedImage(
                        originalImage.getWidth(),
                        originalImage.getHeight(),
                        BufferedImage.TYPE_INT_BGR);

                        newBufferedImage.createGraphics()
                        .drawImage(newBufferedImage, 0, 0, null, null);
                        Graphics2D g2d = newBufferedImage.createGraphics();
                        g2d.setColor(null); //Color.WHITE
                        g2d.fillRect(0, 0, newBufferedImage.getWidth(), newBufferedImage.getHeight());
                        g2d.drawImage(originalImage, 0, 0, null);
                        g2d.dispose();
                        newBufferedImage.flush();
                        ImageIO.write(newBufferedImage, "jpg", outputStream);
                
                        File imageFile1 = getTempFile(123211, "png");
                        // writing the byte array to the file using filewriter.
                        FileOutputStream fileWriter1 = new FileOutputStream(imageFile1);
                        fileWriter1.write(outputStream.toByteArray());
                        fileWriter1.close();
            }
            
            
//                    outputStream = new ByteArrayOutputStream();
            // Get ImageWriter for JPG format
            ImageWriter writer = ImageIO.getImageWritersByFormatName("jpg").next();
            
            // Set compression quality
            ImageWriteParam writeParam = writer.getDefaultWriteParam();
            writeParam.setCompressionMode(ImageWriteParam.MODE_EXPLICIT);
            writeParam.setCompressionQuality(0.35f); // Set compression quality (0.0f - 1.0f)
            
            // Compress and write image to output stream
            ImageOutputStream ios = ImageIO.createImageOutputStream(outputStream);
            writer.setOutput(ios);
            writer.write(null, new IIOImage(originalImage, null, null), writeParam);
            ios.close();
            writer.dispose();
            
            // Get compressed image data from output stream
            byte[] compressedImageData = outputStream.toByteArray();
            Image compressedImage = new Image(ImageDataFactory.create(compressedImageData));
            
            File imageFile = getTempFile(1232, "png");
            // writing the byte array to the file using filewriter.
            FileOutputStream fileWriter = new FileOutputStream(imageFile);
            fileWriter.write(compressedImageData);
            fileWriter.close();
            
            System.out.println(imageFile.getAbsolutePath());
           
            File fileOuput = ItextHelper.getTempFile(20203);
            System.out.println(fileOuput.getAbsolutePath());
            // Create PDF document
            PdfDocument pdfDocument = new PdfDocument(new PdfWriter(fileOuput));
            Document document = new Document(pdfDocument);
            
            // Create Image element with compressed image data
            
            Table table = getTableForImageRendering();
            Image originalImage1 = getImage(path);
            System.out.println(compressedImage.getImageHeight() + " "+ originalImage1.getImageHeight());
            setPhotosInTable(originalImage1, table);
            setPhotosInTable(compressedImage, table);
            // Add compressed image to PDF document
//            document.add(compressedImage);
            document.getPdfDocument().getDefaultPageSize();
            document.add(table);
            
            // Close the document
            document.close();
            
            System.out.println("Image compression and PDF integration complete.");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
	
	public static Table getTableForImageRendering() {
		Table imageTable = new Table(UnitValue.createPercentArray(new float[] { 500f }));
		imageTable.setFixedLayout().setWidth(UnitValue.createPercentValue(100)); //
		System.out.println(UnitValue.createPercentValue(50).getValue());
		imageTable.setBorderCollapse(BorderCollapsePropertyValue.SEPARATE);
		imageTable.setVerticalBorderSpacing(10);
		imageTable.setHorizontalBorderSpacing(10);
		return imageTable;
	}
	public static void setPhotosInTable(Image serverImage, Table imageTable) {
		
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		System.out.println(imageTable);
		imageTable.addCell(new Cell().add(serverImage.scaleToFit(400f, 250f).setHorizontalAlignment(HorizontalAlignment.CENTER)
				.setPadding(10f))
				
				.add(new Paragraph("name"+ ", Date : ").setTextAlignment(TextAlignment.CENTER)
						.setFontSize(10f).setBackgroundColor(WebColors.getRGBColor("#afcddc")))
				.setBorderRadius(new BorderRadius(5f)));
	}
	public static Image getImage(String path) {
	URI uri = null;
	// Image data for create image from path and send it to the Image object.
	ImageData imageFactory = null;
	try {
		uri = new URI(path);
		imageFactory = ImageDataFactory.create(path);
	} catch (Exception e) {
		e.printStackTrace();

	}
	return new Image(imageFactory);
}
	
	public static File getTempFile(int i,String extension) {
		File tempDirectory = new File(System.getProperty("java.io.tmpdir"));
		File file = new File(tempDirectory.getAbsolutePath() + File.separator + "tmpFile" + i + "."+extension);
		return file;
	}
}
