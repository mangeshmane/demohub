package com.dem.demoHub.pdfPractice;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class FileReadWriteExample {
	
	@SuppressWarnings("unused")
	public static void main(String[] args) {
		
		File file = new File("C:\\Users\\mange\\OneDrive\\Desktop\\Re__\\Code_test.txt");
		
		try {
			FileReader reader = new FileReader("C:\\Users\\mange\\Downloads\\1_Program.txt");
			BufferedReader streamReader = new BufferedReader(reader);
			File newFile = new File("C:\\Users\\mange\\OneDrive\\Desktop\\Re__\\Code_test1.txt");
			FileWriter writer = new FileWriter(newFile,true);
			BufferedWriter buffWriter = new BufferedWriter(writer);
			File newFile1 = new File("C:\\Users\\mange\\OneDrive\\Desktop\\Re__\\Code_test2.txt");
			FileWriter writer1 = new FileWriter(newFile1,true);
			BufferedWriter buffWriter1 = new BufferedWriter(writer1);
			String line = null;
			List<String> lines = new ArrayList<String>();
			List<String> commentedLines = new ArrayList<String>();
			while ((line = streamReader.readLine()) != null) {
					lines.add(line);
			}
			List<String> arr = new ArrayList<String>();
			for(int i=0; i<lines.size(); i++) {
				if((lines.get(i).startsWith("//") || lines.get(i).contains("//")) 
						|| ((lines.get(i).startsWith("/*") || lines.get(i).contains("........") 
								|| lines.get(i).contains("*/") || lines.get(i).contains("/*"))
						|| lines.get(i).startsWith("*") || lines.get(i).startsWith("*/"))) {
					
					commentedLines.add(lines.get(i));
//					arr.add(i);
				} else {
					arr.add(lines.get(i));
				}
			}
			
			for(String liness: arr) {
				buffWriter1.write(liness);
				buffWriter1.newLine();
			}
			buffWriter1.close();
			if (null != commentedLines) {
				for(String lineb : commentedLines) {
					buffWriter.write(lineb+"\r\n");
					System.getProperty("line.separator");
					buffWriter.newLine();
				}
				
			} else {
				System.out.println("There are no commented code in the file");
			}
			buffWriter.close();
			System.out.println(lines);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
}
