package com.dem.demoHub.pdfPractice;

import java.io.File;
import java.net.URI;

import com.itextpdf.kernel.colors.ColorConstants;
import com.itextpdf.kernel.events.Event;
import com.itextpdf.kernel.events.IEventHandler;
import com.itextpdf.kernel.events.PdfDocumentEvent;
import com.itextpdf.kernel.geom.PageSize;
import com.itextpdf.kernel.geom.Rectangle;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfPage;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.kernel.pdf.canvas.PdfCanvas;
import com.itextpdf.layout.Canvas;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.borders.Border;
import com.itextpdf.layout.element.AreaBreak;
import com.itextpdf.layout.element.Cell;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.element.Table;
import com.itextpdf.layout.properties.AreaBreakType;
import com.itextpdf.layout.properties.TextAlignment;
import com.itextpdf.layout.properties.VerticalAlignment;

public class FooterExamplePDF {
	private static final File file = new File("C:\\Users\\mange\\OneDrive\\Desktop\\New folder\\pdf\\"
			+ "_sign1.pdf");
	 public static void main(String[] args) throws Exception {

	        new FooterExamplePDF().manipulatePdf(file);
	    }

	    protected void manipulatePdf(File dest) throws Exception {
	        PdfDocument pdfDoc = new PdfDocument(new PdfWriter(file));
	        pdfDoc.addNewPage();
	        Document doc = new Document(pdfDoc, PageSize.A4,false);
	        //doc.setMargins(36, 36, 72, 36);

	        Table table = new Table(1).useAllAvailableWidth();

//	        Cell cell = new Cell().add(new Paragraph("This is a test doc"));
//	        cell.setBackgroundColor(ColorConstants.ORANGE);
//	        table.addCell(cell);
//
//	        cell = new Cell().add(new Paragraph("This is a copyright notice"));
//	        cell.setBackgroundColor(ColorConstants.LIGHT_GRAY);
//	        table.addCell(cell);

	        String rightImage = "https://sadwerkdev.blob.core.windows.net/document/dWerk_5f107e0d-56bb-414a-9268-323fc6dcc789_right.jpeg";
	        String leftImage = "https://sadwerkdev.blob.core.windows.net/document/dWerk_5f107e0d-56bb-414a-9268-323fc6dcc789_right.jpeg";
	        URI left = new URI(leftImage);
	        URI right = new URI(rightImage);
	        for (int i = 0; i < 150; i++) {
	            doc.add(new Paragraph("Hello World!"));
	        }
	        System.out.println(doc.getBottomMargin()+doc.getLeftMargin()+doc.getTopMargin()+doc.getRightMargin());
	        doc.add(new AreaBreak(AreaBreakType.NEXT_PAGE));
	        doc.add(new Paragraph("Hello World!"));
	        doc.add(new AreaBreak(AreaBreakType.NEXT_PAGE));
	        doc.add(new Paragraph("Hello World!"));
	        pdfDoc.addEventHandler(PdfDocumentEvent.START_PAGE, new TableHeaderEventHandler(table) );
	        pdfDoc.addEventHandler(PdfDocumentEvent.END_PAGE, new TableFooterEventHandler(table,doc));
	        int n = pdfDoc.getNumberOfPages();
	        Paragraph footer;
	        for (int page = 1; page <= n; page++) {
//	        	table.addCell(new Cell().add(new Paragraph("06.04.2022 v01").setTextAlignment(TextAlignment.CENTER)));
// 	            table.addCell(new Cell().add(new Paragraph("Besichtigungsbericht").setTextAlignment(TextAlignment.CENTER)));
// 	            table.addCell(new Cell().add(new Paragraph("Seite "+page+"/"+n)));
//	            footer = new Paragraph(String.format("Page %s of %s", page, n));
//	            doc.showTextAligned(footer, 297.5f, 20, page, TextAlignment.CENTER, VerticalAlignment.MIDDLE, 0);
	        }
	        doc.close();
	    }


	    private static class TableFooterEventHandler implements IEventHandler {
	        private Table table;
	        private Document doc;

	        @SuppressWarnings("unused")
			public TableFooterEventHandler(Table table,Document doc) {
	            this.table = table;
	            this.doc = doc;
	        }

	        @SuppressWarnings("resource")
			@Override
	        public void handleEvent(Event currentEvent) {
	            PdfDocumentEvent docEvent = (PdfDocumentEvent) currentEvent;
	            PdfDocument pdfDoc = docEvent.getDocument();
	            int noOfPages = docEvent.getDocument().getNumberOfPages();
	            int pageNo = pdfDoc.getPageNumber(docEvent.getPage());
	            PdfPage page = docEvent.getPage();
	            table = new Table(new float[] {300f,300f,300f}).useAllAvailableWidth();
	            table.addCell(new Cell().add(new Paragraph("06.04.2022 v01").setTextAlignment(TextAlignment.LEFT)));
 	            table.addCell(new Cell().add(new Paragraph("Besichtigungsbericht").setTextAlignment(TextAlignment.CENTER)));
 	            table.addCell(new Cell().add(new Paragraph("Seite "+pdfDoc.getPageNumber(page)+"/"+noOfPages).setTextAlignment(TextAlignment.RIGHT)));
	            PdfCanvas canvas = new PdfCanvas(page.newContentStreamBefore(), page.getResources(), pdfDoc);
//	            PdfCanvas canvass = new PdfCanvas(page);
	            Rectangle rectangle = new Rectangle(36, 10, page.getPageSize().getWidth() -67, 26);
	            Canvas canvasObj =  new Canvas(canvas, rectangle);
	            canvasObj.add(table);
	            canvasObj.close();
	        }
	        
	    }
	    
	    protected class TableHeaderEventHandler implements IEventHandler {
	    	private Table table;

	        public TableHeaderEventHandler(Table table) {
	            this.table = table;
	        	
	        }

	        @Override
	        public void handleEvent(Event event) {
	            PdfDocumentEvent docEvent = (PdfDocumentEvent) event;
	            PdfDocument pdfDoc = docEvent.getDocument();

	            PdfPage page = docEvent.getPage();
	            Rectangle pageSize = new Rectangle(26, 10, page.getPageSize().getWidth() -62, 26);

	            Canvas canvas = new Canvas(new PdfCanvas(page.newContentStreamBefore(), page.getResources(), pdfDoc), pageSize);
	            canvas.setFontSize(18);

	            // Write text at position
	            canvas.showTextAligned("this is header",
	                    pageSize.getWidth() / 2,
	                    pageSize.getTop() - 30, TextAlignment.CENTER);
	            canvas.close();
	        }
	    }

}
