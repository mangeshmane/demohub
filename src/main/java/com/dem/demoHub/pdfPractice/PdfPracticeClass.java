package com.dem.demoHub.pdfPractice;

import java.io.File;
import java.io.FileNotFoundException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;

import com.itextpdf.io.image.ImageData;
import com.itextpdf.io.image.ImageDataFactory;
import com.itextpdf.kernel.colors.ColorConstants;
import com.itextpdf.kernel.colors.WebColors;
import com.itextpdf.kernel.events.Event;
import com.itextpdf.kernel.events.IEventDispatcher;
import com.itextpdf.kernel.events.IEventHandler;
import com.itextpdf.kernel.events.PdfDocumentEvent;
import com.itextpdf.kernel.geom.PageSize;
import com.itextpdf.kernel.geom.Rectangle;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfPage;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.kernel.pdf.canvas.PdfCanvas;
import com.itextpdf.layout.Canvas;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.borders.Border;
import com.itextpdf.layout.borders.SolidBorder;
import com.itextpdf.layout.element.AreaBreak;
import com.itextpdf.layout.element.Cell;
import com.itextpdf.layout.element.Image;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.element.Table;
import com.itextpdf.layout.layout.LayoutArea;
import com.itextpdf.layout.layout.LayoutContext;
import com.itextpdf.layout.layout.LayoutResult;
import com.itextpdf.layout.properties.AreaBreakType;
import com.itextpdf.layout.properties.HorizontalAlignment;
import com.itextpdf.layout.properties.TextAlignment;
import com.itextpdf.layout.properties.UnitValue;
import com.itextpdf.layout.properties.VerticalAlignment;
import com.itextpdf.layout.renderer.DocumentRenderer;
import com.itextpdf.layout.renderer.TableRenderer;

public class PdfPracticeClass {
	private static final File file = new File("C:\\Users\\mange\\OneDrive\\Desktop\\New folder\\pdf\\" + "_sign1.pdf");

	public static void main(String[] args) throws MalformedURLException, URISyntaxException {

		String a = "\"abc\"";
		System.out.println(a);
		System.out.println(a.replaceAll("\"", ""));
		PdfWriter writer = null;
		try {
			writer = new PdfWriter(file);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		PdfDocument pdfDoc = new PdfDocument(writer);
		pdfDoc.addNewPage();

		Document doc = new Document(pdfDoc, PageSize.A4, false);
//		doc.setMargins(70, 0, 0, 00);
//		String rightUrl = "https://sadwerkdev.blob.core.windows.net/document/dWerk_5f107e0d-56bb-414a-9268-323fc6dcc789_right.jpeg";
//      String leftUrl = "https://sadwerkdev.blob.core.windows.net/document/dWerk_5f107e0d-56bb-414a-9268-323fc6dcc789_right.jpeg";
		String rightUrl = "C:\\Users\\mange\\Downloads\\right.jpeg";
		String leftUrl = "C:\\Users\\mange\\Downloads\\left.jpeg";
//        URI left = new URI(rightUrl);
//        URI right = new URI(leftUrl);
		ImageData imageFactoryLeft = ImageDataFactory.create(leftUrl);
		ImageData imageFactoryRight = ImageDataFactory.create(rightUrl);
		Image leftImage = new Image(imageFactoryLeft);
		Image rightImage = new Image(imageFactoryRight);
		// TODO: if any thing is not working then add table 1 data here or simply
		// initialize it.
		Headers header = new Headers(doc, leftImage, rightImage);
		pdfDoc.addEventHandler(PdfDocumentEvent.END_PAGE, header);
		doc.setMargins(112, 36, 36, 36);
		float[] columnWidth = { 100f, 400f, 200f };
		Table table3 = new Table(UnitValue.createPercentArray(columnWidth));
		table3.setWidth(UnitValue.createPercentValue(100));
		table3.setFixedLayout().setMarginTop(20);

//		table3.setMargins(50, 0, 0, 0);

		table3.addCell(new Cell(0, 0).add(new Paragraph("jfald")).setTextAlignment(TextAlignment.LEFT)
				.setHorizontalAlignment(HorizontalAlignment.CENTER).setVerticalAlignment(VerticalAlignment.MIDDLE)
				.setBorder(Border.NO_BORDER).setBorderRight(new SolidBorder(1))
				.setBackgroundColor(WebColors.getRGBColor("#f2f2f2")));
		Table t2 = new Table(2).setPadding(0).setFixedLayout().useAllAvailableWidth().setMargin(0);
		t2.addCell(new Cell().add(new Paragraph("finefinefine")).setBorder(Border.NO_BORDER));
		t2.addCell(new Cell().add(new Paragraph("finefinefine")).setBorder(Border.NO_BORDER));
		t2.addCell(new Cell().add(new Paragraph("finefinefine")).setBorder(Border.NO_BORDER));
		t2.addCell(new Cell().add(new Paragraph("finefinefine")).setBorder(Border.NO_BORDER));
		table3.addCell(new Cell(0, 2).add(t2).setPadding(0).setBorder(Border.NO_BORDER)).setBorder(Border.NO_BORDER)
				.setBorderRight(new SolidBorder(1));
//		table3.addCell(new Cell().add(new Paragraph("Gesamtlagerfläche:\nLagergut:\r\n"
//				+ "Anzahl der Brandabschnitte:\r\n"
//				+ "Größte Lagerfläche:\r\n"
//				+ "Art der Lagerung:"))).setBorder(Border.NO_BORDER);
//		table3.addCell(new Cell().add(new Paragraph("")));
//		table3.addCell(new Cell().add(new Paragraph("")).setBorder(Border.NO_BORDER));
		doc.add(table3);
		doc.add(new AreaBreak());
		int n = pdfDoc.getNumberOfPages();
		Paragraph footer;
		for (int page = 1; page <= n; page++) {
			footer = new Paragraph(String.format("Page %s of %s", page, n));
			doc.showTextAligned(footer, 297.5f, 20, page, TextAlignment.CENTER, VerticalAlignment.MIDDLE, 0);
		}

		// this code is used to draw the canvas square or any shape on the page at given
		// position.
		PdfPage page = pdfDoc.getPage(1);
		Rectangle pageRect = new Rectangle(36, 400, 300, 380);
		new PdfCanvas(page).setStrokeColor(ColorConstants.RED).setLineWidth(3)
				.roundRectangle(33, page.getMediaBox().getTop() - 300, 530, 200, 5)
//	        .rectangle(pageRect)
				.stroke();
		//create table for photos.
		Table photoTable = new Table(new float[] {100,100,100});
		photoTable.useAllAvailableWidth().setFixedLayout();
		
		photoTable.addCell(new Cell().add(rightImage.setAutoScale(true)).add(new Paragraph("Right Image").setTextAlignment(TextAlignment.CENTER)));
		photoTable.addCell(new Cell().add(rightImage.setAutoScale(true)).add(new Paragraph("Right Image").setTextAlignment(TextAlignment.CENTER)));
		photoTable.addCell(new Cell().add(rightImage.setAutoScale(true)).add(new Paragraph("Right Image").setTextAlignment(TextAlignment.CENTER)));
		doc.add(photoTable);
		doc.close();
	
}
}
class Headers implements IEventHandler {
	String header;
	Document doc;
	Table table1;
	float tableHeight;

	public Headers(Document doc, Image leftImage, Image rightImage) {
		this.doc = doc;

		float[] columnWidtha = { 500f, 500f };
		table1 = new Table(UnitValue.createPercentArray(columnWidtha));
		table1.useAllAvailableWidth();
		table1.setFixedLayout();
		table1.addCell(new Cell().add(leftImage.setAutoScale(true))

		);
		table1.addCell(new Cell().add(rightImage.setAutoScale(true).setHorizontalAlignment(HorizontalAlignment.RIGHT))

		);

		TableRenderer renderer = (TableRenderer) table1.createRendererSubTree();
		renderer.setParent(new DocumentRenderer(doc));

		// Simulate the positioning of the renderer to find out how much space the
		// header table will occupy.
		LayoutResult result = renderer.layout(new LayoutContext(new LayoutArea(0, PageSize.A4)));
		this.tableHeight = result.getOccupiedArea().getBBox().getHeight();
	}

	public float getTableHeight() {
		return tableHeight;
	}

	@SuppressWarnings("resource")
	@Override
	public void handleEvent(Event event) {
		PdfDocumentEvent docEvent = (PdfDocumentEvent) event;
		PdfDocument pdf = docEvent.getDocument();
		PdfPage page = docEvent.getPage();
		float coordX = page.getPageSize().getX() + doc.getLeftMargin() - 10;
		float coordY = page.getPageSize().getTop() - doc.getTopMargin() - 10;
		float width = page.getPageSize().getWidth() - doc.getRightMargin() - 5;
		float height = 80;

		Rectangle rectangle = new Rectangle(coordX, coordY, width, height);
		PdfCanvas pdfCanvas = new PdfCanvas(page.newContentStreamBefore(), page.getResources(), pdf);
		Canvas canvas = new Canvas(pdfCanvas, rectangle);
		canvas.add(table1);
		canvas.close();
	}
	
}
