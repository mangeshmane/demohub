package com.demo.demoHub.serviceImpl;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

import com.demo.demoHub.model.MarketPairDetailsData;
import com.demo.demoHub.response.GetAllMarketResponse;
import com.demo.demoHub.response.GetAllPairsWithMarketDetailsResponse;
import com.demo.demoHub.service.ICoinDcxService;

@Service("ICoinDcxService")
public class CoinDcxServiceImpl implements ICoinDcxService {

	@Value("${coin.api.key}")
	private String apiKey;

	@Value("${coin.secrete.key}")
	private String secrete;

	@Value("${dcx.url}")
	private String url;

	@Autowired
	WebClient webClient;

	@Override
	public GetAllMarketResponse getAllPairsInMarkets() {
		GetAllMarketResponse response = new GetAllMarketResponse();
		List<String> clientResponse = new ArrayList<>();
		try {
			webClient = WebClient.builder().baseUrl(url + "/exchange/v1/markets")
					.defaultHeader("Authorization", "Bearer " + apiKey + ":" + secrete).build();

			clientResponse = webClient.get().retrieve().bodyToMono(ArrayList.class).block();
			if (null != clientResponse || !clientResponse.isEmpty()) {
				response.setListOfMarketCurrency(clientResponse);
			}
			System.out.println(response);
			return response;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}

	}

	@Override
	public GetAllPairsWithMarketDetailsResponse getAllPairsWithMarketsDetails() {
		GetAllPairsWithMarketDetailsResponse response = new GetAllPairsWithMarketDetailsResponse();
		List<MarketPairDetailsData> clientResponse = new ArrayList<>();
		try {
			webClient = WebClient.builder().baseUrl(url + "/exchange/v1/markets_details")
					.codecs(configurer -> configurer.defaultCodecs().maxInMemorySize(10 * 1024 * 1024)) // Set the
																										// buffer size
																										// to 10 MB
					.defaultHeader("Authorization", "Bearer " + apiKey + ":" + secrete).build();

			clientResponse = webClient.get().retrieve()
					.bodyToMono(new ParameterizedTypeReference<List<MarketPairDetailsData>>() {
					}).block();
			List<String> currencies = clientResponse.stream().map(data -> data.getBaseCurrencyName())
					.collect(Collectors.toList());
			if (null != clientResponse || !clientResponse.isEmpty()) {
				response.setList(clientResponse);
				System.out.println(currencies);
			}
			System.out.println(response);
			return response;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}

	}
}

