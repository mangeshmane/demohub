package com.demo.demoHub.serviceImpl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import com.demo.demoHub.dao.EmpRepo;
import com.demo.demoHub.dao.IDepartmentRepository;
import com.demo.demoHub.model.Department;
import com.demo.demoHub.model.Employee;
import com.demo.demoHub.model.Result;
import com.demo.demoHub.service.EmployeeService;

@Service("EmployeeService")
public class EmpServiceImpl implements EmployeeService {

	@Autowired
	private EmpRepo empRepo;
	
	@Autowired
	private IDepartmentRepository departmentRepository;
	

	@Override
	public List<Employee> getEmp() {

		List<Employee> list = empRepo.findAll();
		return list;
	}

	@Override
	public Result saveEmp(Employee emp) {
		Result result = new Result();
		try {
			
			updateEmployee(emp);
			try {
				saveEmployeeResult(emp);
			} catch (Exception e) {
				throw new RuntimeException("This will work and won't rollback others...");
			} 
			Optional<Employee> er = empRepo.findByName(emp.getName());
			if(er.isPresent()) {
//				empRepo.delete(emp);
				throw new RuntimeException("Emp name already present.");
			}
			Employee emp1 = empRepo.save(emp);
			if(emp1 == null) {
				result.setSuccess(Boolean.FALSE);
				result.setError("NOT SAVED");
				result.setSuccessMessage("USER IS NOT SAVED");
			} else {
				result.setSuccess(Boolean.TRUE);
				result.setSuccessMessage("USER SAVED SUCCESSFULLY");
			}
		} catch (Exception e) {
			Employee employee = new Employee();
			BeanUtils.copyProperties(emp, employee);
			employee.setId(null);
			createEmp(emp);
			throw new RuntimeException("Failed to save the employee.");
		}
		return result;
				
	}
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	private Result saveEmployeeResult(Employee emp) {
		Result result = new Result();
		try {
			
//			updateEmployee(emp);
			Optional<Employee> er = empRepo.findByName(emp.getName());
			if(er.isPresent()) {
				empRepo.delete(er.get());
				throw new RuntimeException("Emp name already present.");
			}
			Employee emp1 = empRepo.save(emp);
			if(emp1 == null) {
				result.setSuccess(Boolean.FALSE);
				result.setError("NOT SAVED");
				result.setSuccessMessage("USER IS NOT SAVED");
			} else {
				result.setSuccess(Boolean.TRUE);
				result.setSuccessMessage("USER SAVED SUCCESSFULLY");
			}
		} catch (Exception e) {
//			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
			throw new RuntimeException("Failed to save the employee.");
		}
		return result;
	}
	
	@Transactional(propagation = Propagation.REQUIRES_NEW, noRollbackFor = RuntimeException.class)
	private Employee createEmp(Employee emp) {
		return empRepo.save(emp);
	}
	
	@Transactional(propagation = Propagation.REQUIRES_NEW, noRollbackFor = RuntimeException.class)
	private Employee updateEmployee(Employee emp) {
		Employee er = empRepo.findById(emp.getId()).get(); 
		BeanUtils.copyProperties(emp, er);
		return empRepo.save(er);
	}

	@Override
	public Employee findById(int id) {
		return empRepo.findById(id).get();
		}
	
	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW, noRollbackFor = RuntimeException.class)
	public Result updateEmp(Employee emp) {
		Employee er = empRepo.findById(emp.getId()).get(); 
		Result result = new Result();
		if((er != null)) {
//			er.setName(emp.getName()); 
			BeanUtils.copyProperties(emp, er);
			empRepo.save(er);
			result.setSuccess(Boolean.TRUE);
			result.setError("no error occured");
			result.setSuccessMessage("USER UPDATED SUCCEESSFULLY");
			return result;
		}else {
			result.setSuccess(Boolean.FALSE);
			result.setError("error occured");
			result.setSuccessMessage("USER NOT UPDATED");
			return result;
		}
	}
	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW, noRollbackFor = RuntimeException.class)
	public Result deleteById(int id) {
		Result result = new Result();
//		empRepo.deleteById(id);
		departmentRepository.deleteById(id);
		result.setError("nothing");
		result.setSuccess(Boolean.TRUE);
		result.setSuccessMessage("deleted successfully");
		return result;
	}

	}
