package com.demo.demoHub.service;

import com.demo.demoHub.response.GetAllMarketResponse;
import com.demo.demoHub.response.GetAllPairsWithMarketDetailsResponse;

public interface ICoinDcxService {

	public GetAllMarketResponse getAllPairsInMarkets();

	public GetAllPairsWithMarketDetailsResponse getAllPairsWithMarketsDetails();
	
}
