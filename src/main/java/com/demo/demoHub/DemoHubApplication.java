package com.demo.demoHub;

import java.io.File;
import java.io.FileNotFoundException;
import java.net.MalformedURLException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.reactive.function.client.WebClient;

import com.azure.storage.blob.BlobClientBuilder;
import com.demo.demoHub.Utility.SasTokenForUrls;
import com.itextpdf.io.image.ImageData;
import com.itextpdf.io.image.ImageDataFactory;
import com.itextpdf.kernel.colors.ColorConstants;
import com.itextpdf.kernel.colors.WebColors;
import com.itextpdf.kernel.events.Event;
import com.itextpdf.kernel.events.IEventHandler;
import com.itextpdf.kernel.geom.PageSize;
import com.itextpdf.kernel.numbering.RomanNumbering;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.borders.Border;
import com.itextpdf.layout.borders.SolidBorder;
import com.itextpdf.layout.element.AreaBreak;
import com.itextpdf.layout.element.Cell;
import com.itextpdf.layout.element.Image;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.element.Table;
import com.itextpdf.layout.properties.AreaBreakType;
import com.itextpdf.layout.properties.HorizontalAlignment;
import com.itextpdf.layout.properties.TextAlignment;
import com.itextpdf.layout.properties.UnitValue;
import com.itextpdf.layout.properties.VerticalAlignment;
import com.itextpdf.svg.utils.TextRectangle;

@SpringBootApplication
//@ComponentScan(basePackages = "com.demo.demoHub.model.*")
//@EnableJpaRepositories("com.demo.demoHub.dao")
//@EntityScan("com.demo.demoHub.model")
@EnableScheduling
public class DemoHubApplication {

	@Bean
	public WebClient getWebClient() {
		return WebClient.builder().build();
	}

	@Bean
	public BlobClientBuilder getClient() {
		BlobClientBuilder client = new BlobClientBuilder();
//		client.containerName(n)
		return client;
	}

	public static void main(String[] args) throws MalformedURLException {
		ConfigurableApplicationContext context = SpringApplication.run(DemoHubApplication.class, args);
		SasTokenForUrls bean = (SasTokenForUrls) context.getBean("sasTokenForUrls");
//		calledToRun();
	}
	public static void calledToRun() throws MalformedURLException {
//		bean.generateSasUrl();
		File file = new File("C:\\Users\\mange\\OneDrive\\Desktop\\New folder\\pdf\\" + "_sign.pdf");
		PdfWriter writer = null;
		try {
			writer = new PdfWriter(file);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		PdfDocument pdfDoc = new PdfDocument(writer);
		pdfDoc.addNewPage(PageSize.A4);
		Document doc = new Document(pdfDoc);
//		doc.setMargins(20, 25, 25, 20);
		Paragraph para = new Paragraph("2.3 Versicherungswerte (in T €)").setFontSize(8);
		para.setBorder(new SolidBorder(0.5f)).setBackgroundColor(WebColors.getRGBColor("#ffc000")).setPaddingLeft(2)
				.setBorderRight(Border.NO_BORDER);
		TextRectangle textRectangle = new TextRectangle(0, 0, 0, 0, 0);
		doc.add(para);
//		float[] colWidth = { 40, 300, 50, 300, 200, 50 };
//		Table table = new Table(UnitValue.createPercentArray(6));

		// create table with nine column with
		Table table = new Table(9);
		table.setWidth(UnitValue.createPercentValue(100));
		table.setFixedLayout();
		table.setAutoLayout();
		table.addCell("2.3 Versicherungswerte (in T €)").setBold().setTextAlignment(TextAlignment.LEFT);
//	    doc.add(table);
		// table for section and chapter example
		Table sectionTable = new Table(2);
		sectionTable.setWidth(UnitValue.createPercentValue(100));
		sectionTable.setFixedLayout();
		sectionTable.setAutoLayout();
		sectionTable.startNewRow().addCell("2.3 Versicherungswerte (in T €)").setFontColor(ColorConstants.BLACK)
				.setBold().setTextAlignment(TextAlignment.LEFT).setBackgroundColor(WebColors.getRGBColor("#ffc000"))
				.setHeight(10f).setBorderRight(Border.NO_BORDER);

		// Main table start from here section
		Table t1 = new Table(2);
		t1.setWidth(UnitValue.createPercentValue(100));
		t1.setFixedLayout();
		t1.setAutoLayout();
		Cell c5 = new Cell();
		c5.add(new Paragraph("2.3 Versicherungswerte (in T €)").setFontSize(7).setPaddings(0, 0, 0, 0));
		c5.setBackgroundColor(WebColors.getRGBColor("#ffc000"));
		c5.setBorderRight(Border.NO_BORDER);
		c5.setTextAlignment(TextAlignment.LEFT);
		t1.addCell(c5);
		doc.add(t1);

		// main table with rows and column with different property.
		Table table2 = new Table(7);
		table2.setWidth(UnitValue.createPercentValue(100));
		table2.setFixedLayout();
		table2.setAutoLayout();
		table2.setMarginTop(10f);
		table2.setMargins(10, 15, 0, 15);

		int rowCount = 10;
		for (int row = 0; row <= rowCount; row++) {
			for (int col = 0; col < table2.getNumberOfColumns(); col++) {
				Cell cell = getCell();

				// first row and column from 0 to end
				if ((col >= 0 && row == 0)) {
					cell.setBackgroundColor(WebColors.getRGBColor("#ffc000")).setBorderBottom(Border.NO_BORDER)
							.setTextAlignment(TextAlignment.LEFT).setVerticalAlignment(VerticalAlignment.MIDDLE)
							.setBorderBottom(new SolidBorder(0.5f));

					// for first column in first row only
					if (row == 0 && col <= 0) {
						cell.setTextAlignment(TextAlignment.LEFT).setVerticalAlignment(VerticalAlignment.BOTTOM)
								.setMarginBottom(-1f);
					}

					// for row greater than 0 and first column
				} else if (row > 0 && col == 0) {
					cell.setBackgroundColor(WebColors.getRGBColor("#ffc000")).setBorderBottom(Border.NO_BORDER)
							.setTextAlignment(TextAlignment.LEFT).setVerticalAlignment(VerticalAlignment.BOTTOM)
							.setMarginBottom(-1f);

				} else {
					// for remaining cells value and adding text
					cell.setTextAlignment(TextAlignment.LEFT).setVerticalAlignment(VerticalAlignment.MIDDLE);
				}

				// For removing border and adding roman numbers in table.
				if ((row > 0 && row < rowCount) && col == 0) {
					String num = RomanNumbering.toRoman(row, true);
					cell.add(new Paragraph(num));
					if (row > 1 && row < rowCount) {
						cell.setBorderTop(Border.NO_BORDER);
					}

				}

				// first row border removing
				if (col >= 0 && col <= table2.getNumberOfColumns() - 2 && row == 0) {
					cell.setBorderRight(Border.NO_BORDER);

				}

				// for last column solid black border
				if (col == table2.getNumberOfColumns() - 2 && row >= 0) {
					cell.setBorderRight(new SolidBorder(0.5f));
				}
				// for top border of all last cell in table
				if (row == rowCount && col >= 0) {
					cell.setBorderTop(new SolidBorder(1.2f));
				}
				table2.addCell(cell);
			}

		}
		doc.add(table2);
		// starting new page from here
		doc.add(new AreaBreak(AreaBreakType.NEXT_PAGE));
		Paragraph p = new Paragraph("This is the next page");
//		doc.add(p);
		float[] columnWidth = { 100f, 400f, 200f };
		Table table3 = new Table(UnitValue.createPercentArray(columnWidth));
		table3.setWidth(UnitValue.createPercentValue(100));
		table3.setFixedLayout();
//		table3.setAutoLayout();
//		table3.setMargins(10, 15, 0, 15);

		table3.addCell(new Cell(0, 0).add(new Paragraph("jfald")).setTextAlignment(TextAlignment.LEFT)
				.setHorizontalAlignment(HorizontalAlignment.CENTER).setVerticalAlignment(VerticalAlignment.MIDDLE)
				.setBorder(Border.NO_BORDER).setBorderRight(new SolidBorder(1))
				.setBackgroundColor(WebColors.getRGBColor("#f2f2f2")));
		table3.addCell(new Cell()
				.add(new Paragraph("sfgsfgswrfggggggggggggggggggggggggggggggggggggrtythssdfgsgsdgdgsdgdgdfgsdgsd"))
				.setBorder(Border.NO_BORDER).setBorderRight(new SolidBorder(1)));
		table3.addCell(new Cell().add(new Paragraph("Gesamtlagerfläche:\nLagergut:\r\n"
				+ "Anzahl der Brandabschnitte:\r\n" + "Größte Lagerfläche:\r\n" + "Art der Lagerung:")))
				.setBorder(Border.NO_BORDER);
//		table3.addCell(new Cell().add(new Paragraph("")));
//		table3.addCell(new Cell().add(new Paragraph("")).setBorder(Border.NO_BORDER));

		String imageFile = "C:\\Users\\mange\\OneDrive\\Desktop\\New folder\\thumbnail_companyLogo.jpg";
		ImageData data = ImageDataFactory.create(imageFile);

		// Creating the image
		Image img = new Image(data);
		Image img1 = new Image(data);
		// Adding image to the cell
//	      table3.addCell(new Cell().add(img.setAutoScale(true).setHorizontalAlignment(HorizontalAlignment.RIGHT)).setWidth(10).setHeight(10));

		for (int head = 1; head <= 1; head++) {
			Table ta = new Table(2);
			ta.setWidth(UnitValue.createPercentValue(100));
			ta.setFixedLayout();
			ta.setAutoLayout();
//	    	ta.setMarginTop(-15);
			ta.addCell(new Cell().add(img.setAutoScale(true).setPaddingLeft(0)).setWidth(50).setHeight(50));
			ta.addCell(new Cell().add(img1.setAutoScale(true).setHorizontalAlignment(HorizontalAlignment.RIGHT))
					.setWidth(50).setHeight(50));
//	    	pdfDoc.addEventHandler(PdfDocumentEvent.START_PAGE, (IEventHandler) ta);
//	    	PdfPage page =pdfDoc.getPage(head);
			doc.showTextAligned(p, 100, head, TextAlignment.LEFT);

			doc.add(ta);
		}
//		for(int row = 0; row<5; row++) {
//			for(int col = 0; col < 3; col++) {
//				if(row == 0 && col == 0) {
//					table3.addCell(getCell(3,0).add(new Paragraph(" akdjf")
////							.setPaddingTop(10f)
//							.setTextAlignment(TextAlignment.CENTER)
//							.setVerticalAlignment(VerticalAlignment.MIDDLE))
//							.setHorizontalAlignment(HorizontalAlignment.CENTER)
////							.setBorder(Border.NO_BORDER)
//							.setBorderRight(new SolidBorder(1)));
//					
//				}
//				table3.addCell(getCell().add(new Paragraph("akdjf")).setBorder(Border.NO_BORDER));
//				table3.addCell(getCell().add(new Paragraph("akdjf")));
//			}

//		}
		doc.add(table3);
		doc.close();
	}
//
	public static Cell getCell() {
		Cell cell = new Cell();
		// border color with type of border eg:solidborder
		cell.setBorder(new SolidBorder(ColorConstants.GRAY, 0.5f)).setHeight(18f)
				.setBackgroundColor(WebColors.getRGBColor("#f1f4ff")).setWidth(60f).setFontSize(8f);
		return cell;
	}

	public static Cell getCell(int row, int col) {
		Cell cell = new Cell(row, col);
		// border color with type of border eg:solidborder
		cell.setBorder(new SolidBorder(ColorConstants.GRAY, 0.5f)).setHeight(18f)
				.setBackgroundColor(WebColors.getRGBColor("#f1f4ff")).setWidth(60f).setFontSize(8f);
		return cell;
	}

	public static Table getTable(Integer no) {
		Table table = new Table(2);
		return table;
	}
//	table.addCell(new Cell().add(new Paragraph("upi").setBackgroundColor(WebColors.getRGBColor("#ffc000"))).setBorderRight(Border.NO_BORDER));
}

class HeaderAndFooterHander implements IEventHandler {

	@Override
	public void handleEvent(Event event) {
		// TODO Auto-generated method stub

	}

}