package com.demo.demoHub.exceptionhandler;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice // file level exception handler where we handle exception with handler methods.
						// spring search till this method get found.
public class GlobalExceptionHandler {

	@ExceptionHandler(Exception.class)
	public String handleGlobalExceptions(Exception e) {
		return "lakjdf";
	}

	public static void main(String[] args) {

	}
	/*
	 * checked or compile time means compiler checks that expression can throw the
	 * exception at run time so it tell user to handle it using throws or try-catch.
	 */

}

class ExceptionPractice {
	public static void main(String[] args) {
		// Exception without try catch
		// This exception in not handled so main method will create the exception and
		// jvm check
		// method is handled exception or not if not then send it to default exception
		// handler to print the object.
		// program will terminate abnormally means it will not run after exception is
		// not handled.
		int a = 100 / 10;
		System.out.println(a);

		System.out.println(100);
		try { // try block can have the code that can throw the exception so we can catch
			printValue();
			readFileAndPrintObject();
			System.out.println(100);
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(10); // here exception is handled and it runs the program further.
		}
		System.out.println("code after exception handling...");

	}

	static void printValue() {
		/*
		 * When we call this method and exception occur then this method create the
		 * exception object but if exception is not handled by the method then jvm check
		 * if called method is handled or not if caller method also not handled then
		 * goes to default exception handler and print the object.
		 */
		int a = 100;
		a = a / 10; // method create exception object and then jvm check handled or not if not then
					// goes to called method and there check excpetion handled or not
					// if there not then program terminate abnormally and don't run further. default
					// exception handler print the object.
		System.out.println(a);
	}

	static void readFileAndPrintObject() throws IOException {

		File file = new File("C:/Users/mange/OneDrive/Desktop/HtmlPractice/file_practice.txt");
		if (!file.exists()) {
			file.createNewFile();
		}
		FileOutputStream output = new FileOutputStream(file);
		output.write(20);
		FileInputStream stream = new FileInputStream(file);
		int i;
		while ((i = stream.read()) != -1) {
			System.out.println((char) i);
		}
		System.out.println(file);
	}
}
