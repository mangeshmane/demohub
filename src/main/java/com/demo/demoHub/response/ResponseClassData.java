package com.demo.demoHub.response;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ResponseClassData {

	// First object
	int id;
	String name;
	boolean haschild;
	String linkto;
	boolean selected;
	boolean expand;
	@JsonProperty("parent")
	List<Parent> parent;
	
}
class ResponseObject {
	@JsonProperty("data")
	List<ResponseClassData> data;
}

class Child {
	int id;
	String name;
	String linkto;
	String pid;
}
class Parent {
	int id;
	String name;
	boolean haschild;
	String linkto;
	String pid;
	boolean expand;
	List<Child> object;
}

