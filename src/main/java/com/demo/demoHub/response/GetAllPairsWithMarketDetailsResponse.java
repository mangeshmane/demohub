package com.demo.demoHub.response;

import java.util.List;

import com.demo.demoHub.model.MarketPairDetailsData;
import com.fasterxml.jackson.annotation.JsonProperty;

public class GetAllPairsWithMarketDetailsResponse {

	@JsonProperty("market_pair_details")
	List<MarketPairDetailsData> list;

	public List<MarketPairDetailsData> getList() {
		return list;
	}

	public void setList(List<MarketPairDetailsData> list) {
		this.list = list;
	}

	@Override
	public String toString() {
		return "GetAllPairsWithMarketDetailsResponse [list=" + list + "]";
	}

}
