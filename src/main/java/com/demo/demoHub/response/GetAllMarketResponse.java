package com.demo.demoHub.response;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class GetAllMarketResponse {
	
	@JsonProperty("active_currency_list")
	List<String> listOfMarketCurrency;

	public List<String> getListOfMarketCurrency() {
		return listOfMarketCurrency;
	}

	public void setListOfMarketCurrency(List<String> listOfMarketCurrency) {
		this.listOfMarketCurrency = listOfMarketCurrency;
	}

	@Override
	public String toString() {
		return "GetAllMarketResponse [listOfMarketCurrency=" + listOfMarketCurrency + "]";
	}
}
