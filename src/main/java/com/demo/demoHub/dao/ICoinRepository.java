package com.demo.demoHub.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.demo.demoHub.model.Coin;

@Repository
public interface ICoinRepository extends JpaRepository<Coin, Long> {

}
