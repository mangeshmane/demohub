package com.demo.demoHub.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.demo.demoHub.model.RankDiffs;

@Repository
public interface IRankDifferenceRepository extends JpaRepository<RankDiffs, Long>{

}
