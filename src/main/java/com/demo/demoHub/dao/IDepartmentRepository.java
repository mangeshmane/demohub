package com.demo.demoHub.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.demo.demoHub.model.Department;

@Repository
public interface IDepartmentRepository extends JpaRepository<Department, Integer> {

}
