package com.demo.demoHub.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.demo.demoHub.model.Performance;

@Repository
public interface IperformanceRepository extends JpaRepository<Performance, Long> {

}
