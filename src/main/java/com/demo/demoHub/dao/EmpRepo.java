package com.demo.demoHub.dao;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import com.demo.demoHub.model.Employee;

@Repository
public interface EmpRepo extends JpaRepository<Employee, Integer> {
	Optional<Employee> findByName(@Param("name") String name);
}
