package com.demo.demoHub.Utility;

class WrapperPractice {
	
	
	Integer a ;
	Integer b;
	//there ways that known that is using forloop with i--,while ,and swapping the last to first with with
	//value start++ and end--;
	public void reverseArray() {
		Integer[] arr = new Integer[] {1,2,3,4,5,6,7,8,9};
		for(int i = arr.length-1; i>=0; i--) {
			System.out.println(arr[i]);
		}
		int i = arr.length-1;
		while( i >= 0) {
			System.out.print(arr[i] + " ");
			i--;
		}
		
		int temp;
		int first = 0;
		int end = arr.length-1;
		while(first<end) {
			temp = arr[first];
			arr[first] = arr[end];
			arr[end] = temp;
			System.out.println("");
			System.out.print(arr[first]);
			first++;
			end--;
		}
	}
	
}
public class HashCodeAndeEquals {

	public static void main(String[] args) {
		String a = "abc";
		String b = "abc";
		String c = new String(b);
		String d = new String(c);
		System.out.println(a==b);
		System.out.println(a.equals(b));
		System.out.println(b == c);
		System.out.println(c==d);
		System.out.println(b.equals(c));
		
		System.out.println(a.hashCode() + " " + b.hashCode()  + " " + a.hashCode() + " " +c.hashCode());
		PracticeClass pc = new PracticeClass();
		System.out.println(pc.hashCode());
		PracticeClass pd = new PracticeClass();
		System.out.println(pd.hashCode());
	
		int i = 20;
		int j = i;
		
		j = 30;
		System.out.println(i + " " + j);
		
		WrapperPractice wp = new WrapperPractice();
		wp.reverseArray();
		wp.a = 29;
		wp.b = 30;
		
		System.out.println(wp.a);
		WrapperPractice wp1 = wp;
		wp1.a = 23;
		System.out.println(wp1.a);
		System.out.println(wp.a); //non primitves are reference type so if you change another object value
								  // first object value also get changed because reference var are stored in 
								 // stack and object is same for both variables present in heap so both changes.
		
		
	}

}
