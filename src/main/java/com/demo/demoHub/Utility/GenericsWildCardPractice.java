package com.demo.demoHub.Utility;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class GenericsWildCardPractice {

	public static void main(String[] args) {
		genericSimpleMethod(10); // can send any type of data string ,int , double, or any.
		genericSimpleMethod("abc");
		genericSimpleMethod(new Integer[1]);
		
		List<Integer> intList = Arrays.asList(10,930,34,32,64,12);
		genericsWithTypeParamInList(intList);
		
		List<Vehical> vehicalList = new ArrayList<Vehical>(); // Both angular bracket value must match in generics parent child won't work here. 
		List<?> testList = new ArrayList<>();
//	testList.add(23);// In this case we cannot add the element in it because compiler doesn't know the type breaks the type-safety
		
		List<String> tList = new ArrayList<>();
		GenricsWildCard<Integer> genWild = new GenricsWildCard<>(89);
		System.out.println(genWild.getType(343));
		System.out.println("\'abc\'");
		int templateUri = 20;
		System.out.println("SELECT COUNT(*) FROM TemplateStore ts INNER JOIN ProductTemplateRepository ptr ON ts.id = ptr.templateId WHERE ts.uri = \'"
				+ templateUri + "\'");
		
	}
	
	// Compiler and Jvm can come to the picture when we use generics actually gives compile time type safety.
	/*
	 * So this is the method which accept any type in the parameter. instead of totally depends on the class where we 
	 * write class<T> we can define it before the method return type. we can return the T type also for the method. 
	 * At first java doesn't know what T means in method param so have to tell the compiler T is generic type
	 * and how we tell by adding syntax like below is <T>(generics Type).
	 */
	
	/*
	 * This is simple method where method accept the any type of parameter and then we can print the type
	 * of object and we can access the method and property of that object or type.
	 */
	public static <T> void genericSimpleMethod(T type) { // For this type of param we can't use the ? because it only works with <> in here.
		System.out.println("class type: "+type.getClass()+" with value : " +type);
		
//		type = 199;  // We can't change the type value because it breaks the type safety we don't know the what type of data will come.just print or get class details.
//		type = 10.20;
	}
	public static void genericSimpleMethods(Object type) {
		System.out.println("class type: "+type.getClass()+" with value : " +type);
		type = "stirng "; // Here from above method why we don't try writing object here it is not type safe to use. because we can change the type value.
	}
	
	
	/*
	 * This is simple method where method accepting the class with give T type. so we can send only a class with class. 
	 */
	public static <T> T genericMethod(Class<T> type) throws InstantiationException, IllegalAccessException {
		T response = type.newInstance();
		return type.cast(type); 
	}
	/*
	 * This is a simple method which is accepting the list of type T where we can now print the list
	 * can access the method property and methods inside of it and return the list of T. In it we can now add the 
	 * T is the type of data that we going to send with parameter and declared in <T> data type of T.
	 */
	public static <T extends Number> void genericsWithTypeParamInList(List<T> type) {
//		type.add(32); // Can't do this here breaks the type safety. because we are sending the same type and add other type to it.
	}
	
	/*
	 * This method is accepting the list of unknown type means any type of parameter can send here
	 * why we are Using ? mark because when we send the object of list polymorphism for angular <> only for base type
	 * This is list read-only means we can't add any because it breaks the type safety.
	 * It looks like List<?> type = new ArrayList<anytype>(); read-only.
	 */
	public static void genericWithWildCardInList(List<?> type) {
		// read-only for any type if add it will automatically raise the compile time error.
	}
	
	// With bounded generic
	
	/*
	 *  Accepting the only generic list with only vehical types and below of its. read only.
	 *  Because if we are sending the list of bike from method call and then again adding the car in the list
	 *  it breaks the type safety it gives the type safety. unknown type extends the animal.
	 */
	
	public static void genericWithWildCardInListWithBound(List<? extends Vehical> type) {
		
	}
	/*
	 * It gives the more flexibility to add the values in the array list. So it will add the vehical and it's super type e.g:Object.
	 */
	public static void genericWithWildCardInListWithBoundForWrite(List<? super Vehical> type) {
		type.add(new Bike());
//		type.add(new Object()); // why it is not working because it looks like this child = new object(); we can't do this because object is not the numbers.
	}
	
	public static void genericWithWildCardInListWithBoundForWriteInt(List<? super Integer> type) {
		type.add(39);
		type.add(12);
		Number aa = Integer.valueOf(10);
		Number a = Long.valueOf((long)103.3f);
		type.add((Integer)a);
		System.out.println(type.get(1));
		// compiler checks the what type of reference is on left and at runtime jvm checks right side object.
		List list = new ArrayList<String>(); 
		/*
		 *  Here list is a normal list not a generic and pointing(referencing to) generic arraylist.
		 *  and Here comes the twist compiler only checks at reference type at compile type that what is their type
		 *  but at the comile time there is no concept available like generics Arraylist of String so at runtime jvm act like there is not type
		 *  so it will add the any value to it.
		 *  1.At compile time reference value will be checked and at runtime jvm checks thhe type of object that refenrce is refering to means
		 *  what object is on right side.2. compile time check reference and at runtime checks actual object at right side.
		 *  SO IT MEANS THERE IS NO GENERICS CONCEPT AT RUN TIME.
		 */
		list.add(390);
		list.add("adc");
	}
}

// Accept any type to store it in class. Also T can accept any type with bounds means extends and super type will accept.
class GenricsWildCard<T> {
	private T type;
	
	public GenricsWildCard() {
		// TODO Auto-generated constructor stub
	}
	
	public GenricsWildCard(T type) {
		
		this.type = type;
	}
	public T getType(T type) {
		return this.type;
	}
	
	public static <T extends Vehical> T printT(T type) {
		// So the type <T> in the method is different than the Class<T> don't get confused.
		return (T) new Bike();
	}
		
}

abstract class BaseGeneric {
	
	public abstract <T extends Object> T getTypeOfGeneric(String action, Class<T> type);
}

class GenericPracticeMethodImpl extends BaseGeneric {
	/*
	 * This method accepting the Type-parameter <T> and this is getting from base.class so T will be base type.
	 */
	public <T extends Object> T getTypeOfGeneric(String action, Class<T> type) {
		String response = null;
		if (action.equals("THIS_IS_GENERIC_METHOD")) {
			response = "Yes this is generic Method";
		}
		return (T) response;
	}
}
