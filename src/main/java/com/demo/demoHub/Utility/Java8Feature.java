package com.demo.demoHub.Utility;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.IntFunction;
import java.util.function.Predicate;
import java.util.function.Supplier;

import jnr.ffi.Struct.off_t;

public class Java8Feature {
	public static void main(String[] args) {
		
		System.out.println("annotation".equals("annotation "));
		
		String test1 = "This is sample annotation for power supply\\\\n\\\\n\\\\n\\\\n\\\\n\\\\n\\\\n\\\\n\\\\n\\\\n\\\\n\\\\n\\\\n\\\\n\\\\n\\\\n\\\\n\\\\n\\\\n\\\\n\\\\n\\\\n\\\\n\\\\n\\\\n\\\\n\\\\n\\\\n\\\\n\\\\n\\\\nThanks";
		String v = test1.replaceAll("\\\\n", "\n");
		v.replaceAll("\n", System.lineSeparator());
		System.out.println(v);
		System.out.println(test1);
		
		String av = "addcsdfk";
		av = av.substring(0, av.length() -1);
		System.out.println(av);
		//Method reference practice
		//we use method reference where we use the lambdas instead of giving the implementation of the method 
		//using lambda use the already implemented methods. by giving the reference to it.
		
//		1.method referencing using the object.
		List<Integer> list = Arrays.asList(33,35,3,33,6);
		list.forEach(a -> System.out.println(a*3483)); //here we used the lambda implementation of accept methods.
		list.forEach(System.out::println); // a is the parameter for accept method of consumer interface.
		PrintNumbers num = a -> {
			int b = a*a;         // here we given the lambda for the number sqare implementation.
			System.out.println(b);
		};
		num.numberSquare(23); //so this above implemention using lambda used for the squaring of the given number.
		
//		instead we can use the method reference for the given implementation.
//		given method is non-static means instance/object method call it after object creation.
		AbsoluteNumber numq = new AbsoluteNumber();
		PrintNumbers num1 = numq::printNumber;// here we give printnumber reference instead of lambda 
		num1.numberSquare(22);
		
//		2.constructor method reference( whenever we return the object of the any class then use this.)
		PrintBasics basic = () -> new AbsoluteNumber(); // here we are returning the object using lambda.
		
		PrintBasics basic1 = AbsoluteNumber:: new; // like that using constructor method reference.
		AbsoluteNumber numberSquare = basic1.numberSquare();
		
		
		//3. Consumer Interface practice.
		Adc adc = new Adc();
		adc.acceptConsumer((d)->System.out.println(d*d));
		
		//4. supplier interface call the supplierPractice class below.
		String string = SupplierPractice.supplier.get();
		List<String> nameList = SupplierPractice.getName().get();
		
		//5. Function interface practice.
		Integer apply = FunctionPractice.getFunction().apply("adb");
		
		//6.predicate
		boolean test = PredicatePractice.getPredicateBoolean().test(20);
	}
}

@FunctionalInterface
interface PrintNumbers {
	public void numberSquare(int a);
}

@FunctionalInterface
interface PrintBasics {
	public AbsoluteNumber numberSquare();
}

class AbsoluteNumber {
	public void printNumber(int a) {
		System.out.println(a*a);
	}
}

class Adc {
	public void acceptConsumer(Consumer<Integer> t ) {
		//just print the methods value with doing the square of it.
		List<Integer> a = Arrays.asList(10,12,92,24);
		//print square of every element inside of the list a
		a.forEach(n -> t.accept(n));
		for(Integer e : a) {
			t.accept(e);
		}
	}
}

class SupplierPractice {
	// Supplier<T> is the generic functional interface with get() method who doesn't accept any argument and return type T.
	// example showing the public variable implemntation of the supplier variable.
 	 public static Supplier<String> supplier = new Supplier<String>() {
		
		@Override
		public String get() {
			//only retrun the string because created the supplier with the String as generics.
			return "supplier the functional interface.";
		}
	};
	
	public static Supplier<List<String>> getName() {
		//so this method is returning the supplier which is having the get method implementation of getname();
		Supplier<List<String>> list = () -> {
			List<String> nameList = Arrays.asList("ram","sham","rahul","ramesh");
			return nameList;
			
		};
//		return list;
		
		//also we can directly write the inline implementation.
		return () -> new ArrayList<String>();
	}
}

class FunctionPractice {
	// basically Function<T, R> interface accept the T type of data and return the Result with R type of Data.
	public static Function<String, Integer> getFunction() {
		Function<String, Integer> fun = (T) -> 23; //accept the string and return the Integer type.
		return fun;
	}
	
}

class PredicatePractice {
	public static Predicate<Integer> getPredicateBoolean() {
		return (n) -> {
			if (n % 2 == 0) { 
				return true;
			} else {
				return false;
			}
		};
	}
}
 

	
	



























