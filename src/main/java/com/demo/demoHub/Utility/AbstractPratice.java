package com.demo.demoHub.Utility;

import java.util.Objects;

import org.apache.tomcat.util.buf.CharsetCache;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

public class AbstractPratice {
	static int var1 = 1000;
	String var2;
	public static void main(String[] args) {
		Car car = new Car();
		car.setName("alskdjf");
		car.setNumber(9843);
		setCarName(car);
		System.out.println(car.getName() + "   "+ car.getNumber());
		//boxing-unboxing
		long v1 = 10; // both operations are done implicitly boxing- int to Integer. unboxing Integer to int.
		Long v2 = (Long) v1; //implicit -- auto-boxing
		v1 = (long)v2; // explicit un_boxing
		//widening-narrowing
		int v3 = (int) v1; //narrowing done explicitly--- primitive data types convert from high to low. data reduces
		long v4 = v3;  // widening it works because we are storing the small value in big data-type.
		
		// conversion from one wrapper data type to another and also the one wrapper class to another primitive type.
		Long v7 = 20L;
		int v8 = (int) v7.longValue();
		Integer v9 = v7.intValue();
		
		Long v5 = 203L;
		Object o = 93L;
		long  v6 = 9;
		
		Long v10 = new Long(v3);
		Object v11 = v10;
		checkPoints((Long)v11);
		
		// Parent p = new Child();
		Long value = (long) var1; // here we are explicitly casting from small to wide and then explicit boxing is done on long to Long. no need to assign explicit casting.
		          // for above int to (long) as we are assigning the int to we can't assign implicitly so we first cost int to long and then assign we can 
		Long value1 = (long)(Integer.valueOf(var1)); // done this is alternate way.
		
		checkPoints(10);
		String[] arr = {"[{\"CDNR\":[{\"Status\":\"FAIL\",\"val\":268978.64,\"sgtin\":\"27AABCK0820C1Z7\",\"reason\":[\"InvoiceType can not be Null\"],\"bgstin\":\"27AABCV1740N1Z4\",\"idt\":\"08-07-2023\",\"inum\":\"2023240045\"}]}]",
				"[{\"B2B\":[{\"Status\":\"Success\",\"val\":75048.2,\"sgtin\":\"27ALSPA2925M1ZR\",\"bgstin\":\"27AABCV1740N1Z4\",\"idt\":\"08-04-2023\",\"inum\":\"000015\"}]}]",
				"[{\"CDNUR\":[{\"Status\":\"FAIL\",\"val\":765,\"sgtin\":\"\",\"reason\":[\"InvoiceType is invalid\"],\"bgstin\":\"27AABCV1740N1Z4\",\"idt\":\"20-09-2023\",\"inum\":\"66CN\"}]}]",
				"[{\"B2B\":[{\"Status\":\"Success\",\"val\":34432,\"sgtin\":\"27AEFPV6599C1ZX\",\"bgstin\":\"27AABCV1740N1Z4\",\"idt\":\"09-06-2023\",\"inum\":\"23-24\\/18\"}]}]",
				"[{\"reason\": \"Something Went Wrong..\"}]"};
		checkJson(arr);
	}
	public static void checkPoints(long value) {
		System.out.println(value);
	}
	public static void setCarName(Car car) {
		car.setName("new one");
	}
	public static void checkJson(String[] list) {
		for (int i = 0; i < list.length; i++) {
			if (null != list[i]) {
				ObjectMapper objectMapper = new ObjectMapper();
				try {
					JsonNode node = objectMapper.readTree((String) list[i]);
					JsonNode firstObject = node.get(0);
					if (firstObject.isObject()) {
						if (firstObject.has("reason")) {
							System.out.println(
									firstObject.has("reason") ? firstObject.findValue("reason").asText() : null);
						} else {
							String fieldName = null;
							if (firstObject.fieldNames().hasNext()) {
								fieldName = firstObject.fieldNames().next();
							}
							JsonNode reason = null;
							if (Objects.nonNull(fieldName)) {
								reason = firstObject.findValue(fieldName);
							}
							if (!reason.isNull() && reason.isArray()) {
								JsonNode secondObject = reason.get(0);
								if (secondObject.isObject()) {
									if (secondObject.has("reason")) {
										System.out.println(secondObject.has("reason")
												? secondObject.findValue("reason").get(0).asText()
												: null);
									}
								}
							}

							
						}

					}
				} catch (Exception e) {
					System.out.println("Exception occured");
				}

			}
		}
		
	}
}


/// int = (int) long like this big to small need explicit then 

class CheckNumbersType {
	public void checkNumbers() {
		// Boxing primitive to Wrapper (operations performed automatic conversion (without cast)). but you can type (cast) 
		int v1 = 10; Integer v2 = v1; v1 = v2; long v3 = 20; Long v4 = v3;
		
		// un-boxing from wrapper to primitive type automatic conversions.
		Long v5 = 30l; v3 = v5;
		Number num = v1; v5 = (long)v1; v1 = (int) (v5.longValue());
		
		// widening and narrowing with primitives
		int v6 = 20; long v7 = 20; 
		v7 = v6; // wide small store in big
		v6 = (int)v7; // narrow big try to fit in small so narrow it data loss.
		
		Long v8 = (long)v6; Long v10 = (long)v8.intValue();
		Short aa = 20; Integer ab = 23; Long ac = 20L; Double ad = 203.34;
		ab = (int)aa; ab = ac.intValue(); ab = (int)((long)ac);
		int ae = 10; short ah = 33; long af = 20; double ag = 23;
		
		// boolean, short, int, float, long, double 
		// Boolean, Short, Integer, Float, Long, Double
		
		short add = (short)((int)ab);
		aa = ac.shortValue();  ac = aa.longValue();
		Object a11 = v8;
		long a12 = (Long)a11;
		ah = ((short)(int)ab);
		add = (short)((int)ae);
		ae = aa; ac = (long)ag;
		ag = aa;
		ac = (long)((double)ad);// or
		ac = ad.longValue();
		ad = (double)((long)ac);
		ad = ac.doubleValue();
		ah = ab.shortValue();
		ab = (int)af;
		
		
	}
}
