package com.demo.demoHub.Utility;

public class BasicPractice extends Practice {

	int marks = 50; // instance variable are change when we pass object refernce to any method and
					// change values.
	int number;
	String s;
	Raju raju;
	static String companyName;
	private static final String name = "yours";
	static final short i = 2;
	public static int j = 0;
	
	public BasicPractice() {};

	public BasicPractice(Raju raju) {
		this.raju = raju;
		raju.setMarks(19);
	}

	@Override
	public String toString() {
		companyName = "yes_bank";
		return "BasicPractice [marks=" + marks + ", number=" + number + "," + s + "]";
	}

	public void changes(int marks) {
		System.out.println(marks);
		marks = marks + 40; // only local variable will change not the object var.
		System.out.println(marks);
		beCool();
	}

	static void changeObjectRef(BasicPractice bs) {
		bs.marks = 200;
		bs.number = 50;

	}

	public static void beCool() {
		System.out.println("sure...");
	}

	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		return false;
	}
	public static void swap(Integer a , Integer b) {
		Integer temp = b;
		b = a;
		a = temp;
	}

	
}
class Practice {
	int id;
	String name;
}

class Main {
	public static void main(String[] args) {
		Raju rs = new Raju();
		BasicPractice bs = new BasicPractice(rs);
		BasicPractice.changeObjectRef(bs);
		System.out.println(bs.toString());
		System.out.println("\"abc\"");
		BasicPractice bss = bs;
		System.out.println(bs.equals(bss));
		for (int k = 0; k < 3; k++) {
			switch (k) {
			case BasicPractice.i:
				System.out.println(" 0 ");
			case BasicPractice.i - 1:
				System.out.println(" 1 ");
			case BasicPractice.i - 2:
				System.out.println(" 2 ");
			}
		}
		
		Integer a = 10;
		Integer b = 30;
		BasicPractice.swap(a,b);
		System.out.println(a +"  "+ b);
		
		Practice p = new BasicPractice();
		BasicPractice bd = (BasicPractice)p;
		
		BasicPractice bp = new BasicPractice();
		Practice pp = bp;

	}
}
