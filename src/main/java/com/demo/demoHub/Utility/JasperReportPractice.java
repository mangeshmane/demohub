package com.demo.demoHub.Utility;

import java.io.File;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.demo.demoHub.model.User;
import com.itextpdf.commons.utils.Base64.InputStream;

import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

public class JasperReportPractice {

	public static void main(String[] args) {
		
		String path = "C:\\Users\\mange\\STS-master\\demohub\\src\\main\\resources\\Simple_Blue.jrxml";
		String newFilePath = "C:\\Users\\mange\\OneDrive\\Desktop\\New folder\\pdf\\jsperFile.pdf";
		// key of the parameter & value for the paramter text fields.
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("studentName", "John carter");
		
		List<User> list = Arrays.asList(new User(1,"abcd","efgj","ijkl","mnop"),new User(2,"qrst","uvwx","y","z"));
		JRBeanCollectionDataSource source = new JRBeanCollectionDataSource(list);
		try {
			//compile the jrxml file to .jasper file.
			JasperReport compileReport = JasperCompileManager.compileReport(path);
			
			//send data to the compiled file to save in the report
			JasperPrint fillReport = JasperFillManager.fillReport(compileReport, map, source);
			
			//now create or export the report into any format ex. pdf
			JasperExportManager.exportReportToPdfFile(fillReport, newFilePath);
			File file = new File(newFilePath);
			if(file.exists()) {
//				file.delete();
			}
//			file.createNewFile();
			InputStream a;
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

}
