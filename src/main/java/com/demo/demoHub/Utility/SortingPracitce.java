package com.demo.demoHub.Utility;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class SortingPracitce {

	public static void main(String[] args) {
		List<Bike> list = Arrays.asList(new Bike(29,"splender"),new Bike(9,"splender"),new Bike(23,"splender"),new Bike(22,"splender"));
	//	list.sort(); // Only works with comparator because only accept comparator use collection.sort();
		Collections.sort(list); // It accepting the object because list implements the comparable class methods or it is a object of comparable.
		System.out.println(list);
		
		List<Car> carList = Arrays.asList(new Car(29,"splender"),new Car(9,"splender"),new Car(23,"splender"),new Car(22,"splender"));
		// Use comparator now.
		carList.sort(new Comparator<Car>() {

			@Override
			public int compare(Car o1, Car o2) {
				if(o1.getNumber() > o2.getNumber()) 
					return 1;
				else if(o1.getNumber() < o2.getNumber())
					return -1;
				else
					return 0;
						
		
			}
		});
		
		List<Car> carList1 = Arrays.asList(new Car(29,"splender"),new Car(9,"splender"),new Car(23,"splender"),new Car(22,"splender"));
		carList1.sort((c1,c2) -> c1.getNumber().compareTo(c2.getNumber()));
		
		List<Car> carList2 = Arrays.asList(new Car(29,"splender"),new Car(9,"splender"),new Car(23,"splender"),new Car(22,"splender"));
		carList2.sort(Comparator.comparing((c) -> c.getNumber()));

		System.out.println(carList);
		
		Vehical [] vehical = {new Car(), new Bike()};
		for(Vehical veh : vehical) {
			System.out.println(veh);
		}
		/*
		 * // It is ok but when we are trying to send the dog array to the animal array and try to 
		 * change the animal and try to add the cat in there jvm say hey don't, add the dog we are saving only dogs here.
		 * so in case of polymorphism we can call the overriden method for each animal child.
		 */
		List<Vehical> vehicalList = new ArrayList<Vehical>();
		vehicalList.add(new Car());vehicalList.add(new Car());
		printVehical(vehical); // Works fine here because sending the vehical array.
		Bike[] bikeArray = {new Bike(), new Bike()};
		printVehical(bikeArray); // But here jvm know what we are sending when we try to add car in the bike array it says array store exception.
		List<Vehical> bikeList = new ArrayList<>();
//		List<Bike> bikeList = new ArrayList<>();
		bikeList.add(new Bike());
		printVehical(bikeList); //  compile time issue when we send List<Bike> instead of List<Vehical> no match.
		
		GenericsVehical<String> vehicalType = new GenericsVehical<>("Tyep is String");
		vehicalType.getType();
		
		GenericsVehicalWithBound<Bike> boundedVehical = new GenericsVehicalWithBound<Bike>(new Bike());
		boundedVehical.getType();
		
		GenericsVehicalWithArray<Integer> arrayInit = new GenericsVehicalWithArray<Integer>();
		arrayInit.addElementInArray(12321);
		arrayInit.addElementInArray(123221);
		arrayInit.getType();
		// when we call the above method we are writing just like below.
//		List<Vehical> vehicalList = new ArrayList<Bike>(); Here we are getting compiler error. inside the angular bracket they have to match.
		//jvm does not know the type so compiler has to step in.
		
		// But we can use wild card generics with extends and super for the type like below.
		List<? extends Vehical> extendedList = new ArrayList<Bike>(); // Works here because we are providing type of ? is vehical and below vehical like car and 
//		extendedList.add(new Vehical()); This list is read-only and used in the method parameter only to get the list of vehical and below.
		
		// And super means super class of given class or itself class not below of that.
		List<? super Bike> superList = new ArrayList<Vehical>(); // Bike, Vehical, and objects are allowed in Array List.
		// But in super we can change the list or add the new elements into it.
		
		// Above we call the bounded generics lower bound and upper bound generics.
		// But here when we try to print these list we have to get it in object because we don't know the actual type like cat or animal is come for print
		// because bike is object and animal is also object.
		}
	//So this method is accepting the vehicle and any class extends to vehicle.
	public static void printVehical(Vehical[] vehical) {
		//This animal array is non-change array when we try to add the different animal other than called animal
//		vehical[0] = new Car(); // For sending the bike array it will not works because jvm knows the type.
		vehical[1] = new Bike(); 
		// Everything works fine till here but when we send the dog or cat array to this method then problem arises.
		
	}
	
	//So this method is accepting the vehicle and any class extends to vehicle.
		public static void printVehical(List<Vehical> vehical) {
			/* 
			 * It works for vehicle class perfectly because we are sending List<Vehical> but when we are sending
			 * list of any derived class like List<Bike> or List<Car> because in generics polymorphism not work 
			 * for both <> angular value must match work for base classes like
			 * List<> list = ArrayList<>(); like list and array list parent look for child but
			 * polymorphism not works for the <> generics , like List<Animal> list = new ArrayList<cat>() not works
			 * it gives the compile time error becuase jvm doesn't know the type of the data. 
			 */
			
			vehical.add(new Bike());
		}
		
		public static void printVehicalWithGenerics(List<? extends Vehical> vehical) {
			// The Vehical list is read-only can't add any other vehical or bike or car
			//because if we are sending the car and try to add the bike here then type safety we break and
			// cause the run time exception. so compile-time exception like below.
//			vehical.add(new Bike());
		}
		
		/*
		 * At the end it says in array polymorphism works for basetype compiler doesn't know the type jvm does.
		 * but in list compiler know the type at compile time but value in the generics<> must match.
		 */
	
	
}
// The beauty of the generics is we can know the object type at the compile time so give error if we are storing the different object. instead of runtime.
// generics class without bound. we use the generics classes to store the object of any type in the class.
class GenericsVehical<T> {
	
	T type; // Instead of giving only one type we are giving generics with initialization of the class so we can create any type of data using this class.Like:int,char,or any object.
	
	public GenericsVehical(T type) {
		this.type = type;
	}
	
	public GenericsVehical() {
	}
	void getType() {
		System.out.println(type);
	}
}

// This class has a limit means only sends the data with extended or lower type of data.
// Class with bounded Type only send the upper and lower bound Type of data. with classes, interfaces or methods.
class GenericsVehicalWithBound<T extends Vehical> {
	
	T type; // Instead of giving only one type we are giving generics with initialization of the class so we can create any type of data using this class.Like:int,char,or any object.
	
	public GenericsVehicalWithBound(T type) {
		this.type = type;
	}
	
	public GenericsVehicalWithBound() {
	}
	void getType() {
		System.out.println(type);
	}
}
// At the time of object creation T or any replaced with actual object.
class GenericsVehicalWithArray<T> {
	
	T [] typeArray = (T[]) new Object[10]; 
	Integer counter = 0;
	
	public void addElementInArray(T type) {
		typeArray[counter++] = type;
	}
	
	public GenericsVehicalWithArray() {
	}
	T getType() {
		return typeArray[counter++];
	}
}

class WildCardClass<T> {
	
}
// We are defining the type of object which we are going to store at creation of the generic class.
class Car extends Vehical {

	private Integer number; // If the data type is primitive type then it won't work on it only Objects allow.
	private String name;
	
	public Car() {
		super();
	}

	public void printState() {
		System.out.println("here is a state....");
	}
	public Car(Integer number, String name) {
		super();
		this.number = number;
		this.name = name;
	}

	public Integer getNumber() {
		return number;
	}

	public void setNumber(Integer number) {
		this.number = number;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "Bike [number=" + number + ", name=" + name + "]";
	}
}

class Vehical {
	
	public Vehical() {
		// TODO Auto-generated constructor stub
	}
	
}
class Bike extends Vehical implements Comparable<Bike> {

	private Integer number; // If the data type is primitive type then it won't work on it only Objects allow.
	private String name;
	
	public Bike() {
		super();
	}

	public Bike(Integer number, String name) {
		super();
		this.number = number;
		this.name = name;
	}

	public int getNumber() {
		return number;
	}

	public void setNumber(int number) {
		this.number = number;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "Bike [number=" + number + ", name=" + name + "]";
	}

	@Override
	public int compareTo(Bike o) {
		/*
		 *  why we used the compareTo() method because in Integer class it already implemented
		 *  so just need to call here and sort the object based on the the value returned from the 
		 *  compareTo() method to the collection.sort() method.
		 *  This method return the results lexicographically if equal 0 if x<y 1 and if x>y -1 like wise
		 *  Then sort method sort the elements based on the results for compartTo();
		 */
		return this.number.compareTo(o.number);  // Here you can also add your own implementation and return the answer like 0,1,-1.
	}
	
}