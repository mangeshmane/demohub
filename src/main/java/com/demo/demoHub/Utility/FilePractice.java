package com.demo.demoHub.Utility;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Date;
import java.util.Locale;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;

public class FilePractice {

	public static void main(String[] args) throws ParseException {
		try {
			String path = "https://sarcsdocumentsprod.blob.core.windows.net/rcsprodcontainer1/parent/105417/docs/objmod/14/1688024569171_außenansicht.jpg";
			try {
				
				HttpClient httpClient = HttpClientBuilder.create().build();
				HttpGet httpGet = new HttpGet(path);

				HttpResponse response = httpClient.execute(httpGet);
				response.getEntity().getContent();
				int statusCode = response.getStatusLine().getStatusCode();
			    URL url = new URL("https://sarcsdocumentsprod.blob.core.windows.net/rcsprodcontainer1/parent/105417/docs/objmod/14/1688024569171_außenansicht.jpg");

			    // Open a connection to the URL
			    HttpURLConnection connection = (HttpURLConnection) url.openConnection();

			    connection.setAllowUserInteraction(true);
			    // Check the response code
			    int responseCode = connection.getResponseCode();
			    if (responseCode == HttpURLConnection.HTTP_OK) {
			        // Create output stream for compressed image
//			        BufferedImage image = ImageIO.read(connection.getInputStream());
			        // Process the image
			    } else {
			        System.out.println("Error: HTTP " + responseCode);
			        // Handle the error condition accordingly
			    }

			    // Close the connection
			    connection.disconnect();
			} catch (IOException e) {
			    e.printStackTrace();
			    // Handle any other exceptions that may occur
			}
			
			Locale locale = Locale.GERMAN;
			NumberFormat numberFormat = NumberFormat.getNumberInstance(locale);
			Number number = numberFormat.parse("1223232323232");
			String string = numberFormat.format(number);
			
			FileInputStream input = new FileInputStream(new File("C:\\Users\\mange\\OneDrive\\Desktop\\FileDoc.txt"));
			int readChar;
			File out = new File("C:\\Users\\mange\\OneDrive\\Desktop\\FileDoc1.txt");
			if(!out.exists()) {
				out.createNewFile();
			}
			BufferedInputStream inputStream = new BufferedInputStream(input);
			FileOutputStream outputStream = new FileOutputStream(out);
			byte[] charArray = new byte[1024];
			//this will directly read whole charArray and put in the charArray
			while(inputStream.read(charArray) != -1) {
				outputStream.write(charArray, 0, charArray.length);
			}
			//here
			while((readChar = inputStream.read()) != -1) {
				outputStream.write(readChar);
				System.out.print((char)readChar);
			}
			inputStream.close();
			
			outputStream.write(charArray, 0, 1024);
			outputStream.close();
			System.out.println(out.delete());
			
		} catch (FileNotFoundException e) {
			System.out.println("Exception occured due to :" + e.getMessage());
			e.printStackTrace();
		} catch (IOException e) {
			System.out.println("Exception occured due to :" + e.getMessage());
			e.printStackTrace();
		}
		System.out.println("please check the file:");
	}
	
	public File getTempFile() {
		File tempDirectory = new File(System.getProperty("java.io.tmpdir"));
		File file = new File(tempDirectory.getAbsolutePath() + File.separator + "tmpFile" + new Date().getTime() + ".pdf");
		return file;
	}
}
