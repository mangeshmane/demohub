package com.demo.demoHub.Utility;

public class JpaPractice {

	/*( Mismatch or difference between oo and rdb)
	 * 1. what are the object orientation and RDMS data
	 * IN objects there is address(object references(obj == obj) and java objects where as how we know any objects in database using primary key.
	 * 2. Associations between objects using unidirectional and bidirectional 
	 * e.g: 1To1 ,1ToMany,manyTo1, manyTomany.
	 * where in db everything is bidirectional.using foreign key. we associate the relation between two entities.
	 * 3. how we get data for associated entity is to call person.getAddress() methods.
	 * where in database we get data using queries and joins between the two tables data.
	 * 
	 * so how to connect the both : create the bridge between class and rdbms tables. where we uses the annotations or xml configurations.
	 * 
	 * Back in the day we are using the hibernate and many vendors like mybatis to provide the ORM Tools or we can say connection configurations.
	 * But they were providing their own implementations and thing are getting worse when we want change the ORM tool from one to another.
	 * 
	 * So then JPA(Java Persistence Api's) comes into picture.
	 * JPA is the specifications for the ORM tools. And also it create bridge between OO and RDM.
	 * What is Specification(एखादी गोष्ट कशी करायची याबाबत पष्ट तपशीलवार आदेश):- It describes what kind of feature it should be included but doesn't have its own implementations.
	 * implementations leaves to the different vendors like hibernate ...etc. 
	 * But now a days most of the features are similar to Hibernate.
	 * Because the reason behind is when everyone has their own implementation for bridge between OO and RDBMS then>
	 * JPA comes out and standardize the set of rules for the implementation of the bridge.
	 * If any one want to implements the bridge or Create ORM then it must have to follow the specification which is
	 * provided by the JPA(Java Persistence Api's)
	 * One of the best implementation of JPA is Hibernate.
	 * Here all vendors follows same specifications. therefore no need to change code when jump from one vendor to another.
	 */
	
	// N-Tier application.
	// presentation layer : view and controller
	// domain layer : models JPA use(annotations)
	// persistence layer or Dao layer or repo layer: where we have the hibernate implementations.
	
	/*
	 * Hibernate architecture:-
	 * 
	 * Java api's 
	 * @Temporal on the @column field: if we set the date in db it sets the by default value like date and time aslo
	 * so if we want to set the data use LocalDate instead.@Temporal(temporal = type.date);
	 * 
	 * @Access : It has two type 1.field access 2.property access.
	 * Field access:when we add the @Id field on the or any field then hibernate will populate the data using the 
	 * reflection api instead setter and getter so it breaks the encapsulation, so to use getter setter property instead of reflection 
	 * use @Access(propertyName). 
	 * PROPERTYACCCESS: In this we add annotations on the property field/ methods (setter/getter).
	 * 
	 * IMPORTANT: Remember where the @Id annotation written decide the field access or property Access.
	 * so when we add the @Id on the field then it's Field access and hibernate will use the reflection api's to set and get the data to object.
	 * and When we add the @Id on the getter method then it is property access , then hibernate will use the getter and setter to set and get the value.(just not to break the encapsulation.
	 * 
	 * 
	 */
	/// Entity Life Cycle    (Persistence Context (PC))
	/*
	 * Entity Life Cycle: It has Four states (Persistence Context:PC)
	 * 1.Transient : First when we create the new object it outside the persistence context/first level cache/session .
	 * 2.Managed/Persisted : When we write the em.persist(p1); it will create the gets add in the persistence context and when we commit it will save to the database.
	 * 3.Detached: when we detached or em.detached()/em.close() it goes from persistence context to state like transient but here difference is transient entity doesn't have the primary key,
	 * 			   but detached entity does have primary key. And when we write em.merge() it again goes to PC but there it create new space and also update the changed value to database.
	 * 4.Removed: We use em.Remove(p1) to remove the object from persistence context and also from database.(delete).
	 * All the create/update operations save the persistence context data to DB after the commit.
	 * create : use em.persist(p1) update em.merge(p1) delete : em.remove(p1); find: em.find(p1.id);
	 */
	
	// @PersistanceContext EntityManager em; here we are getting the em object from the persistence context not from spring IOC container.
	/*
	 * we can get the em object from 
	 * EntityManagerFactory emf =  Persistance.createEntityManagerFacotry( unitName:from the xml file);
	 * emf.createEntityManager(); now call the api to save update find and delete api's in the em.remove();
	 */
	
	// em.perist(p1);
	/*
	 * em.persist(p1); save the object in the persistence context firstly(first level cache) fire the insert query all the PC object are going to save in the db directly.
	 * so before commit if we change the p1 object then it will automatically update the query to update(query) p1 instead of insert and push the changes to db.
	 * hibernate track all the managed/persisted entity before we close the em or before the any commit.
	 */
	
	//em.find(Obj.class,1L) and em.getReference(person.class, 1L); class and primery key (id) findbyId; 
	/*
	 * here em.find() method first check if object is present in the persistence context if it does not present then directly call for db (fire select query).
	 * but em.getReference() method first check present or not if not then it doesn't call for db instead
	 * it return the proxy object. it actually call the query when we get something from that proxy object
	 *  like p.getUsername() automatically call the db.
	 *  
	 *   Here use of reference() is used when we want to less the db call actually best use it when we
	 *   remove the entity from the db.
	 *   
	 *   1st call to db).em.find() <--> persont   <---  db
	 *   em.remove(p) --- 2nd call to -- db;
	 *   
	 *   em.refernce() ---- return proxy object from(Not a actual call to db) -- db
	 *   em.remove() here the actual call to db with proxy object having same primary key that we asked for.
	 */
	
	// IMP: Here when we perform add update operation it object first save to the PC and then save to db.
	
	/*
	 * em.merge(): when we detach the object from persistence context of db it come out of PC but it has the primary key 
	 * and when we change that detached object value like p1.setName("something") then when we try to save again then it give us the error that same type of entity present in PC
	 * so we need to use em.merge(p1) method to update that object in db. first push that updated object in the PC and then push that to db automatically.  
	 */
	/*
	 * Flow: Person p1 = em.find(Person.class,1l); First get the object (entity)
	 * em.detach(p1); then if we want to update the entity we will detach throw the entity out of the PC.
	 * if again we fetch the entity from db let say p2 so p2 is same id as p1 
	 * if we try to persist the p1 it say entity with same id already in persistence context so instead 
	 * of saving we merge(update) the entity em.merge(p1) so it will create new instance in hibernate Persistence context
	 * and directly update the p1 identity in db. So just use merge when you detach or update the record.
	 * If em.merge(p1) then it first check same id object is present in the PC if not then create the new object in PC and then save it in db.
	 * and if present it only copy the state means only changed property of that object and update it in db.   
	 * 
	 */
	
	// Just remember that where is your object it is managed/persisted entity or outside of the PC.
	// Where is the object pointing to because if it point to the detached object then chaning the values of object doesn't change anything in db.
	// but if it is point to the object inside the PC then on change of property of that object it save that changes in db.
	// so if the entity is transient and we directly em.merge(p1) then it directly create the object inside PC and save to db 
	// but that p1 object is still pointing to old transient object so if we change the object value it won't save the object again 
	// you need write or get the object p1 = em.merge(p1) then p1 will point to the managed entity in the PC this is how merge() works.
	
	// Difference between persist() and merge()
	/*
	 * Persist the transient entity to PC and save to db and p1 object point to object inside the PC it doen't return nothing. also on changing of that object value it save to the db.
	 * But merge can save the transient entity in PC and save to db but if the entity would not be point to object inside the PC we need explicitly point to that entity.
	 * also merge can update the entity if the id is already present in the db but persist can't it will throw entity present exception.
	 * If the @Id is the identity then it directly insert into the database that is Identity works.
	 * IMP:
	 * In persist(p1) insert query for Identity strategy and p1.setName() and commit it hit the update query and save update to PC also.
	 */
	///IMP : The whole process start with Transaction to the db : begin start with transaction save update with in it and in between if we change the object value then it will hit to the db with update query.
	/// 	it is  very important that before commit don't change the object value it will change in db also.
	/// before remove if we change the object value it is not directly hit the db because hibernate hold the operations till the end because it know the operation gone be remove it will not update it.
	// some time operations will be the only updated in PC
	// contains() method check the object is inside the cache or not.
	// Remove() : remove can only remove the managed/persist object not the transient or detached object.
	
	// IMPORTANT: All the operation like insert update remove are not performed immediately it store the changes to Cache(PC) 
	// only when we commit a transaction, query and flush it goes to hit db and save the changes.
	// so all the objects are in the cache will instantly save to db after those commit,query and flush.
	// In commit operations all objects changes goes to db right before .commit(). also before query call it save PC object to db and hit db and flush also same.
	// This is only case when we using the natural key like without auto generation or other than number.
	// So merge() is not automatically hit the db before commit ,query or flush keep in mind 
	// if we want to save the changes immediately then write flush() after merge().
	// .clear() will detach all the object inside the cache. em.contains() check if the object is present in cache (PC).
	
	
	//Type Of Associations: unidirectional and bidirectional.
	/*
	 * unidirectional : one object is present in another class . person has address. but address class doesn't have person like this.
	 * In Object orientation (OO) : below are associations but in database everything is bidirectional like we can get object from foreign key and joins.
	 * Association            unidirectional             bidirectional
	 * one-to-one                 Yes                        Yes
	 * one-to-many                Yes						 No        -- if list of Address present in one class it can't be present in List of person.
	 * many-to-one 				  Yes                        No        -- combination of one-to-many and many-to-one will create the bidirectional.
	 * many-to-many               Yes                        Yes       -- same like one-to-one
	 * 
	 */
	
	/* If the configuration for the hibernate is drop-update-create it create the same table as class property and like ManyToOne as foreign key
	 * Many-to-One (bi-directional) : @JoinColumn is a foreign key also has @Join table is a foreign key.
	 * If we only write manytoone on the field of property then it will create the name of the primary key of its table _field name. table name = foreignKeyFieldName_columnIdName person_person_id.
	 * just use @joinColumn(name = "actualTableColumnName") to give name to column.
	 * For JoinColumn clumn as a foreign key it can add the more than one null value in column but this is not normalized table so use @jointable it create the new table with one id and another column id.
	 * @joinTable(name = "abc", joincolumn = {@joinColumn(name = "abc_id)} ,inverseJoinColumn = {@joinColumn (name = "samecolumn id)})
	 * @joinTable with one-to-many(unidirectional)  : for one to many case many side is unique so by default OneToMany is @joinTable(Its like @JoinColumn to give default name to columns). so if we don't add many side as unique then it will work like many to many it's like foreign key can store multiple time but not the primary key.
	 * 
	 * Many side is always unique because one person has many address in this if person side 1 can repeat itself 
	 * but if many addresses should not repeat reason behind is simple if 1 has many address for one address 
	 * it should not repeat for same user many means different addresses for one person.
	 * If we consider another example for Many-To-One Many side is primary key(can't repeat) side and for many address there is only one person. 
	 *  
	 * One-To-Many uni-directional using join column. add the JoinColumn on OneToMany side it will create the 
	 * one side means person side person_id on the Address table but its association is not present in the address table.
	 * 
	 * 
	 * One-To-Many and Many-To-One bidirectional.
	 * In this case we add Many-To-One and One-To-Many it will create the three table one is person another will be person_address caused by One-To-Many by default join table feature and Many-To-One it add the person foreign key in the address table.
	 * To avoid this use the mappedBy(name = "fieldName") in the One-To-Many it says drop the relationship from my side and use the foreign key of another class.
	 * using this we are now bidirectional. Address table will have the foreign key.
	 */
	
	// One-To-One unidirectional using join column:
	/*
	 * Its same as the many to one with join column so just add the foreign key in the person table one person has the one address.
	 * One-To-One Bidirectional:- just add the mapped by in either side of the table so remove the association or give the relationship.
	 * 
	 * 
	 * Many-To-Many : It is same as the one to many and many to one uni direactional and bidirectinal mapping
	 * u can use the List<object> in one class and use the join table to give names to columns using join-column and inverse join column it create new join table
	 * also add the List<object> in another class and use the mapped by in either of the class to drop the one of its new created column and so use mapped by in either of its column.
	 * 
	 */
	
	// Cascades : persist, detach, merge, refresh, remove these are the some examples of the cascades its means 
	// if we have the one the one relationship in the class and trying to save the main class object with foreign key
	// object it has to save the child(foreign key) object also so need to add the configuration, for that so use one of the cascade type to save or delete or merge refresh the object.
	// so in this case add the cascade where there is no foreign key present like in where the one-to-many written and mapped by written.
	// for saving the foreign key we need the actual object present in the database first. cascade = cascadeType.all,persist ,remove.
	
	// ORM with inheritance: If Account is a parent and saving and current account are their child then how ORM works.
	
	// Cartesian product: This is occurs when we join the multiple two or more collection in entity e.g:
	/*
	 * One customer has many @oneToMany books and @oneTomany movies so for each customer all movies and product will get but result will be like  
	 * cust  book1  movie1 : cust book1 movie2 and cust book1 movie3 like wise for all customers books movies print again and again This 
	 * is called the Cartesian product issue to solve the issue don't query multiple collection in single query.
	 * 
	 * for e.g: from customer c left join c.books left join c.moives;
	 * instead just join only one collection at time.
	 * 
	 * N + 1 problem : This is big issue that when we get the customer with many address or books so 
	 * 
	 * for one customer we get response for one customer and contain list of books or addresses:
	 * In this first query is select * from customer where id = anyId but when we write c.getBooks() in response
	 * then for every books select statement for that customer so if the books are N number select hit N times :
	 * this will slow down the process and code called the N + 1 problem. N means = n number of select statements fired 
	 * and One (1) means for single customer(entity).
	 */
}















