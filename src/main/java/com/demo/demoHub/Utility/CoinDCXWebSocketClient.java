package com.demo.demoHub.Utility;

import java.net.URI;
import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Future;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import javax.websocket.ClientEndpoint;
import javax.websocket.CloseReason;
import javax.websocket.ContainerProvider;
import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.WebSocketContainer;

import com.google.gson.Gson;

@ClientEndpoint
public class CoinDCXWebSocketClient {

//	 private static final String SOCKET_ENDPOINT = "wss://stream.coindcx.com/socket.io";
	 private static final String SOCKET_ENDPOINT = "wss://stream.coindcx.com/socket.io/?EIO=3&transport=websocket";
	    private static final String API_KEY = "8b70f5ab7dea01be2808f6d230bd63d10fc840a70104d68e";
	    private static final String SECRET = "c71c6085a1bd6463463c7dbd657026816a1bf621fddcc559a54d228354b097d8";
	  private static String thirdChannel = "42[\"join\","
        		+ "{\"channelName\":\"" + "currentPrices@spot@10s" + "\"},{\"x-source\":\"web\"}]";
	    public CoinDCXWebSocketClient() {
	    	
	        connectToWebSocket();
	    }

	    @OnOpen
	    public void onOpen(Session session) throws NoSuchAlgorithmException, InvalidKeyException, InterruptedException {
	        String channel = "coindcx";
	        String channel3 = "currentPrices@spot@10s";
	       
	        String firstChannel = "[\"join\","
	        		+ "{\"channelName\":\"" + channel + "\"},{\"x-source\":\"web\"}]";
	        String authSignature = generateAuthSignature(firstChannel, SECRET);
	        
	        String secondChannel = "[\"leave\", {\"channelName\": \"coindcx\"}]";
	        String secondChannelLeave = generateAuthSignature(secondChannel, SECRET);
	        
//	        String thirdChannel = "[\"message\":\"join\","
//	        		+ "{\"channelName\":\"" + channel3 + "\"},{\"x-source\":\"web\"}]";
	        String thirdChannel = "42[\"join\","
	        		+ "{\"channelName\":\"" + channel3 + "\"},{\"x-source\":\"web\"}]";
	        String thirdChannelJoin = generateAuthSignature(thirdChannel, SECRET);
	        
	        Map<String,String> message = null;
	        
	        message = getMap(thirdChannel, authSignature, API_KEY);
//	        Future<Void> callSendTextMethod = null;
//	        Future<Void> sendText = session.getAsyncRemote().sendText(jsonMapToString(message));
	        Future<Void> callSendTextMethod = session.getAsyncRemote().sendText(thirdChannel);
//	        callSendTextMethod = callSendTextMethod(message, session);
//	        System.out.println(callSendTextMethod.toString());
//	        
//	        message = getMap(channel, secondChannelLeave, API_KEY);
//	        callSendTextMethod = callSendTextMethod(message, session);
//	        System.out.println(callSendTextMethod.toString());
//	        Boolean check = true;
//	        
//	        message = getMap(channel3, thirdChannelJoin, API_KEY);
//	        callSendTextMethod = callSendTextMethod(message, session);
//	        System.out.println(callSendTextMethod.toString());
	        String thirdChannelJoins = generateAuthSignature("new-trade", SECRET);
	        message = getMap("B-XRP_ETH", thirdChannelJoins, API_KEY);
	        callSendTextMethod = callSendTextMethod(message, session);
	        System.out.println(callSendTextMethod.toString());
	       
	        
	    }

	    Future<Void> callSendTextMethod(Map map,Session session) {
	    	return session.getAsyncRemote().sendText(jsonMapToString(map));
	    }
	    
	    Map getMap(String channel,String authSign,String apiKey) {
	    	Map<String, String> message = new HashMap<>();
	        message.put("channelName", channel);
	        message.put("authSignature", authSign);
	        message.put("apiKey", apiKey);
	        return message;
	    }
	    
	    @OnMessage
	    public void onMessage(String message) {
	    	System.out.println("yes we are getting the messages from server...");
	        System.out.println("Received message: " + message);
//            Gson gson = new Gson();
//            Object tradeUpdate = gson.fromJson(message, Object.class);
//            System.out.println("Received trade-update event: " + tradeUpdate.toString());
            
	    }

//	    @OnClose
//	    public void onClose(Session session, CloseReason reason) throws InterruptedException {
//	    	Thread.sleep(1000000l);
//	        System.out.println("WebSocket closed: " + reason.getReasonPhrase());
//	    }

	    @OnError
	    public void onError(Session session, Throwable throwable) {
	        System.err.println("WebSocket error: " + throwable.getMessage());
	    }

	    private void connectToWebSocket() {
	    	new DepthUpdateWebSocketClient();
	        WebSocketContainer container = ContainerProvider.getWebSocketContainer();
	        try {
	            Session connectToServer = container.connectToServer(this, new URI(SOCKET_ENDPOINT));
	            connectToServer.setMaxIdleTimeout(10000);
	            String sessionActivatedEvent = org.apache.catalina.Session.SESSION_ACTIVATED_EVENT;
	            String sessionCreatedEvent = org.apache.catalina.Session.SESSION_CREATED_EVENT;
	        } catch (Exception e) {
	            e.printStackTrace();
	        }
	    }

	    private String generateAuthSignature(String message, String secret)
	            throws NoSuchAlgorithmException, InvalidKeyException {
	        Mac sha256_HMAC = Mac.getInstance("HmacSHA256");
	        SecretKeySpec secret_key = new SecretKeySpec(secret.getBytes(StandardCharsets.UTF_8), "HmacSHA256");
	        sha256_HMAC.init(secret_key);
	        byte[] hash = sha256_HMAC.doFinal(message.getBytes(StandardCharsets.UTF_8));
	        return Base64.getEncoder().encodeToString(hash);
	    }

	    private String jsonMapToString(Map<String, String> map) {
	        StringBuilder sb = new StringBuilder("{");
	        for (Map.Entry<String, String> entry : map.entrySet()) {
	            sb.append("\"").append(entry.getKey()).append("\":\"").append(entry.getValue()).append("\",");
	        }
	        sb.deleteCharAt(sb.length() - 1); // Remove trailing comma
	        sb.append("}");
	        return sb.toString();
	    }

	    public static void main(String[] args) throws InterruptedException {
	        new CoinDCXWebSocketClient();
	        Boolean check = true;
	        while(check) {
 	        	Thread.sleep(1000l);
 	        	check = false;
 	        }
	    }
}


@ClientEndpoint
class DepthUpdateWebSocketClient {

    private static final String SOCKET_ENDPOINT = "wss://stream.coindcx.com/socket.io/?EIO=3&transport=websocket";

    private static String thirdChannel = "42[\"join\","
    		+ "{\"channelName\":\"" + "currentPrices@spot@10s" + "\"},{\"x-source\":\"web\"}]";    
    public DepthUpdateWebSocketClient() {
        connectToWebSocket();
    }

    @OnOpen
    public void onOpen(Session session) {
        System.out.println("WebSocket connection opened.");
        // Subscribe to the desired channel here (e.g., "B-BTC_USDT")
        subscribeToChannel(session, "B-BTC_USDT");
    }

    @OnMessage
    public void onMessage(String message) {
        // Parse and process the incoming "depth-update" event
        System.out.println("Received depth-update event:");
        System.out.println(message);
    }

//    @OnClose
//    public void onClose(Session session) {
//        System.out.println("WebSocket connection closed.");
//    }

    @OnError
    public void onError(Session session, Throwable throwable) {
        System.err.println("WebSocket error: " + throwable.getMessage());
    }

    private void connectToWebSocket() {
        WebSocketContainer container = ContainerProvider.getWebSocketContainer();
        try {
            Session connectToServer = container.connectToServer(this, new URI(SOCKET_ENDPOINT));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void subscribeToChannel(Session session, String channelName) {
        // Construct and send a subscription message for the specified channel
        String subscriptionMessage = "{\"channel\":\"" + channelName + "\"}";
        session.getAsyncRemote().sendText(thirdChannel);
    }

    public static void main(String[] args) {
        new DepthUpdateWebSocketClient();
    }
}
