package com.demo.demoHub.Utility;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;

import org.hibernate.internal.build.AllowSysOut;

import net.bytebuddy.asm.Advice.AllArguments;

/*
 * Annonymous class is used to provide implementation of the interface or parent class
 * without creating the new class for interface or any child class to give implementations.
 * also lambda does not create the .class file for implementation.
 * 
 *      work with functional interface 
 *   	the interface having the only one abstract method called the functional interface 
 *   	also has @functinalInterface annotation.*/

class Animal {
	String name;
	String type;
	int a;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public int getA() {
		return a;
	}

	public void setA(int a) {
		this.a = a;
	}

	public Animal() {

	}

	public Animal(String name, String type, int a) {
		this.name = name;
		this.type = type;
		this.a = a;
	}

	public String getSound() {
		System.out.println("animal has a sound...."); // here only need to implement the method without creating new
														// class for child animal.
		return "animal has a sound....";
	}

	public String toString() {
		return "name :" + name + " type: " + type + "a :" + a;
	}

//	@Override
//	public int compareTo(Animal o) {
//		if(this.a>o.a)
//			return 1;
//		else if(this.a<o.a)
//			return -1;
//		else
//			return 0;
//	}
}

// same goes for interfaces 
interface Shape {

	public void draw();

}

@FunctionalInterface
interface Universe {

	public String createUniverse(int numberOfStars, int numberOfPlanets);

}

@FunctionalInterface
interface Bank {

	public String getInterest(String user);

}

public class LambdaAndAnonymousClass {
	public static void main(String[] args) {
		byte bbb = (byte) 129;
		System.out.println(bbb);
		// just implement first using anonymous inner class animal to getsound() method.
		Animal animal = new Animal() { // how to read get sound method is implemented in anonymous class and call
										// return the animal type of object.
			public String getSound() {
				System.out.println("wof wof .... this is dog.");
				return "wof wof .... this is dog.";
			}
		};
		animal.getSound(); // this will call the anonymous class methods.
		Shape shape = new Shape() {
			public void draw() {

				System.out.println("draw circle");
			}
		};
		shape.draw();
		// galaxy class implementation.
		Universe uv = (int noOfStars, int noOfPlanets) -> {
			return "Milky Way";
		};
		Universe uv1 = (noOfStars, noOfPlanets) -> {
			return "Milky Way";
		};
		System.out.println(uv.createUniverse(23, 34));
		// above examples are with the annonymous class

		// Lambda start from here

		Shape s = // this below part is the implementation of the method without having new class.
					// return type is Shape type.which is the parent.
				() -> {
					System.out.println("circle is drawn by lambda...."); // instead of creating the child class for
																			// implementation of parent method just
																			// write the lambda
					// right side is the child class with method implementations.
				};
		s.draw();

		// also we can pass the implementation to the method as a parameters.
		acceptShape(s);

		// with parameters
		Bank b = new Bank() {
			@Override
			public String getInterest(String user) {
				System.out.println("The interest for User " + user + "is 3%"); // Code inside the curly braces after new
																				// interface is actually a anonymous
																				// class.
				return user;

			}
		};
		b.getInterest("mark");

		Bank bank = (d) -> { // this is method implementation.
			System.out.println("The interest rate for user " + d + "is 3%");
			return d;
		};
		bank.getInterest("phil");
	}

	public static void acceptShape(Shape s) {
		s.draw();
		List<Integer> list = Arrays.asList(20, 30, 40, 50);
		list.forEach(new Consumer<Integer>() {

			@Override
			public void accept(Integer t) {
				System.out.println(t);
				list.add(34); // We can also change the value of the list and also perform some operation on
								// the list element if is the user defined object.

			}
		});
		Bank bank = new Bank() {
			@Override
			public String getInterest(String user) {

				return null;
			}
		};
		list.forEach(n -> {
			System.out.println(n);
		});// for each using the consumer implementation using lambda.

		final Map<String, Integer> order = new HashMap<>();
		order.put("dfs", 1);
		order.put("dfds", 2);
		List<Animal> animalList = Arrays.asList(new Animal("dfs", "dog", 5), new Animal("dfds", "cat", 2));
//		Collections.sort(animalList);

		/*
		 * So comparing method accepting the function where giving the object of one and
		 * return the integer and then this int is returning which is related to the
		 * animal object like if object is a and int return for this object is 5 so in
		 * the sorting method it sort the object based on what number this object belong
		 * so if 5: after sort object goes to number 5.and like wise all object will
		 * sorted out.
		 */
		animalList.sort(Comparator.comparing((Animal a) -> a.getA()));
		animalList.sort((a, b) -> {
			return a.getA() > b.getA() ? 1 : -1;
		});
		/*
		 * Comparator can sort the object based on the id, name both but in case of
		 * comparable it only either id or name.
		 */
		System.out.println(animalList);
		animalList.forEach(n -> {
			n.name = "ddfde";
			n.type = "mouse";
		});
		System.out.println(animalList);
	}
}
