package com.demo.demoHub.Utility;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class FilePracticeExamples {
	public static void main(String[] args) throws IOException {
		String filePath = "C:\\Users\\mange\\OneDrive\\Desktop\\New folder\\practice_file.txt";
		String path = "C:\\Users\\mange\\OneDrive\\Desktop\\Re__\\data_file.txt";
		
		File outFile = new File("C:\\Users\\mange\\OneDrive\\Desktop\\Re__\\data_fil1e.txt");
		
		File file = new File(filePath);
		File fileSrc = new File(path);
		File fileDest = new File("C:\\Users\\mange\\OneDrive\\Desktop\\Re__\\data_fil1e1.txt");
		FileInputStream in1 = new FileInputStream(path);
		FileInputStream in = new FileInputStream(file); // Open the file in the binary form and read the byte one by one
		int count;
		byte[] buffer = new byte[1024];
		while((count = in1.read(buffer)) != -1) {
			System.out.println((char)count);
		} 
		in1.close();
		// Write into byte array
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		byte[] buffer1 = new byte[1024];
		int cl;
		while((cl = in.read(buffer1)) != -1) {
			out.write(buffer1, 0, cl);
		}
		
		out.close();
		in.close();
		/*
		 * Here buffer read the stream from the input stream and stored it buffer array in buffered input stream
		 * so direct operations getting minimized (disk operation).
		 */
		FileInputStream input = new FileInputStream(fileSrc);
		BufferedInputStream stream  = new BufferedInputStream(input);
		BufferedOutputStream outStream = new BufferedOutputStream(new FileOutputStream(outFile));
		int a;
		byte[] buff = new byte[1024];
		while((a = stream.read(buff)) != -1) {
			outStream.write(buff,0,a);
		}
		outStream.close();
		stream.close();
		
		FileInputStream src = new FileInputStream(fileSrc);
		String str = "This is new form of byte array";
		byte[] bArr = str.getBytes();
		try (ByteArrayInputStream inStream = new ByteArrayInputStream(bArr);
			FileOutputStream outputStream = new FileOutputStream(fileDest);
			ByteArrayOutputStream oStream = new ByteArrayOutputStream()) {
			int cout;
			byte[] bf = new byte[1024];
			String s = null;
			while((cout = inStream.read(bf)) != -1) {
				outputStream.write(bf,0,cout);
				oStream.write(bf,0,cout);
				s = new String(oStream.toByteArray());
			}
			System.out.println(s);
		}
		
	}
}
 