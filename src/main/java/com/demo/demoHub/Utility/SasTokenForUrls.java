package com.demo.demoHub.Utility;

import java.time.OffsetDateTime;
import java.time.temporal.ChronoUnit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;

import com.azure.storage.blob.BlobClient;
import com.azure.storage.blob.BlobClientBuilder;
import com.azure.storage.blob.sas.BlobSasPermission;
import com.azure.storage.blob.sas.BlobServiceSasSignatureValues;

@Component
public class SasTokenForUrls {

	@Autowired
	BlobClientBuilder client;
	
//	https://sarcsdocumentsdev.blob.core.windows.net/rcs-ivmdatafiles/newfolder/Data Cleaning.pdf
	
	private static final String CONNECTION_STRING = "DefaultEndpointsProtocol=https;AccountName=sarcsdocumentsdev;AccountKey=zIYLFPpjyWYAqseZENQU7+rSO/kRXBXBIlPdW7Q0PBtBRHO96ujVVHRDClc5HTKxmArZxW+E/PiZ+AStUWwODg==;EndpointSuffix=core.windows.net";
//	10_RCSVN.csv 10_RCSVS.csv
	public void generateSasUrl() {
		String sasUrl = "";
		BlobClientBuilder client = new BlobClientBuilder();
		client.connectionString(CONNECTION_STRING);
		BlobClient blob = client.containerName("rcs-ivmdatafiles").blobName("newfolder/Data Cleaning.pdf").buildClient();
		blob.openInputStream();
		BlobSasPermission permission = new BlobSasPermission().setReadPermission(true);
        OffsetDateTime expiryTime = OffsetDateTime.now().plus(30, ChronoUnit.MINUTES); // Set the expiry time
        String sasToken = blob.generateSas(new BlobServiceSasSignatureValues(expiryTime, permission));
       
        //construct the URL with the SAS token
		sasUrl = blob.getBlobUrl() + "?" + sasToken;
		System.out.println(sasUrl);
	}

}
