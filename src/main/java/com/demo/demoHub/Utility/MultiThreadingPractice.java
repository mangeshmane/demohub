package com.demo.demoHub.Utility;

import java.util.Date;

import lombok.Getter;
import lombok.Setter;

class Triangle implements Shape {
	
	private Shape shape;
	
	
	public Shape getShape() {
		return shape;
	}

	public void setShape(Shape shape) {
		this.shape = shape;
	}

	public Triangle(Shape shape) {
		this.shape = shape;
		System.out.println(shape.getClass());
		shape.draw();
	}

	@Override
	public void draw() {
		System.out.println("draw called in shape....");
		
	}
	
}

@Getter
@Setter
class Threading extends Thread {
	 
	@Override
	public void run() {
		System.out.println(new Date());
		for (int i = 0; i < 43; i++) {
			System.out.println(i);
		}
		System.out.println(new Date());
	} 
}
class Threading1 implements Runnable {
	@Override
	public void run() {
		for(int i =0;i<10;i++) {
			System.out.println(i);
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		for(int i=0;i<10;i++) {
			System.out.println(i);
		}
		System.out.println(new Date());
	}
}
public class MultiThreadingPractice  {
	
	public static void main(String[] args) {
		
		Triangle shape = new Triangle(() -> {
			System.out.println("Draw method is called...");
			}
		);
		shape.draw();
	Threading t = new Threading();
	Runnable r = new Threading1();
	Thread t2 = new Thread(r);
	t2.start();
	t.start();
	Threading1 t13 = new Threading1();
	Thread t1 = new Thread(t13);
	t1.start();
	Thread t3 = new Thread(() -> {
		for(int i=0;i<10;i++) {
			System.out.println(i);
			try {
				Thread.sleep(1001);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
	});
	t3.start();
	}
}
