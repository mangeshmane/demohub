package com.demo.demoHub.Utility;

abstract class Brand {

	public abstract void showBrand();

	public Brand() {
		System.out.println(" This is the brand...");
	}

	public void makeBrands() {
		System.out.println("you can mkae brands here....");
	}

	public void doSomeResearch(Machine machine) {
		showBrand();
	}
}

class RedBull extends Brand {

	@Override
	public void showBrand() {
		System.out.println("And this is RedBull....");

	}

	public void makeBrandMoreFamous() {		
		System.out.println(this);
		doSomeResearch(new Laptop());//
		System.out.println("you can think more creativity...");
	}
	public <T> void makeSomeMachine(Machine machine) {
		Class<? extends Machine> t = machine.getClass();
		if(machine instanceof Laptop) {
			System.out.println("this is not right...");
		}
		//Laptop lap = (Laptop)t.cast(t);
		machine.getMachineInfo();
	}

}

class Sprite extends Brand {
	@Override
	public void showBrand() {
		System.out.println("This is sprite....");

	}
}

class Machine {
	String name;
	String type;
	int id;

	public void getMachineInfo() {
		System.out.println(name + " " + type + " " + id);
	}

	public void makeOffer() {
		System.out.println("we made a offer....");
	}

}

class Laptop extends Machine {
	int version;
	String gen;

	@Override
	public void getMachineInfo() {
		super.getMachineInfo();
		makeOffer();
		System.out.println(version + "" + gen);
	}
}

public class ParentChildPractice {

	public static void main(String[] args) {
		new RedBull().makeBrandMoreFamous();
		new RedBull().makeSomeMachine(new Machine());
	}
 	 
	
}
