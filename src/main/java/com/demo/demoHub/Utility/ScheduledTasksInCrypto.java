package com.demo.demoHub.Utility;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;

import com.demo.demoHub.dao.ICoinRepository;
import com.demo.demoHub.model.Coin;
import com.demo.demoHub.model.MarketPairDetailsData;

@Component
public class ScheduledTasksInCrypto {
	
	@Autowired
	WebClient webClient;
	
	@Autowired
	ICoinRepository coinRepository;
	
	
	public static final String BUBBLE_URL = "https://cryptobubbles.net/backend/data/bubbles1000.usd.json"; 
	public static double oneMin = 0.00;
	public static double fiveMin = 0.00;
	public static double fifteenMin = 0.00;
	
//	@Scheduled(cron = "0 * * * * *")
	public void getDataFromServer() {
		List<Coin> clientResponse = new ArrayList<>();
		AtomicInteger count = new AtomicInteger(0);
		try {
			webClient = WebClient.builder().baseUrl(BUBBLE_URL)
					.codecs(configurer -> configurer.defaultCodecs().maxInMemorySize(10 * 1024 * 1024)).build();

			clientResponse = webClient.get().retrieve().bodyToMono(new ParameterizedTypeReference<List<Coin>>() {
			}).block();
			if (null != clientResponse || !clientResponse.isEmpty()) {
				List<Coin> coins = clientResponse.parallelStream().map(coin -> {
					Integer innerCount = count.getAndIncrement();
					if (innerCount == 0) {
						coin.setId(null);
						coin.getSymbols().setId(null);
						coin.getPerformance().setId(null);
						coin.getRankDiffs().setId(null);
					}
					if (0 < innerCount) {
						if (coin.getPerformance().getMin1() > 5) {
							if (oneMin != coin.getPerformance().getMin1()) {
								System.out.println("Coin Name : " + coin.getName() +" " + coin.getSymbol() + " And price change in one minute: " + coin.getPerformance().getMin1());
							}
							
							oneMin = coin.getPerformance().getMin1();
						}
						if (coin.getPerformance().getMin5() > 8) {
							System.out.println("Coin Name : " + coin.getName() +" " + coin.getSymbol() + " And price change in five minute: " + coin.getPerformance().getMin5());
						}
						if (coin.getPerformance().getMin15() > 10) {
							System.out.println("Coin Name : " + coin.getName() +" " + coin.getSymbol() + " And price change in fifteen minute: " + coin.getPerformance().getMin15());
						}
						if (coin.getPerformance().getMin1() > 20) {
							System.out.println("Coin Name : " + coin.getName() +" " + coin.getSymbol() + " And price change in one minute with greater than twenty percent change : " + coin.getPerformance().getMin1());
						}
					}
//					coinRepository.save(coin);
					return coin;
				}).collect(Collectors.toList());
			}
			
//			CoinDCXWebSocketClient.main(null);
			throw new RuntimeException();
			 
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
		
	}
}
