package com.demo.demoHub.Utility;

import java.util.Scanner;

public class CodingPractice {

	public static void main(String[] args) {
		
		// 16 code : pattern with *_*_
		int iso = 10;
		for(int s = 0; s<5; s++) {
			for(int j = 1; j<iso-1; j++) {
				System.out.print("_ ");
				if(j == iso/2 -1 && s == 0) {
					System.out.print("* ");
				}
				
			}
			System.out.println("");
		}
		
		// 15 code : pattern start from 1 to 15 
		int coun = 1;
		for(int r = 1; r<=5; r++) {
			for(int j = 0; j<r; j++) {
				System.out.print(coun);
				coun++;
			}
			System.out.println("");
		}
		
		//14 code : print star pattern with numbers
		for(int i = 1;i<=5;i++) {
			for(int j = 1; j<=i; j++) {
				System.out.print(j);
			}
			System.out.println("");
		}
		
		//13 code : menu driven program to find prime and even odd and odd number from 1 to 100
		boolean switche = true;
		while(switche) {
			System.out.println("1.prime numbers");
			System.out.println("2.even numbers");
			System.out.println("3.odd numbers");
			
			Scanner menu = new Scanner(System.in);
			System.out.println("Please give your input...");
			switch(menu.nextInt()) {
			case 1:
				for(int i = 2;i<=100;i++) {
					boolean chek = true;
					for(int j = 2;j<i;j++) {
						if(i%j == 0) {
							chek = false;
							break;
						}
					}
					if(chek) {
						System.out.print(i );
					}
				}
				break;
			case 2:
				for(int i = 0;i<=100;i++) {
					if(i%2 == 0) {
						System.out.println(i + " even");
					}
				}
				break;
			case 3:
				for(int i = 0;i<=100;i++) {
					if(i%2 == 0) {
						System.out.println(i + " even");
					}
				}
				break;
				
				default:
					System.out.println("Please enter the correct input....");
					switche = false;
					
					
			}
		}
		
		
		
		
		// 12 code : start from 101 to 699 print number end with number 7
		
		for(int i = 101;i<700;i++) {
			if(i%10 == 7) {
				System.out.print(i + " ");
				
			}
		}
		// 11 code : palindrome code:
		
		int real = 12321;
		int pal = real;
		int rem = 0,sum = 0;
		while(pal>0) {
			rem = pal%10;  // gives a remainder
			sum = sum*10+rem; // logic is: calculate sum of remainer with multiple of older sum into 10
			pal = pal / 10;
		}
		if(real == sum) {
			System.out.println("the pal is the palindrome number.");
		} else {
			System.out.println("not a palindrome.");
		}

		// 10 code : write a number of digit in the number.
		int digit = 34343;
		int countDigit = 0;

		while (digit != 0) {
			digit = digit / 10;
			countDigit++;
		}
		System.out.println(countDigit);

		// 9 code : code for factorial of the number.:

		int factNum = 5;
		int factTotal = 1;
		int fact = 1;
		for (int i = 1; i <= factNum; i++) {
			factTotal = factTotal * i;
		}
		System.out.println(factTotal);
		factTotal = 1;
		// or this you can use
		while (fact <= factNum) {
			factTotal = factTotal * fact;
			fact++;
		}
		System.out.println(factTotal);

		// 8 code : nextcode is TODO:Write a program to find if given number is prime or
		// not
		// prime number is the number is divisible by it's own and its greater than 1

//		here the logic just get value which are less or equal to current value and then check if go from 0 to that number less than its own.
		for (int i = 2; i <= 100; i++) {
			int k = 2;
			boolean checkPrime = true;
			while (i > k) {
				if (i % k == 0) {
					checkPrime = false;
					break;
				}
				k++;
			}
			if (checkPrime) {
				System.out.print(i + " ");
			}

		}

		int av = 20;
		int counterNum = 0;
		for (int i = 2; i < 100; i++) {
			boolean counter = true;
			for (int j = 2; j < i; j++) {
				if (i % j == 0) {
					counter = false;
					break;
				}
			}
			if (counter) {
				System.out.println(i + " is prime");
				counterNum++;
			} else {
//				System.out.println(i + " is not prime");
			}

		}
		System.out.println(counterNum);

		// 7 code : print pattern for reverse right angle triangle.

		for (int i = 5; i > 0; i--) {
			for (int j = 0; j < i; j++) {
				System.out.print(j + 1);
				System.out.print("*");
			}
			System.out.println();
		} // we can write another way of for loop.
		for (int i = 0; i < 5; i++) {
			for (int j = 5; j > i; j--) {
				System.out.print(5 - (j - 1));
				System.out.print("*");
			}
			System.out.println();
		}
		System.out.println();

		for (int i = 0; i < 5; i++) {
			for (int j = 0; j <= i; j++) {
				System.out.print(j + 1);
				System.out.print("*");
			}
			System.out.println();
		}
		for (int i = 5; i > 0; i--) {
			for (int j = 5; j >= i; j--) {
				System.out.print(5 - (j - 1));
				System.out.print("*");

			}
			System.out.println();
		}

		System.out.println();

		// 1 code: Print following message on console
//		1)Hef ” Shine
//		2) “HefShine”
		System.out.println("Hef " + '"' + "Shine");
		System.out.println('"' + "HefShine" + '"');

		// 2 code : Print a message on two lines using only one sys out statement
//		Output: Hef
//		 Shine

		System.out.println("Hef\nShine");// or try below one
		System.out.printf("%s %n%s", "Hef", "Shine\n");
		int a = 20, b = 30;
		if (a > b)
			System.out.println("a is greater than b.");
		else
			System.out.println("b is greater than a.");

		// 4 code :Write a program to input an alphabet and display whether it is a
		// vowel or a consonant.
		Scanner sc = new Scanner(System.in);
		char inputChar = sc.next().charAt(0);
		if (inputChar == 'a' || inputChar == 'e' || inputChar == 'i' || inputChar == 'o' || inputChar == 'u') {
			System.out.println(inputChar + "  character is a vovel ");
		}

		// 5 code : display if a character is an alphabet, number or special character.
		if ((inputChar >= 65 && inputChar <= 90) || (inputChar >= 97 && inputChar <= 122)) {
			System.out.println("Given input is alphabet.");
		}
		if (inputChar >= 48 && inputChar <= 57) {
			System.out.println("Given input is digit.");
		} else {
			System.out.println("Given input is special char.");
		}

		// 6 code : Write a program to input marks of five subjects by the user. Now
		// calculatesum and percentage of the marks.
		// Find the grade according to following: (Variations)
		int subJectMarks = 0;
		int count = 0;
		Scanner mark = new Scanner(System.in);
		for (int i = 0; i < 5; i++) {
			subJectMarks += mark.nextInt();
			count++;
		}
		float average = subJectMarks / count;
		double percentage = ((subJectMarks * 100) / 500);
		System.out.println("average = " + average + " and percentage = " + percentage);

	}

}
