package com.demo.demoHub.Utility;

import java.time.LocalTime;
import java.util.concurrent.CompletableFuture;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

//
@Setter
@Getter
@NoArgsConstructor
class Browser extends Thread {
	private Integer threadId; 
	
	private String names;
	
	@Override
	public void run() {
		System.out.println(LocalTime.now());
		for (int i = 0; i < 43; i++) {
			System.out.println(i + " This is done using the thread class...."+"\n \n");
		}
		System.out.println(LocalTime.now() + "\n \n" );
	} 
	public Browser(Runnable runnable) {
		super(runnable);
	} 
}

@Setter
@Getter
@NoArgsConstructor
class HumanBody implements Runnable {
	private Integer threadId;
	
	
	@Override
	public void run() {  // what we want to run using thread add it in run method.
		System.out.println(LocalTime.now());
		for (int i = 0; i < 43; i++) {
			System.out.println(i + "This is using the runnable interface run method...\"\\n \\n\"");
		}
		System.out.println(LocalTime.now() );
	}

}

//What is an example of Multithreading?
//Multithreading enables us to run multiple threads concurrently. 	
public class MultithreadingPracticeNew {
	static void printValue(String val) throws InterruptedException {
		System.out.println(Thread.currentThread().getName() + "in print value");
		Thread.sleep(10000l);
		System.out.println(val);
	}
	// Runnable only the blueprint for the thread we have to pass the instance of runnable to thread then it
	// call the thread method.
	public static void main(String[] args) throws InterruptedException {
		System.out.println(Thread.currentThread().getName());
		CompletableFuture<Void> future = CompletableFuture.
				runAsync(() -> {
					try {
						printValue("This is value : " + 100);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				})
				.thenRun(() -> System.out.println(Thread.currentThread().getName()))
				.thenRun(() -> System.out.println("after sleep"));
		
		
		Thread.sleep(10000l);
		System.out.println("after sleep");
		Thread th = Thread.currentThread();
		System.out.println(th.getName());
		Browser browser = new Browser(() -> System.out.println("this is used in thread"));
//		Thread thread = new Thread(() -> System.out.println("this is used in thread1"));
		Thread thread = new Thread(new HumanBody()); 
		thread.start();
		thread.sleep(1000);
		System.out.println("slept for one second...");
//		thread.wait(10000);
		System.out.println("waiting for ten seconds...");
		System.out.println("\n \n");
		browser.start();
	}
}



