package com.demo.demoHub.Utility;

//If the class is final then we can't extends it to its child classess.

final class Android {
	public void finalClass() {
		System.out.println("can't make any child or extends this class");
	}
}
class Animals {
	
	public void getSound() {
		System.out.println("kdj");
	}
	
	//if method is final can't override the method in child class (basically like abstract class method or interface methods.
	public final void eat() {
		System.out.println("iejd");
	}
}

class Cat extends Animals {
	
	public static final String ANIMAL = "cat";
	public static int a;

	public void getSound() {
		System.out.println("meow");
		
	}
	//giving error because of the 
//	public void eat() {
//		
//	}
}

public class FinalKeyworkPractice {
	
	//final variable
	public static final Integer CODE = 358; //constant creation using this syntax. fix value like pi can't changes.
	
	
	public static void main(String[] args) {
		int a = CODE;
		System.out.println(a);
		int c = 0;
		System.out.println(c);
		Cat animal = new Cat();
		String f = Cat.ANIMAL;
		Cat.a = 89;
		
		
		
	}

}
