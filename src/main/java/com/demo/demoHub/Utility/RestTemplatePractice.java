package com.demo.demoHub.Utility;

import java.io.File;
import java.io.FileInputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

public class RestTemplatePractice {
	
	public static void main(String[] args) {
		int x =5 ; int y =7;
				int z = x++ + ++y - --x + y-- ;
		Gson gson = new Gson();
		String json = "{\r\n"
				+ "    \"parameter_value_data\":[\r\n"
				+ "    {    \r\n"
				+ "    \"parameter_id\": 1 ,\r\n"
				+ "    \"param_value_list\":[\r\n"
				+ "        {\r\n"
				+ "            \"id\":50,\r\n"
				+ "            \"is_valid\":true,\r\n"
				+ "            \"value\":\"param133\",\r\n"
				+ "            \"is_deleted\":false\r\n"
				+ "\r\n"
				+ "        },\r\n"
				+ "        {\r\n"
				+ "            \"id\":49,\r\n"
				+ "            \"is_valid\":true,\r\n"
				+ "            \"value\":\"param7\",\r\n"
				+ "            \"is_deleted\":true\r\n"
				+ "        }\r\n"
				+ "    ]\r\n"
				+ "},\r\n"
				+ "{   \r\n"
				+ "    \"parameter_id\": 1 ,\r\n"
				+ "    \"param_value_list\":[\r\n"
				+ "        {\r\n"
				+ "            \"id\":1,\r\n"
				+ "            \"is_valid\":true,\r\n"
				+ "            \"value\":\"param50\",\r\n"
				+ "            \"is_deleted\":false\r\n"
				+ "\r\n"
				+ "        },\r\n"
				+ "        {\r\n"
				+ "            \"is_valid\":true,\r\n"
				+ "            \"value\":\"param98\",\r\n"
				+ "            \"is_deleted\":false\r\n"
				+ "        }\r\n"
				+ "    ]\r\n"
				+ "    }\r\n"
				+ "    ]\r\n"
				+ "}";
		Map<String,Object> json2 = gson.fromJson(json,Map.class);
		Object json3 = gson.fromJson(json,Object.class);
		json2 = (Map<String, Object>)json3;
		
		json2.forEach((k,v) -> {
			System.out.println("This is k: "+ k);
			List<Object> obje = (List<Object>)v;
			obje.stream().forEach((obj) -> {
				JsonObject jsonObje = gson.fromJson(gson.toJson(obj),JsonObject.class);
				long parameterId = jsonObje.get("parameter_id").getAsLong();
				JsonArray paramArray = jsonObje.getAsJsonArray("param_value_list");
				paramArray.forEach((key) -> {
					JsonObject paramObject = key.getAsJsonObject();
					System.out.println(paramObject);
				});
			});
		});
		
		json2.forEach((k,v) -> {
			System.out.println("For Value V key is :" + k);
			List<Object> value = (List<Object>)v;
			value.forEach(list -> {
				System.out.println(list);
				Map<String,Object> map = (Map<String,Object>) list;
				System.out.println(map);
				Double parameterId = (Double)map.get("parameter_id");
				List<Object> listOfMap = (List<Object>) map.get("param_value_list");
				listOfMap.forEach((mapList) -> {
					Map<String,Object> lastValues = (Map<String, Object>) mapList;
					lastValues.forEach((keys,values) -> {
						System.out.println("Value of K:"+keys+" and Value for k is v: "+values);
					});
				});
				
			});
			
		});
		
//		HttpCallMethods.createUser();
		
	}
}

class HttpCallMethods {
	private static final String URL = "https://gorest.co.in/public/v2/users";
	private static final String token = "3286986999041cd705df8a7c10329c630d0e4cd58fd41e84998264085846d518";
	
	private static final Logger logger = LoggerFactory.getLogger(HttpCallMethods.class);
	
	public static void createUser() {
		try {
			RestTemplate client = new RestTemplate();
			Users user = new Users(0l,"user",UUID.randomUUID().toString()+"@gmail.com","male","inactive");
			Users user1 = new Users(0l,"user",UUID.randomUUID().toString()+"@gmail.com","male","inactive");
			// For body and headers we use Http headers and multivalued map to send it with httpEntity.
			HttpHeaders httpHeader = new HttpHeaders();
			httpHeader.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
			httpHeader.set("Authorization", "Bearer "+token);
			
			HttpEntity<Users> entity = new HttpEntity<Users>(user, httpHeader);
			HttpEntity<Users> entity1 = new HttpEntity<Users>(user1, httpHeader);
			Object objectResponse = client.postForObject(new URI(URL), entity, Object.class);
			String stringResponse = client.postForObject(new URI(URL), entity1, String.class);
			System.out.println(objectResponse);
			System.out.println(stringResponse);
			Map<String, Object> map = (Map<String, Object>) objectResponse;
			ObjectMapper mapper = new ObjectMapper();
			Gson gson = new Gson();
			String jsonString = gson.toJson(objectResponse);
			String testJson = gson.toJson(jsonString);
			JsonObject json = gson.fromJson(jsonString,JsonObject.class);
			Long id = json.get("id").getAsLong();
			
			
			HttpHeaders headers = new HttpHeaders();
			headers.add("Content-Type",MediaType.APPLICATION_JSON_VALUE);
			
//			HttpEntity<Integer> httpEntity = new HttpEntity<>(headers);
//			ResponseEntity<String> entities = client.getForEntity(new URI(URL+"/64dc97f2448b8e03e877e1fc"), String.class);
			
		} catch (Exception e) {
			logger.error("exception occurred for following reason -> "+e.getMessage());
		}
		
	}
}

@Getter
@Setter
@ToString
class Unicorn {
	String _id;
	String name;
	String colour;
	Integer age;

}

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
class Users {
	private long id;
	private String name;
	private String email;
	private String gender;
	private String status;
	
	// Reflection example.
	public static void forName() throws ClassNotFoundException, InstantiationException, IllegalAccessException {
		Class cls = Class.forName("com.demo.demoHub.Utility.Users");
		cls.getFields();
		cls.getMethods();
		Users user = (Users) cls.newInstance();
	}

}
