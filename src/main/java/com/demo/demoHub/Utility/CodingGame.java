package com.demo.demoHub.Utility;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
public class CodingGame {
	String x;
	boolean check = false;

	public synchronized void printPunch() throws InterruptedException {
		String x = "Punch on face...";
		if (check) {
			wait(1000);
		}
		System.out.println(x);
		check = false;
		notify();

	}

	public synchronized void printKick() throws InterruptedException {
		String x = "Kick on face...";
		if (!check) {
			wait(1000);
		}
		check = true;
		System.out.println(x);
		notify();
	}

	// Created threads are run by jvm and scheduler algorithm which thread run first
	// and likewise you can
	public static void main(String[] args) {
		CodingGame game = new CodingGame();
		Thread punch = new Punch(game); // These are single task single thread. like kick and punch. and end.
		Thread kick = new Kick(game);
		Thread kick2 = new Kick(game); // At the time when we create the object of kick again means for single task
										// multiple threads.
		punch.start();
		kick.start();
		kick.interrupt();
	}
}

/*
 * So why we create the threads: It is the thread which has the run method it
 * means a one task to complete. one thread one task in the run method in which
 * we can create multiple object from it and do the same task concurrently. If
 * want to run multiple task then create multiple threads means create more run
 * methods. and done the single process with multiple tasks simultaneously. so
 * let's begin...
 */

@NoArgsConstructor
@AllArgsConstructor
class Punch extends Thread {
	CodingGame game;

	@Override
	public void run() {
		try {
			for (int i = 0; i < 10; i++) {
				game.printPunch();
				int x = 0;
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

}

@NoArgsConstructor
@AllArgsConstructor
class Kick extends Thread {
	CodingGame game;

	@Override
	public void run() {
		try {
			for (int i = 0; i < 10; i++) {
				game.printKick();
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

}

@NoArgsConstructor
class Inherit extends Animal {
	private int a;

	public void printA() {
		System.out.println(a);
	}

	public int printA(int a) {
		return a;
	}

	public double printA(double a) {
		return a;
	}

	public int getSuperA() {
		return super.a;
	}

	Inherit(int a) {
		super.a = a;
	}

	public static void main(String[] args) throws InterruptedException {
		Inherit in = new Inherit(33);
		int aa = in.getSuperA();
		Animal as = new Inherit();
		Animal ani = new Animal();
		int an = as.a;
		Animal animal = in;
		Inherit inh = (Inherit) as;
		String name2 = in.name;

		Thread.currentThread().setName("Its javaces");
		// Thread example
		Audio t1 = new Audio();
		ProgressBar t2 = new ProgressBar();
		t1.start();
		t1.setName("AudioThread");
		t2.start();
		System.out.println("wait to finish the join method First and then run...");
		t2.join();

		// Thread Daemon
		DaemonThread daemon = new DaemonThread();
		daemon.setDaemon(true);
		daemon.start();

		// Join method example
		JoinMethod join = new JoinMethod();
		join.start();
		System.out.println("wait to for main method...");
		join.join();
		System.out.println("Main Thread worked before the join thread....");

		// Is used to interrupt a current executing thread but thread must be in sleep()
		// or wait() stage other wise work normally.
		// we call the t.interrupt() it interrupt the thread and going in the catch
		// block for interrupting the thread.

	}
}

// These are music player threads.(so thread means Task to perform simultaneously.)
class Audio extends Thread {
	@Override
	public void run() {
		for (int i = 0; i < 20; i++) {
			try {
				Thread.sleep(0); // give some sleep to have simultaneous work.
			} catch (InterruptedException e) {
			}
			System.out.println("This is for Audio");
		}
	}
}

class ProgressBar extends Thread {
	@Override
	public void run() {
		for (int i = 0; i < 20; i++) {
			try {
				Thread.sleep(0); // give some sleep to have simultaneous work.
			} catch (InterruptedException e) {
			}
			System.out.println("This is for Progress");
		}

	}
}

class DaemonThread extends Thread {
	@Override
	public void run() {

		try {
			Thread.sleep(0); // give some sleep to have simultaneous work.
		} catch (InterruptedException e) {

		}
		System.out.println("This is Daemon");
	}
}

// Here we want main thread stop so and joinThread.join() so join can run main thread will work after that.
// one is dependent on other to complete and then it run.
class JoinMethod extends Thread {
	@Override
	public void run() {

		try {
			Thread.sleep(0); // give some sleep to have simultaneous work.
		} catch (InterruptedException e) {

		}
		System.out.println("This is Daemon");
	}
}
// child refered by parent Parent p = new Child();
// Upcasting : parent says hey child you access all things that i have but i can access my things also.
// you know it's just the behavior like access car variable that parent have and
// implementation
// or behavior method is to drive the car but child is driving by it's own way.

// DownCasting : we can't do this directly : Child ch = (child) new Parent(); not the correct way to cast;
/*
 * so what can we do is refer the parent to child and then that child will
 * downcast like below: Animal as = new Inherit(); -> Inherit inh = (Inherit)as;
 * So in down-casting parents wants to access the child things but he can't in
 * this case parent cannot become the child again to referenced by the child
 * reference variable parent is not the child's type like child is the parent
 * type.
 */

/*
 * Every object has two area these are synch area and non-synch area when the
 * code is try to access the synch method/block then this thread acquires the
 * lock so other thread can't access the method or block so this is two areas.
 * class also these areas for access where we can use the static
 * synchronization.
 */
class SynchroPractice extends Thread {
	int ticket1;
	MovieTicket ticket;

	public SynchroPractice(MovieTicket ticket, int tickets) {
		this.ticket1 = tickets;
		this.ticket = ticket;
	}

	@Override
	public void run() {

//		ticket.bookMovieTicketWithSynchMethod(ticket1);
//		ticket.bookMovieTicketWithSynchBlock(ticket1);
		ticket.bookMovieTicketWithStaticSynch(ticket1);  
	}

	public static void main(String[] args) {
		int i = 20, j = 20;
		int k = i++ + ++j + i++ + j++ + j--;
		System.out.println(k);
		MovieTicket ticket = new MovieTicket();
		SynchroPractice t1 = new SynchroPractice(ticket, 2);
		t1.setName("Thread-1");
		t1.start();
		SynchroPractice t2 = new SynchroPractice(ticket, 6);
		t2.setName("Thread-2");
		t2.start();

		// this object is for practice of static synchronization with multiple object
		// with there threads.
		MovieTicket ticket2 = new MovieTicket();
		SynchroPractice t3 = new SynchroPractice(ticket2, 8);
		t3.setName("Thread-3");
		t3.start();
		SynchroPractice t4 = new SynchroPractice(ticket2, 2);
		t4.setName("Thread-4");
		t4.start();

	}
}

class MovieTicket {

	int remainingTickets = 10;

	// Synchronized method is used to when data inconsistency problem occur so if
	// multiple threads try to access
	// the same method/same resource at a time and then data could be wrong because
	// two thread accessing values of
	// variable same time like one ticket available and two people try to access
	// then it will get problem so use this.
	
	synchronized void bookMovieTicketWithSynchMethod(int numberOfTicket) {
		System.out.println(Thread.currentThread().getName());
		if (remainingTickets > 0) {
			System.out.println("before booking " + remainingTickets);
			if (numberOfTicket < remainingTickets) {
				remainingTickets -= numberOfTicket;
			} else {
				System.out.println("please select only " + remainingTickets + " tickets.");
			}

			System.out.println("after booking: " + remainingTickets);

			System.out.println("Booked successfully...");
		} else {
			System.out.println("tickets not available...");
		}

		System.out.println("remaining after end of booking: " + remainingTickets);
	}

	/*
	 * suppose there are thousand lines of code in the method and it is synchronized
	 * method then there is time increases to complete the task for one thread then
	 * process can get slow. solution for that is use synchronized block for the
	 * lines of code which are important rather than whole method using this only
	 * block of lines are accessed by thread one by one but other lines are accessed
	 * by multiple thread single time so use synchronized block like below.
	 */
	void bookMovieTicketWithSynchBlock(int numberOfTicket) {
		System.out.println(Thread.currentThread().getName());
		synchronized (this) {
			if (remainingTickets > 0) {
				System.out.println("before booking " + remainingTickets);
				if (numberOfTicket < remainingTickets) {
					remainingTickets -= numberOfTicket;
				} else {
					System.out.println("please select only " + remainingTickets + " tickets.");
				}

				System.out.println("after booking: " + remainingTickets);

				System.out.println("Booked successfully...");
			} else {
				System.out.println("tickets not available...");
			}
		}

		System.out.println("remaining after end of booking: " + remainingTickets);
	}

	/*
	 * Till now we are working with single object and multiple threads and when we
	 * are trying to access the multiple thread for multiple objects then things
	 * working again differently. now at a same time multiple threads working
	 * concurrently at same time with different objects that is why value is getting
	 * different each object has separate lock when in synchronized manner. But in
	 * case of multiple object it with multi-thread and sharing the same resources
	 * then it will not working good with synchronization also so just use the class
	 * level synchronization(static synchronization) it is a class level where
	 * single thread acquire the thread lock. and object has its own lock so race
	 * condition will not get occur but it is in case threads are accessing the same resource.
	 */

	  static synchronized void bookMovieTicketWithStaticSynch(int numberOfTicket) {
		System.out.println("Thread name : " + Thread.currentThread().getName());
		MovieTicket ticket = new MovieTicket();
		int remainingTickets = ticket.remainingTickets;
		System.out.println("before even thread start: " + remainingTickets);
		if (remainingTickets > 0) {
			System.out.println("before booking " + remainingTickets);
			if (numberOfTicket < remainingTickets) {
				remainingTickets -= numberOfTicket;
			} else {
				System.out.println("please select only " + remainingTickets + " tickets.");
			}

			System.out.println("after booking: " + remainingTickets);

			System.out.println("Booked successfully...");
		} else {
			System.out.println("tickets not available...");
		}

		System.out.println("remaining after end of booking: " + remainingTickets);
	}
	  
}

class WaitAndNotify extends Thread {
	
	int number;
	
	@Override
	public void run() {
		synchronized (this) {
			for (int i = 0; i < 10; i++) {
				number += i;
			}
			this.notify(); // i am done with mark so other can now work and release this object lock of synch.
		}
	}
	
	public static void main(String[] args) throws InterruptedException {
		WaitAndNotify obj = new WaitAndNotify();
		obj.start();
		System.out.println("this is number: "+obj.number);
		synchronized (obj) {
			obj.wait(); // we can only call the wait in synch method or block.
		}
//		Thread.sleep(1000);   if we want to do inter thread communication then use the wait() and notify(); before accessing the number.
		System.out.println(obj.number);
	}
}
class ThisAndSuper {
	
	private int id;
	private String name;
	private int rollNumber;
	
	public ThisAndSuper() {
		this(23,"ndf",20);
	}
// this refer to the current object variable so when A a = new A(); like a is point to the A object in memory like at same time 
	// this is also pointing the reference variable. all the method and variable of the currently created or newly created object 
	// will point to them. This is only used inside of the methods not outside of the methods.
	public ThisAndSuper(int id, String name, int rollNumber) {
		super();
		this.id = id;
		this.name = name;
		this.rollNumber = rollNumber;
	}
	public int getId() {
		return this.id;
	}
	
}
