package com.demo.demoHub.Utility;

import java.util.Collections;

import org.web3j.protocol.Web3j;

import com.demo.demoHub.Utility.StaticOuter.StaticInner;
import com.demo.demoHub.crypto.entity.Web3jEther;

public class AnnonymusClassTest {
	
	public static void main(String[] args) {
		int s = 10;
		short a = 20;
		a = -12;
//		s = s*a;
		a = (short) (s*a);
		double dd = 20.223;
		
		String ss = "";
		while (ss.length() != 2) {
			ss+= "a";
		}
		System.out.println(ss);
		
		int aa = 0;
		for(int i = 0;i<15;i++) {
			aa = aa + (int) Math.pow(2,i);
		}
		System.out.println(aa);
		System.out.println(a*s);
		Object o = 10;
		if (o instanceof Integer) {
			System.out.println(o);
		}
		Integer ii = (Integer)o;
		System.out.println(o);
		Annonymus intern = new Annonymus() {
			@Override
			public Integer getInt(Integer value) {
				return value * value;
			}
		};
		getAnnonymus(intern); // Or
		getAnnonymus(value -> value * value ); // Lambda
		
//		Collections.sort(null);
		primitivePractice();
	}
	
	public static Integer getAnnonymus(Annonymus inter) {
		return inter.getInt(30);
		
	}
	public static void primitivePractice () {
		byte t = 12;
		short sh = 23;
		char c = 'a';
		int i = 12;
		float f = 10.33f;
		long l = 3443234234341342l;
		double d = 23920390.93430;
		String s = "39343";
		
		// This is Boxing conversion of primitive to wrapper(Reference Type) class type.
		Byte by = Byte.valueOf(t);
		System.out.println(by);
		Short ssh = Short.valueOf(sh);
		Character chr = Character.valueOf(c);
		Integer in = Integer.valueOf(i);
		Double converted = 403d;
		in.intValue();
		Long lon = in.longValue();
		int integer = in;
		// Like wise Unboxing means conversion of reference type to primitive. it is implicit (automatic conversion like child to parent.).
		
		i = (int) l; // Loss due to big datatype conversion to small type.
		in = 239; 
		i = in; // This is auto-boxing.
		
		// Static Inner Class
		StaticOuter outer = new StaticOuter();
		outer.outer = 20;
		StaticOuter.outerStatic = 20;
		StaticOuter.StaticInner staticInner = new StaticInner();
		StaticOuter.StaticInner inner = new StaticOuter.StaticInner();
		inner.staticInnerMethod();
		StaticOuter.StaticInner.b = "most beautiful thing is god created this whole world.";
		
		new StaticOuter().new NonStaticIner();
		new StaticOuter.StaticInner();
		outer.new NonStaticIner();
//		new NonStaticInner(); // When reference is StaticOuter.StaticInner like this.
		StaticOuter.StaticInner.b = "at the perfect place";	
		StaticOuter.NonStaticIner nonStatic = outer.new NonStaticIner();
		nonStatic.nonStaticInnerMethod();
		
		// Testing when the static parameter is a object 
		Web3j web3 = outer.web3;
		System.out.println(web3);
		
		// Send the annonymous object in the form of lambda function.
		acceptAnnonymous(a -> a*a);
	}

	// Annonymous class method where we are sending the annonymous class as a parameter.
	public static void acceptAnnonymous(Annonymus an) {
		System.out.println(an.getInt(10));
	}
}

interface Annonymus {
	int a = 10;
	Integer getInt(Integer value);
}

class StaticOuter {
	int outer;
	static int outerStatic;
	static final Web3j web3 = new Web3jEther().getWeb3j();
	
	
	
	@Override
	public String toString() {
		return "StaticOuter [outer=" + outer + "]";
	}
	static class StaticInner {
		int a;
		static String b;
		
		public void staticInnerMethod() {
			StaticOuter outer = new StaticOuter();
			int c = outer.outer;
			System.out.println("This is the static inner class Static method...");
		}
	}
	class NonStaticIner {
		public void nonStaticInnerMethod() {
			
		}
	}
	
}

