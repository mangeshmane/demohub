package com.demo.demoHub.Utility;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

//Type parameter generics.

public class GenericsPractice<T> {
	 
//	Integer param;  //we can only set Integer parameters in the constructor.
	
//	public GenericsPractice(Integer param) {
//		this.param = param;
//	}
	
	T param;   //here T is supposed as Data Type of the parameter.
	
	public GenericsPractice(T param) {
		this.param = param;
	}
	
	public void show() {
		System.out.println(param);
	}
	
	//here instead of commented code use uncommented code to implements generics using type parameter like T.
	public static void main(String[] args) {
//		GenericsPractice p = new GenericsPractice(28);
//		p.show();
		String aa = null;
		String abc = new String("adf"+aa);
		System.out.println(abc);
		GenericsPractice<Integer> p = new GenericsPractice<Integer>(33); //here we can send any kind of data type in the parameter type using angurlar brackets.
																		//and value for constructor in braces.
		p.show();
		
		GenericsPractice<Double> d = new GenericsPractice<Double>(99.04); //sending double value.  
		d.show();
		
		//here we are directly sending the implementation with return type of Bank.Instead of writing
		// Bank b = below argument.
		getResult( (tzuyu) -> {
			System.out.println("hello bank user..."+ tzuyu);
			return "5%";
		});
	//	getResultOfExtendingObject(3); //below method is accepting any type data.like we are sending the 3 here it will accept.
		getResultOfExtendingObject(new Animal()); //
		
		List<Integer> list = Arrays.asList(1,3,4,52,2);
		
		wildCardPractice(new ArrayList<Animal>());
		wildCardPractice(list);
		
		//upcasting
		Sheep cast = new Sheep();
		Animalss a = new Animalss();
		a.type = "djkf";
		System.out.println(Animalss.type);
		Animalss casta = new Sheep();
		cast.makeNoise();// call methods depends on the type of reference variables.it call sheep noise.
		makeAnimal(cast);
		//downcast give compile time error not being performed implicitly.
		Sheep shep = (Sheep)casta;// so casta is animals type and casted to child sheep explicitly.
		
	}

	// here we need to add T before return type or void because it tell method that this is generics type of data.
	public static <T extends Bank> void getResult(T result) {  //bounded generics.
//		Bank b = (Bank) result; //here we are only accept extended or parent object so
		//we written the T extends Bank and only those object are accepting here.
		
		System.out.println(result.getInterest("tzuyu"));
	}
	public static <T> void getResultOfExtendingObject(T result) {
		Animal a = (Animal) result; //if we don't add write T extends Bank Then we need to cast this object and then call his method.
//		Animal b = result; //this also work when write extends Animal;
		a.getSound();
	}
	
	//here we can also use T type generics but we don't know the actual data type so use wildcard just.
	public static void wildCardPractice(List<?> result) {
		System.out.println(result);
	}
	
	public static void makeAnimal(Animalss animal) {
		//here we always send sheep object so it will work but what if we send another object
		Sheep sheep = (Sheep) animal;
		//but some time one can pass other object than sheep then it may raise the class cast exception.
		//use instanceof Operator for this issue
		if (animal instanceof Sheep) {
			Sheep shep = (Sheep) animal;
		}
		
	}
}
class Animalss {
	
	Integer id;
	String name;
	static String type;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public void makeNoise() {
		System.out.println("do make noise");
	}
	@Override
	public String toString() {
		return "Animalss [id=" + id + ", name=" + name + "]";
	}
	
}

class Sheep extends Animalss {
	
	String color;
	
	public void makeNoise() {
		System.out.println("meyyy...");
	}
	
}

class Dog extends Animalss {
	
	String color;
	
	public void makeNoise() {
		System.out.println("bhow bhow...");
	}
	
}























