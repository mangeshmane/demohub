package com.demo.demoHub.Utility;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;


class Test {
	String subject;
	int marks1;
	int marks2;
	
	public void runLambda() {
		Lambda runMethod = () -> System.out.println(marks1);
		runMethod.makeLambda();
	}
	public String toString() {
		return ""+subject +" "+ marks1;
	}
	
	public int getTotalMarks (DoAddition addMarks) {
		return addMarks.add(marks1, marks2);
	}
}
interface Lambda {
	public void makeLambda();
}

@FunctionalInterface
interface DoAddition {
	public int add(int first, int second);
}

interface Left extends Right {
	default void m1() {
		System.out.println("Left.m1()");
		Long sendMoney = sendMoney();
		
	}
	void me();
}
// same method name in both interfaces and so ambiguity will arrive if same class impl both interfaces
// so we can implements or write the method with same name in class or we write specify which interface method
// implementation we need here so we write like below.(for above compile time error will occur.
interface Right {
	default void m1() {
		System.out.println("Right.m1()");
	}
	public Long sendMoney();
}

class Testing2 implements Left {
	public void m1() {
		//m1(); // This will cause the stackoverflow issue due to recursive call.
		System.out.println("own implementation");
	}
	@Override
	public Long sendMoney() {
		// TODO Auto-generated method stub
		return null;
	}
@Override
public void me() {
//	super.m1();// This won't work because super works directly on class not on interfaces 
	m1();
	
}
}
class Testing implements Left, Right {
	public void m1() {
	  System.out.println("Test.m1()");
	  Left.super.m1(); // (call left interface method implementation).
	  	// interface.super.method() this patter is used only when we have a two interface with
		// same name of method and then for specifying which interface(parent ineterfae) method 
		// implementation you want. super is used to call the method of parent class method 
		// which is already implemented or overriden in subclass.
//	  Right.super.m1();  //(call right interface method implementation).
	 }
	@Override
	public Long sendMoney() {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public void me() {
		// TODO Auto-generated method stub
		
	}
}

abstract class NewAbsClass {
	private int id;
	private int a;
	private String b;
	
	public int getA() {
		return a;
	}
	
	NewAbsClass() {
		System.out.println("This is the parent abstract class custroctor");
	}
	
	public abstract <T extends NewAbsClass> int getId(Class<T> t,int a);
	
	public <T extends NewAbsClass> void doProcessing(Class<T> t,int a) {
		getId(t, a);
	}
}
class AbsChild extends NewAbsClass {
	
	int a;
	
	AbsChild(){
		System.out.println("This is child constructor");
	}
	
	public void callParent() {
		
	}
	
	public <T extends NewAbsClass> int getId(Class<T> t,int a) {
//		T obj = t.newInstance();
//		NewAbsClass tt = (NewAbsClass) obj;
		return 32;
	}
}

public class StreamApiPractice {

	public static void main(String[] args) throws InstantiationException, IllegalAccessException, ClientProtocolException, IOException {
		
		String value = "https://sarcsdocumentsprod.blob.core.windows.net/rcsprodcontainer1/parent/null/docs/prodmod/1697025058232_artus%20universalversicherung%20gebäude%202023.pdf";
		
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		// Use this to read input stream from url/resources instead of URL object.
		HttpClient httpClient = HttpClientBuilder.create().build();
		HttpGet httpGet = new HttpGet(value);

		HttpResponse response = httpClient.execute(httpGet);
		InputStream content = response.getEntity().getContent();
		File file = new File("C:\\Users\\mange\\OneDrive\\Desktop\\NewSqlExports\\test.pdf");
		ByteArrayOutputStream output = new ByteArrayOutputStream();
		byte[] buffer = new byte[1024];
		int len;
		while ((len = content.read(buffer)) != -1) {
			output.write(buffer, 0, len);
		}
		FileOutputStream out = new FileOutputStream(file);
		out.write(output.toByteArray());
		value = URLDecoder.decode(value, StandardCharsets.UTF_8.name());
		String str = new String(value.getBytes("UTF-8"),"UTF-8");
		NewAbsClass abst = new AbsChild();
		abst.doProcessing(NewAbsClass.class, 23);
		System.out.println(abst.getId(abst.getClass(),34));
		abst.getA();
		
		"adfd!22".matches("\\d+");
		
		DateTimeFormatter formatter = java.time.format.DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm:ss");
		String format = formatter.format(new java.util.Date().toInstant().atZone(java.time.ZoneId.of("Europe/Berlin")));
		format = formatter.format(new java.util.Date().toInstant().atZone(ZoneId.systemDefault()));
		LocalDate localDate = new Date().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
//		Europe/Berlin 
//		java.time.format.DateTimeFormatter.ofPattern("dd-MMM-yyyy HH:mm").withZone(java.time.ZoneId.of("Europe/Berlin")).format($P{START_DATE}.toInstant())
		System.out.println(format);
		
		int a = 20;
		Test test = new Test();
		test.marks1 = 20;
		test.subject = "math";
		test.marks2 = 29;
		System.out.println(test);
		int totalMarks = test.getTotalMarks((first,second) -> {
			System.out.println(a);
			return first + second;
		});
		System.out.println(totalMarks);
		test = new Test();
		System.out.println(test.subject);
		List<Integer>  list = Arrays.asList(1,13,32,4,264,34);
		//first perform the intermediate operations on list.like filter and map;
		
		list.forEach((i) -> System.out.print(i*3));
		list.stream().forEach(i -> System.out.println(i));
		
		for(Integer t :list) {
			System.out.println(t*3);
		}
		
		Testing ts = new Testing();
		ts.m1();
		Testing2 ts2 = new Testing2();
		ts2.m1();
	}
}












