package com.demo.demoHub.java8;

import java.util.Arrays;

public class ExamPractice {

	    public static String[] uniqueNames(String[] names1, String[] names2) {
	        String[] str = new String[names1.length + names2.length];
	        int len = 0;
	        // Merge logic
	        for (int i = 0; i<str.length; i++) {
	        	if(i < names1.length) {
	        		str[i] = names1[i];
	        	} else {
	        		if (len < names2.length) {
	        			str[i] = names2[len];
	        			len++;
	        		}
	        	}
	        		
	        }
	        Arrays.stream(str).forEach(System.out::println);
	        String[] str1 = new String[str.length];
	        int length = 0;
	      
	        for(int i = 0; i<str.length;i++) {
	        	  boolean duplicate = false;
	        	  System.out.println(length);
	        	for(int j = 1; j < str.length; j++) {
	        		if ((j + 1) < str.length && str[i].equals(str[j + 1])) {  // you can add this logic in separate method
	        			duplicate = true;
	        			break;
	        		}
	        	}
	        	if(!duplicate) {
	        		str1[length] = str[i];
	        		length++;
	        	}
	        	System.out.print(23);
	        }
	        Arrays.stream(str1).forEach(System.out::println);
	        return str1;// Arrays.stream(str).distinct().toArray(e -> new String[e]);
	    }
	    
	    public static void main(String[] args) {
	        String[] names1 = new String[] {"Ava", "Emma", "Olivia"};
	        String[] names2 = new String[] {"Olivia", "Sophia", "Emma"};
	        System.out.println(String.join(", ", ExamPractice.uniqueNames(names1, names2))); // should print Ava, Emma, Olivia, Sophia
	    }
}
