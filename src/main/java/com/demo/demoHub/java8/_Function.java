package com.demo.demoHub.java8;

import java.util.function.BiFunction;
import java.util.function.Function;

// This class used for Function<T,R> practice where it accept the two parameter in class definition Function<parameter_type, return_type>
public class _Function {

	public static void main(String[] args) {
		_Function a = new _Function();
		a.id = 10;
		a.name = "ahhei";
		// this is funtion example where is
		a.id = addNumberPlusOne.apply(10);
		
		// BiFunction exmaple: which accept any two parameter and return one result.
		int aa = biFunctionWhichAddTwoValue.apply(12, 12);
	}
	int id;
	String name;
	// we initialized the varible with function implementation now we can use it anywhere.
	static Function<Integer, Integer> addNumberPlusOne = number -> number + 1;
	
	// above representation is same as this one where we are sending one param and return another one.
	// Above one function is good no need to write method.
	static Integer addNewNumberPlusOne(Integer num) {
		return num + 1;
	}
	
	// BiFunction exmpale :
	static BiFunction<Integer, Integer, Integer> biFunctionWhichAddTwoValue = (num1, num2) -> num1 + num2;
	// This one variable and method does same work but instead of method use bifunction for two argument.
	static int biFunctionWhichAddTwoValue(int num1, int num2) {
		return num1 + num2;
	}
	
}
