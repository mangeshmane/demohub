package com.demo.demoHub.java8;

// First what is lamda expression: no method name(nameless), no return type and no access modifier.
// And only () with special symbol (->) arrow. It is anonymous class. Lambda is just method implementation
// of functional interface class having return type is that interface type. left side of = .
public class _Lambda {
	
	public static void printNameWithoutParameter() {
		System.out.println("My name is Java.");
	}
	
	public static void printNameWithParameter(int id, String name) {
		System.out.println("My name is "+ name + ". where id is = " + id);
	}
	/*
	 *  Suppose if we want to implement this method using the lambda just do below things.
	 *  1. () -> {System.out.println()}; in there is only one line then we can remove the curly bracket
	 *  2. (id, name) -> System.out.println(id + name);	so we are accepting the two param and define the method.
	 *  
	 *  3. At above line compiler automatically guess the type of parameter passed to a method.
	 *  4. Only single public abstract method can be declared in the interface and for implementation 
	 *     we can use the lambda expression for only single method.
	 *     
	 *  5. For data type compiler knows the type implicitly so don't need data type with varibles.(int a) ->instead just write a -> a*a;   
	 *  6. So there we can use the functional interface annotation @FunctionalInterface.  
	 *  7. If we (int a) -> { return a*a }; if we use curly brackets we need to add return keyword.
	 *     but if we don't write the curly bracket don't need to write return and this is for only 
	 *     single line expressions. like : (int a) -> a*a; and also for single parameter we can remove bracess also
	 *     a -> a*a;
	 *     
	 *  8. If lambda expression body contains only one line then remove the curly braces.
	 *  9. Without curly braces we can not use return keyword. compiler will automatically consider returned value.
	 *  10. With in the curly braces if we want return some value compulsory  we should use return.
	 
	 *  n -> return n*n; ==> invalid ... should use curly brackets with return.
	 *  n -> {return n*n;} ==> valid;   
	 *  n -> {return n*n} ==> invalid; ... use semicolon at end of the expression.  
	 *  n -> n*n; ==> valid;   
	 *  
	 *  In functional interface only one abstract method and it can contain any number of static and default methods.
	 *  because of this annotation compile can tell hey use only one abstract method. other interfaces can declare many method.
	 
	 *  @functionalInterface
	 *  interface Interf {
	 *  public void main1(); one abstract method.
	 *  static main2(){}
	 *  default main3(){}  any number of static and default methods.
	 *  }
	 *  For calling the lambda expression we need the functional interface. like below:
	
	 *  Interf i = () -> System.out.println("This is good");
	 *  i.main1(); // this will call the lambda expression. inside the i.
	 *  Parent p = new Child();
	 *  Basically instead of creating the separate class to implement the interface method we can write lambda for this method.
	 *  
	 *  11. we can not change the value of the local variable if they are used in the lambda we can just
	 *  	use them but can't change because at the time of lambda creation compiler take ss(screen shot) of that variable
	 *  	so actual value of that variable is going to use in the lambda so we can't change the value.
	 *  And this variable should be final or effectively final.
	 */
	
	
	public static void main(String[] args) {
		int x = 10;
		
		// Implementation is here for abstract method.
		Interf i = () -> System.out.println("This is lambda implementation" + x);
		
		// To call the lambda method use Functional interface object.
		i.printValue();
		
		Interf2 i2 = (a, b) -> a * b; // we can write { return a*b;}; a*b; 
		System.out.println(i2.multiply(10, 10));
	}
}

@FunctionalInterface
interface Interf {
	public void printValue(); // only one abstract method
}

@FunctionalInterface
interface Interf2 {
	public int multiply(int a, int b); // only one abstract method
}
