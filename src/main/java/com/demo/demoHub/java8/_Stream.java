package com.demo.demoHub.java8;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


// If we want to process the objects inside the collection then we use Stream.
// Stream s = c.stream();  stream method is available in collection interface and Stream is also interface.

//A stream is not a data structure instead it takes input from the Collections, Arrays, or I/O channels.

//Streams don’t change the original data structure, they only provide the result as per the pipelined methods.

//Each intermediate operation is lazily executed and returns a stream as a result, 
//hence various intermediate operations can be pipelined. Terminal operations mark the end of the stream and return the result.

public class _Stream {
	Integer ass = 10;
	public static void main(String[] args) {
		  
		String url = "https://sadwerkdev.blob.core.windows.net/dls-documents/493%2F238%2FdWerk_bdc14c1b-bffc-4df0-8f2a-525e3c4dfc23_sample.pdf";
		String sasUrl = "https://sarcsdocumentsdev.blob.core.windows.net/rcs-ivmdatafiles/newfolder/Data Cleaning.pdf";

	        // Extract the part after the domain and before the first forward slash
        int firstSlashIndex = sasUrl.indexOf('/', "https://".length());
        String folderName = sasUrl.substring(firstSlashIndex + 1, sasUrl.indexOf('/', firstSlashIndex + 1));

        // Extract the remaining part after the first forward slash
        String remainingPath = sasUrl.substring(sasUrl.indexOf('/', firstSlashIndex + 1));

		url = url.substring(url.lastIndexOf("/") + 1);
		// Create new stream using below two ways:
		
		Stream<Integer> intStream = Stream.of(1,3,4,2);
		// And
		Stream.Builder<Integer> buildInteger = Stream.builder();
		buildInteger.add(23);
		
		List<String> names = Arrays.asList("abc","def","ghi");
		
		Stream<String> name = names.stream(); // Created the stream instance. we can do it on the below line directly.
		name.filter(nm -> nm.contains("ab")) // first stream end. filter out the element and create new stream of element.
			.map(nam -> nam)				 // second stream end. Perform the operation on each element on the stream of elements.
			.forEach(System.out::println);	 // third terminal operation (end operation).
		
		/*forEach() is a terminal operation, which means that, after the operation is performed, 
		 * the stream pipeline is considered consumed, and can no longer be used.		
		 */Function<String , String> str11 = str1 -> str1+"abc";
		name = names.stream();
		System.out.println(name);
		List<String> str = name.filter(nm -> nm.contains("ab"))
							   .map(str11)					//  its one of the common ways to get stuff out of the 
							   .collect(Collectors.toList());   // stream once we are done with all the processing
		
		// findFirst(): return the first element in the optional<> form orElse() null;
		Predicate<Integer> predicate = num -> num > 1;
		Optional<Integer> optional = 
					intStream.filter(predicate)
//							 .count(); It is also a terminal operation.
							 .findFirst(); // Answer would be 3
		System.out.println(optional.get());
		intStream = Stream.of(1,3,4,2,34);  // With every stream operation stream pipeline is closing so need to create new value for reference.
		Integer[] array = intStream.toArray(number -> new Integer[number]); // toArray used to convert the stream members to java array.
		System.out.println(array);//.toArray(Integer[]::new);
		
		/*
		 *  Flat map : used to when we use complex data structures like List<List<String>> mapping and flattening.
		 *  so what actually map does is it covert all the stream into the single list of elements.
		 *  map(): map function covert all stream into single unit of stream. for each iteration it create new stream for each element and at last convert the new stream combined all.
		 *  flattening : means it convert all the combined stream data into single list.
		 */
		 List<List<String>> namesNested = Arrays.asList( 
			      Arrays.asList("Jeff", "Bezos"), 
			      Arrays.asList("Bill", "Gates"), 
			      Arrays.asList("Mark", "Zuckerberg"));
		 List<List<String>> collect = namesNested.stream().collect(Collectors.toList());
		 // But we want the object inside object so use flatMap.
		 List<Stream<String>> collect2 = namesNested.stream().map(Collection::stream).collect(Collectors.toList());
		 System.out.println(collect2);
		 // Above example uses flat map to get elements like list<list<>> to get under list and then perform operations.
		 List<String> collect3 = namesNested.stream().flatMap(list -> list.stream()).collect(Collectors.toList());
		 System.out.println(collect3);
		 
		 namesNested.stream().flatMap(Collection::stream).forEach(n -> System.out.println(n));
		 
		 System.out.println("count of the stream elements: "+namesNested.stream().count());
		 
			/*
			 * peek(): Sometimes we need to perform multiple operations on each element of
			 * the stream before any terminal operation is applied.
			 */
			Integer[] empSalary = { 10, 20, 102, 103 };
			Arrays.stream(empSalary).peek(e -> e.intValue()) // here we are assigning the operations on the stream
																// values and not returning values, So first check we
																// are assigning like e = e.intValue();
					.peek(e1 -> System.out.println(e1 * 2)).map(e -> e).forEach(System.out::println);
		 	
		 
		 /*
		  * As we’ve been discussing, Java stream operations are divided into intermediate and terminal operations.
		  * Intermediate operations such as filter() return a new stream on which further processing can be done. 
		  * Terminal operations, such as forEach(), mark the stream as consumed, after which point it can no longer be used further.
		  * A stream pipeline consists of a stream source, followed by zero or more intermediate operations, and a terminal operation.
		  
		  * Some operations are deemed short-circuiting operations. 
		  * Short-circuiting operations allow computations on infinite streams to complete in finite time: 
		  * Short-Circuiting: Some intermediate operations, such as filter and findFirst, are designed to short-circuit when possible. 
		  * This means that they will stop processing as soon as the result is determined, without examining the entire input
		  */
		// This only declaration of stream like method or lambda
		Stream<Integer> infiniteStream = Stream.iterate(5, i -> i * 2); // infinite start from 5 and 5*2 10*2 20*2
																			// (This is only declaration of stream so
																			// unless this is not in use it doesn't go
																			// for run)
																			// so on.... but actually when we want to
																			// short circuit it we use operations like
																			// limit and skip
//			infiniteStream.forEach(System.out::print);
			List<Integer> collect37 = infiniteStream.skip(3) // some short circuit operations. skip first 3 elements and
										.limit(5).collect(Collectors.toList()); // print up to 5 elements.
					
			collect37.forEach(System.out::print);
			
		/* Lazy Evaluation 
		 * One of the most important characteristics of Java streams is that they allow for significant optimizations through lazy evaluations.
		 * Computation on the source data is only performed when the terminal operation is initiated, and source elements are consumed only as needed.
           All intermediate operations are lazy, so they’re not executed until a result of a processing is actually needed.
           
		 */
		// Short-circuit operation : So basically condition match return the result and end the operation instantly.
		List<Integer> intList = Arrays.asList(12,23,35,327,88,90,34);
		System.out.println(intList.stream().allMatch(n -> n > 11)); // Return false if any of the element doesn't satisfy the predicate.
		
		System.out.println(intList.stream().anyMatch(n -> n > 11));
		
		System.out.println(intList.stream()
			   .map(n -> n).filter(n -> n > 12).findAny().orElse(null)); // will find the any one elements which are greater than 12

		System.out.println(intList.stream().findAny()); // It return the any means first or any number in the stream.
													    // Or any first element that satisfy the filter or map condition.
		System.out.println(intList.stream().filter(n -> n > 33).findFirst());
		System.out.println(intList.stream().max((a1,a2) -> a1.compareTo(a2)));
		System.out.println(intList.stream().max(Comparator.comparing(a -> a)));
			 
		/*
		 * From what we discussed so far, Stream is a stream of object references. However,
		 * the IntStream, LongStream, and DoubleStream – which are primitive specializations for int, long and double 
		 * respectively. These are quite convenient when dealing with a lot of numerical primitives.
		 */
		// map(Function<int,string>) returns the Stream<Type> but not IntStream or LongStream or Double stream.
		String as = "adgb";
		Function<Integer,Integer> string = a -> {
			_Stream st = new _Stream();
			
			return 10;
		};
		System.out.println(string.apply(29));
		Stream<String> map = intList.stream().map(a -> {
			
			return "string";
		}); // like this
		IntStream intStream1 = intList.stream().mapToInt(n -> n);
		double orElse = intStream1.average().orElse(0);
	
		Predicate<Integer> isTrue = a -> 12 > a;
		Consumer<Integer> cons = a -> System.out.println(a);
		Function<Integer,String> fun = a -> "sldfj";
		Supplier<List<String>> sup = () -> Arrays.asList("Kunbi rushi bhosale");
		getListOfString(sup);
		Object a = new Car();
 	}



	static <T> T getListOfString(Supplier<T> supplier) {
		return supplier.get();
	 
}
}
@AllArgsConstructor
@Getter
@Setter
abstract class AbstractAnimal {
	private String color;
	private String breed;
	
	public AbstractAnimal() {
		System.out.println("Animal is here...");
	}
	
	void makeSound() {
		System.out.println("Animal is making sound...");
	}
	
	void eat() {
		System.out.println("Animal is eating...");
	}
}
class AnimalDog extends AbstractAnimal {
	
	public AnimalDog() {}
	
	public AnimalDog(String color, String breed) {
		super(color, breed);
	}
	
	void eatw() {
		System.out.println("Dog is eating...");
		AnimalDog aa = new AnimalDog();
		System.out.println(aa);
	}
	
	public static void main(String[] args ) {
		AbstractAnimal dog = new AnimalDog("brown", "gavthi");
		dog.eat();
		dog.makeSound();
		AbstractAnimalWithTypes<String> forName = new AbstractAnimalWithTypes<>();
		forName.type = "This is Type";
	}
	
}

class AbstractAnimalWithTypes<T> {
	T type;
	
	public void printT() {
		System.out.println(type);
	}
}
