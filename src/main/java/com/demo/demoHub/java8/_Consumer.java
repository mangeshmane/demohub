package com.demo.demoHub.java8;

import java.util.function.BiConsumer;
import java.util.function.Consumer;

// Consumer : accept the one parameter and sending void. and Bi-consumer accept 2 parameter and return void.
public class _Consumer {
	int id;
	String name;
	
	@Override
	public String toString() {
		return "_Consumer [id=" + id + ", name=" + name + "]";
	}

	public static void main(String[] args) {
		_Consumer consumer = new _Consumer();
		consumer.id = 1;consumer.name = "adder";
		// simple function
		printConsumer(consumer);
		
		// call method with consumer and bi-consumer.
		printConsumer.accept(consumer);
		printTwoNumber.accept(1, 12);
		
	}
	// function 
	public static void printConsumer(_Consumer consumer) {
		System.out.println(consumer);
	}
	
	static Consumer<_Consumer> printConsumer = consumer -> System.out.println(consumer);
	
	static BiConsumer<Integer,Integer> printTwoNumber = (num1, num2) -> System.out.println(num1 + num2);
	
	
	
	
	
	
	
	
	
	
}
