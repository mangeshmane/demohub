package com.demo.demoHub.java8;

import java.util.List;
import java.util.function.Predicate;

public class FunctionalProgramming {
//	two types of coding imperative and declarative. write all code and other is just tell the what we want in output.
	public static void main(String[] args) {
		List<Integer> l = null;
		l.stream().filter(null);
		// Here we are added the integer in angular at the compile time due to type erasure T is replaced with integer.
		_Predicate<Integer> pred = new _Predicate<Integer>() { 

			@Override
			public boolean check(Integer a) {
				if (a > 10)
					return false;
				else
					return true;
			}
		};
		
		// Now use this implementation based on why we written this implementation and we can use the different implementation with different data types and get response in boolean.
		checkNumberIsGreaterThanTen(pred);
		
	}
	
	public static boolean checkNumberIsGreaterThanTen(_Predicate<Integer> pred) {
		return pred.check(10);
	}
	
}
// why we put the generics interface because we want call this method using all data types.
// We use this method implementation on the basis of their functional use.(use everywhere if we want to give the response in boolean form.
interface _Predicate<T> {
//	public abstract boolean check(int a, int b);
	boolean check(T i);
	
}
