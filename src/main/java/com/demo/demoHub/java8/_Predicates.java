package com.demo.demoHub.java8;

import java.util.function.Predicate;

// This returns the boolean like condition checks.
public class _Predicates {
	
	

	public static void main(String[] args) {
		Car car = new Car();
		car.name = "aston_martin";
		returnValueIsPresentOrNot(car);
	}
	
	static Predicate<Car> predicate = car -> car.name != null;
	
	// So above and below methods are actually difference between the normal methods and functionals
	// Interface methods;
	
	static boolean returnValueIsPresentOrNot(Car car) {
		return null != car.name ? true : false;
	}
}
class Car {
	int number;
	String name;
}
