package com.demo.demoHub.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "performance")
@Getter
@Setter
public class Performance {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "hour")
    private Double hour;

    @Column(name = "min1")
    private Double min1;

    @Column(name = "min5")
    private Double min5;

    @Column(name = "min15")
    private Double min15;

    @Column(name = "day")
    private Double day;

    @Column(name = "week")
    private Double week;

    @Column(name = "month")
    private Double month;

    @Column(name = "year")
    private Double year;
}
