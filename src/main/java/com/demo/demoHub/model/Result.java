package com.demo.demoHub.model;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class Result<T> {
	private Boolean success;
	private List<String> errors;
	private String successMessage;
	
	private T responseObject;
	
	public void setResponesObject(T object) {
		this.responseObject = object;
	}
	
	public Result<T> returenResult(T object) {
		this.responseObject = object;
		return new Result(this);
	}
	
	public Result() {
		
	}
	public Result(T object) {
		this.responseObject = object;
	}
	
	
	public T getResponseObject() {
		return responseObject;
	}

	public void setResponseObject(T responseObject) {
		this.responseObject = responseObject;
	}


	private File file;
	public File getFile() {
		return file;
	}
	public void setFile(File file) {
		this.file = file;
	}
	public Boolean getSuccess() {
		return success;
	}
	public void setSuccess(Boolean success) {
		this.success = success;
	}
	public List<String> getErrors() {
		return errors;
	}
	public void setErrors(List<String> errors) {
		this.errors = errors;
	}
	public String getSuccessMessage() {
		return successMessage;
	}
	public void setSuccessMessage(String successMessage) {
		this.successMessage = successMessage;
	}
	
	public void setResult(List<String> errors) {
		this.errors=errors;
		if(errors.isEmpty()) {
			this.success=true;
		}else {
			this.success=false;
		}
	}
	
	public void setError(String error) {
	errors=new ArrayList<String>();
	if(error.isEmpty()) {
		this.success=true;
	}else {

		errors.add(error);
	}
	}
	public void show() {
		System.out.println("You got the object");
	}
}
