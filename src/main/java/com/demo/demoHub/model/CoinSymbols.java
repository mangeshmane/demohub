package com.demo.demoHub.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
public class CoinSymbols {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "binance")
    private String binance;

    @Column(name = "kucoin")
    private String kucoin;

    @Column(name = "bybit")
    private String bybit;

    @Column(name = "gateio")
    private String gateio;

    @Column(name = "coinbase")
    private String coinbase;

    @Column(name = "mexc")
    private String mexc;

    @Column(name = "okx")
    private String okx;
}
