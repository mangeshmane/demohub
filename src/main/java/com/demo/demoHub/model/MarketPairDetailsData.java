package com.demo.demoHub.model;

import java.util.Arrays;

import com.fasterxml.jackson.annotation.JsonProperty;

public class MarketPairDetailsData {
	
	    @JsonProperty("coindcx_name")
	    private String coindcxName;

	    @JsonProperty("base_currency_short_name")
	    private String baseCurrencyShortName;

	    @JsonProperty("target_currency_short_name")
	    private String targetCurrencyShortName;

	    @JsonProperty("target_currency_name")
	    private String targetCurrencyName;

	    @JsonProperty("base_currency_name")
	    private String baseCurrencyName;

	    @JsonProperty("min_quantity")
	    private Long minQuantity;

	    @JsonProperty("max_quantity")
	    private Long maxQuantity;

	    @JsonProperty("min_price")
	    private double minPrice;

	    @JsonProperty("max_price")
	    private double maxPrice;

	    @JsonProperty("min_notional")
	    private double minNotional;

	    @JsonProperty("base_currency_precision")
	    private Long baseCurrencyPrecision;

	    @JsonProperty("target_currency_precision")
	    private long targetCurrencyPrecision;

	    private long step;

	    @JsonProperty("order_types")
	    private String[] orderTypes;

	    private String symbol;
	    private String ecode;

	    @JsonProperty("max_leverage")
	    private int maxLeverage;

	    @JsonProperty("max_leverage_short")
	    private String maxLeverageShort;

	    private String pair;
	    private String status;
		public String getCoindcxName() {
			return coindcxName;
		}
		public void setCoindcxName(String coindcxName) {
			this.coindcxName = coindcxName;
		}
		public String getBaseCurrencyShortName() {
			return baseCurrencyShortName;
		}
		public void setBaseCurrencyShortName(String baseCurrencyShortName) {
			this.baseCurrencyShortName = baseCurrencyShortName;
		}
		public String getTargetCurrencyShortName() {
			return targetCurrencyShortName;
		}
		public void setTargetCurrencyShortName(String targetCurrencyShortName) {
			this.targetCurrencyShortName = targetCurrencyShortName;
		}
		public String getTargetCurrencyName() {
			return targetCurrencyName;
		}
		public void setTargetCurrencyName(String targetCurrencyName) {
			this.targetCurrencyName = targetCurrencyName;
		}
		public String getBaseCurrencyName() {
			return baseCurrencyName;
		}
		public void setBaseCurrencyName(String baseCurrencyName) {
			this.baseCurrencyName = baseCurrencyName;
		}
		public Long getMinQuantity() {
			return minQuantity;
		}
		public void setMinQuantity(Long minQuantity) {
			this.minQuantity = minQuantity;
		}
		public Long getMaxQuantity() {
			return maxQuantity;
		}
		public void setMaxQuantity(Long maxQuantity) {
			this.maxQuantity = maxQuantity;
		}
		public double getMinPrice() {
			return minPrice;
		}
		public void setMinPrice(double minPrice) {
			this.minPrice = minPrice;
		}
		public double getMaxPrice() {
			return maxPrice;
		}
		public void setMaxPrice(double maxPrice) {
			this.maxPrice = maxPrice;
		}
		public double getMinNotional() {
			return minNotional;
		}
		public void setMinNotional(double minNotional) {
			this.minNotional = minNotional;
		}
		public Long getBaseCurrencyPrecision() {
			return baseCurrencyPrecision;
		}
		public void setBaseCurrencyPrecision(Long baseCurrencyPrecision) {
			this.baseCurrencyPrecision = baseCurrencyPrecision;
		}
		public long getTargetCurrencyPrecision() {
			return targetCurrencyPrecision;
		}
		public void setTargetCurrencyPrecision(long targetCurrencyPrecision) {
			this.targetCurrencyPrecision = targetCurrencyPrecision;
		}
		public long getStep() {
			return step;
		}
		public void setStep(long step) {
			this.step = step;
		}
		public String[] getOrderTypes() {
			return orderTypes;
		}
		public void setOrderTypes(String[] orderTypes) {
			this.orderTypes = orderTypes;
		}
		public String getSymbol() {
			return symbol;
		}
		public void setSymbol(String symbol) {
			this.symbol = symbol;
		}
		public String getEcode() {
			return ecode;
		}
		public void setEcode(String ecode) {
			this.ecode = ecode;
		}
		public int getMaxLeverage() {
			return maxLeverage;
		}
		public void setMaxLeverage(int maxLeverage) {
			this.maxLeverage = maxLeverage;
		}
		public String getMaxLeverageShort() {
			return maxLeverageShort;
		}
		public void setMaxLeverageShort(String maxLeverageShort) {
			this.maxLeverageShort = maxLeverageShort;
		}
		public String getPair() {
			return pair;
		}
		public void setPair(String pair) {
			this.pair = pair;
		}
		public String getStatus() {
			return status;
		}
		public void setStatus(String status) {
			this.status = status;
		}
		@Override
		public String toString() {
			return "MarketPairDetailsData [coindcxName=" + coindcxName + ", baseCurrencyShortName="
					+ baseCurrencyShortName + ", targetCurrencyShortName=" + targetCurrencyShortName
					+ ", targetCurrencyName=" + targetCurrencyName + ", baseCurrencyName=" + baseCurrencyName
					+ ", minQuantity=" + minQuantity + ", maxQuantity=" + maxQuantity + ", minPrice=" + minPrice
					+ ", maxPrice=" + maxPrice + ", minNotional=" + minNotional + ", baseCurrencyPrecision="
					+ baseCurrencyPrecision + ", targetCurrencyPrecision=" + targetCurrencyPrecision + ", step=" + step
					+ ", orderTypes=" + Arrays.toString(orderTypes) + ", symbol=" + symbol + ", ecode=" + ecode
					+ ", maxLeverage=" + maxLeverage + ", maxLeverageShort=" + maxLeverageShort + ", pair=" + pair
					+ ", status=" + status + "]";
		}
}