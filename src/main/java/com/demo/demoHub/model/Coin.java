package com.demo.demoHub.model;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "coins")
@Setter
@Getter
public class Coin {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@JsonProperty("id")
	private Long id;

	@Column(name = "name")
	@JsonProperty("name")
	private String name;

	@Column(name = "slug")
	@JsonProperty("slug")
	private String slug;

	@Column(name = "rank")
	@JsonProperty("rank")
	private int rank;

	@Column(name = "symbol")
	@JsonProperty("symbol")
	private String symbol;

	@ManyToOne(targetEntity = CoinSymbols.class, cascade = CascadeType.ALL)
	@JoinColumn(name = "symbols_id")
	@JsonProperty("symbols")
	private CoinSymbols symbols;

	@Column(name = "image")
	@JsonProperty("image")
	private String image;

	@Column(name = "stable")
	@JsonProperty("stable")
	private boolean stable;

	@Column(name = "circulating_supply")
	@JsonProperty("circulating_supply")
	private Long circulatingSupply;

	@Column(name = "dominance")
	@JsonProperty("dominance")
	private double dominance;

	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "rank_diffs_id")
	@JsonProperty("rankDiffs")
	private RankDiffs rankDiffs;

	@Column(name = "cg_id")
	@JsonProperty("cg_id")
	private String cgId;

	@Column(name = "price")
	@JsonProperty("price")
	private double price;

	@Column(name = "marketcap")
	@JsonProperty("marketcap")
	private Long marketcap;

	@Column(name = "volume")
	@JsonProperty("volume")
	private Long volume;

	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "performance_id")
	@JsonProperty("performance")
	private Performance performance;

	@Override
	public String toString() {
		return "Coin [id=" + id + ", name=" + name + ", slug=" + slug + ", rank=" + rank + ", symbol=" + symbol
				+ ", image=" + image + ", stable=" + stable + ", circulatingSupply=" + circulatingSupply
				+ ", dominance=" + dominance + ", cgId=" + cgId + ", price=" + price + ", marketcap=" + marketcap
				+ ", volume=" + volume + "]";
	}
}
