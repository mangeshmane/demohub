package com.demo.demoHub.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.demo.demoHub.crypto.service.CryptoWalletService;
import com.demo.demoHub.model.Result;
import com.demo.demoHub.response.GetAllMarketResponse;
import com.demo.demoHub.response.GetAllPairsWithMarketDetailsResponse;
import com.demo.demoHub.service.ICoinDcxService;

@RestController
@RequestMapping("/dcx")
public class CryptoController {

	public static final Logger logger = LoggerFactory.getLogger(CryptoController.class);
	
	@Autowired
	private CryptoWalletService cryptoWalletService;
	
	@Autowired
	private ICoinDcxService coinDcxService;
	
	@PreAuthorize("hasAuthority ('USER')")
	@RequestMapping(value = "/createether", method = RequestMethod.POST)
	public Result createEther() {
		try {
			return cryptoWalletService.createEtherWallet();
			
		}catch(Exception e) {
			logger.info(e.getMessage().toString());
		}
		return new Result();
	}
	
	@PreAuthorize("hasAuthority ('USER')")
	@GetMapping(value = "/exchange/v1/markets")
	public ResponseEntity<GetAllMarketResponse> getAllActiveMarketPairs() {
		try {
			GetAllMarketResponse response = coinDcxService.getAllPairsInMarkets();
			if(null != response) {
				return ResponseEntity.ok(response);
			} else {
				return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new GetAllMarketResponse());
			}
			
		} catch(Exception e) {
			logger.info(e.getMessage().toString());
		}
		return null;
	}
	
	@PreAuthorize("hasAuthority ('USER')")
	@GetMapping(value = "/exchange/v1/markets_details")
	public ResponseEntity<GetAllPairsWithMarketDetailsResponse> getAllActiveMarketPairsWithMarketsDetails() {
		try {
			GetAllPairsWithMarketDetailsResponse response = coinDcxService.getAllPairsWithMarketsDetails();
			if(null != response) {
				return ResponseEntity.ok(response);
			} else {
				return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new GetAllPairsWithMarketDetailsResponse());
			}
			
		} catch(Exception e) {
			logger.info(e.getMessage().toString());
		}
		return null;
	}
	
}
