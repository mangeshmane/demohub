package com.demo.demoHub.controller;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.security.auth.login.LoginException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.demo.demoHub.model.Result;
import com.demo.demoHub.model.User;
import com.demo.demoHub.service.UserService;
import com.itextpdf.io.source.ByteArrayOutputStream;
import com.itextpdf.kernel.colors.Color;
import com.itextpdf.kernel.colors.ColorConstants;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.kernel.pdf.canvas.PdfCanvas;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.borders.Border;
import com.itextpdf.layout.borders.SolidBorder;
import com.itextpdf.layout.borders.Border.Side;
import com.itextpdf.layout.element.Cell;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.element.Table;
import com.itextpdf.layout.properties.UnitValue;

@RestController
public class UserController {

	@Autowired
	private UserService userService;
	
	private static final Logger logger = LoggerFactory.getLogger(UserController.class);

	@RequestMapping(value = "/register" ,method = RequestMethod.POST)
	public Result registerUser(@RequestBody User user) {
		try {
			return userService.registerUser(user);
		} catch (Exception e) {
			logger.error(e.toString());
		}
		return new Result();
	}
	
	@RequestMapping(value = "/authenticate",method = RequestMethod.POST)
	public Map<String,Object> loginUser(@RequestBody User user) throws LoginException {
		try {
			return userService.loginUser(user);
		} catch (Exception e) {
			logger.error(e.toString());
			throw new LoginException("user not found");
		}
		
	}
	@PreAuthorize("hasAuthority ('USER')")
	@RequestMapping(value = "/getallusers" ,method = RequestMethod.GET )
	public List<User> getUsers(){
		try {
			return userService.getAllUsers();
		}
		catch(Exception e) {
			logger.error(e.toString());
		}
		return new ArrayList<User>();
	}
	
	@PreAuthorize("hasAuthority ('USER')")
	@RequestMapping(value = "/signout", method = RequestMethod.GET)
	public Result logout() throws Exception{
		try {
			return userService.logOutCurrentUser();
		}
		catch(Exception e) {
			logger.error(e.toString());
			throw new Exception("USER NOT FOUND EXCEPTION");
		}
		
	}
	@RequestMapping(value = "/get_pdf", method = RequestMethod.GET)
	public Result createPdf() throws FileNotFoundException{
		Result result = new Result();
		File file = new File("C:\\Users\\mange\\OneDrive\\Desktop\\New folder\\pdf\\"
				+ "_sign.pdf");
		PdfWriter writer = new PdfWriter(file);
		PdfDocument pdfDoc = new PdfDocument(writer);
		pdfDoc.addNewPage();
		Document doc = new Document(pdfDoc);
		Paragraph para = new Paragraph("New Start...");
		doc.add(para);
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		result.setFile(file);
		float[] colWidth = { 40, 300, 50, 300, 200, 50 };
		Table table = new Table(UnitValue.createPercentArray(colWidth));
	    table.setWidth(UnitValue.createPercentValue(100));
	    table.setFixedLayout();
		table.addCell(new Cell().add(new Paragraph("upi").setBackgroundColor(ColorConstants.DARK_GRAY)));
		table.addCell(new Cell().add(new Paragraph("upi")));
		table.addCell(new Cell().add(new Paragraph("upi")));
		table.addCell(new Cell().add(new Paragraph("upi")));
		table.addCell(new Cell().add(new Paragraph("upi")));
		table.addCell(new Cell().add(new Paragraph("upi")));
		doc.add(table);
		doc.close();
		return result;
		
	}
	
}
